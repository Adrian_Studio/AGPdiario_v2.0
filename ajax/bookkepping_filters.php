<?php 
    if(!empty($_POST["fechaInicio"])){
        $sql .= ' AND contabilidad.fecha >= ?';
        $a_params[] = $_POST["fechaInicio"];
    }       
    if(!empty($_POST["fechaFin"])){
        $sql .= ' AND contabilidad.fecha <= ?';
        $a_params[] = $_POST["fechaFin"];
    } 
    if(!empty($_POST["año"])){
        $sql .= ' AND YEAR(contabilidad.fecha) LIKE ?';
        $a_params[] = $_POST["año"];
    }
    if(!empty($_POST["mes"])){
        $sql .= ' AND MONTH(contabilidad.fecha) LIKE ?';
        $a_params[] = $_POST["mes"];
    }
    if(!empty($_POST["categoria"])){
        $sql .= ' AND conceptos.id_categoria IN (';
        foreach($_POST["categoria"] as $key=>$valor){
            $sql .= '?';
            if($key != count($_POST["categoria"]) - 1 ){
                $sql .= ','; 
            }
            $a_params[] = $valor;
        }
        $sql .= ' )';
    }
    if(!empty($_POST["concepto"])){
        $sql .= ' AND contabilidad.id_concepto IN (';
        foreach($_POST["concepto"] as $key=>$valor){
            $sql .= '?';
            if($key != count($_POST["concepto"]) - 1 ){
                $sql .= ','; 
            }
            $a_params[] = $valor;
        }
        $sql .= ' )';
    }
    $sql .= ' AND contabilidad.id_usuario LIKE ?';
    $a_params[] = $id_usuario;

?>