<?php 
   
    include('obtenRol.php');
    include('../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'new'){
        $date=str_replace('/','-',$_POST["fecha"]);
        $date = new DateTime($date);

        $stmt = $db->prepare("INSERT INTO contabilidad (fecha, importe, observaciones, id_concepto, id_usuario) VALUES ( ?, ?, ?, ?, ?)");
        $stmt->bind_param('sdsii', $date->format('Y-m-d'), $_POST["importe"], $_POST["observaciones"], $_POST["concepto"], $id_usuario);
        $stmt->execute();
        $contabilidad_id = $stmt->insert_id;

        $stmt = $db->prepare("SELECT categorias.*, conceptos.concepto FROM conceptos INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE conceptos.id = ?");
        $stmt->bind_param('i', $_POST['concepto']);
        $stmt->execute();
        $result = $stmt->get_result();
        $dato = $result->fetch_assoc();
        if($dato['paraVivienda']){
            $stmt = $db->prepare("INSERT INTO contabilidad_vivienda (id_contabilidad, id_vivienda) VALUES ( ?, ?)");
            $stmt->bind_param('ii', $contabilidad_id, $_POST["vivienda"]);
            $stmt->execute();
        }
        if($dato['paraVehiculo']){
            $stmt = $db->prepare("INSERT INTO contabilidad_vehiculo (cuenta_km, litros, id_contabilidad, id_vehiculo) VALUES ( ?, ?, ?, ?)");
            $stmt->bind_param('idii', $_POST["cuentakilometros"], $_POST["litros"], $contabilidad_id, $_POST["vehiculo"]);
            $stmt->execute();
        }
     
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'delete'){

        $stmt = $db->prepare("DELETE FROM contabilidad WHERE id = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();

        $stmt = $db->prepare("DELETE FROM contabilidad_vivienda WHERE id_contabilidad = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();

        $stmt = $db->prepare("DELETE FROM contabilidad_vehiculo WHERE id_contabilidad = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();


    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'update'){
        $date=str_replace('/','-',$_POST["fecha"]);
        $date = new DateTime($date);

        $stmt = $db->prepare("UPDATE contabilidad SET fecha = ?, importe = ?, observaciones = ?, id_concepto = ? WHERE id = ?");
        $stmt->bind_param('sdsii', $date->format('Y-m-d'), $_POST["importe"], $_POST["observaciones"], $_POST["concepto"], $_POST['editTag']);
        $stmt->execute();
        
        $stmt = $db->prepare("SELECT categorias.*, conceptos.concepto FROM conceptos INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE conceptos.id = ?");
        $stmt->bind_param('i', $_POST['concepto']);
        $stmt->execute();
        $result = $stmt->get_result();
        $dato = $result->fetch_assoc();
        if($dato['paraVivienda']){
            $stmt = $db->prepare("SELECT * FROM contabilidad_vivienda WHERE id_contabilidad = ?");
            $stmt->bind_param('i', $_POST['editTag']);
            $stmt->execute();
            $result = $stmt->get_result();
            $vivienda = $result->fetch_assoc();
            if(!empty($vivienda['id'])){
                $stmt = $db->prepare("UPDATE contabilidad_vivienda SET id_contabilidad = ?, id_vivienda = ? WHERE id = ?");
                $stmt->bind_param('iii', $_POST['editTag'], $_POST["vivienda"], $vivienda['id']);
                $stmt->execute();
            }else{
                $stmt = $db->prepare("INSERT INTO contabilidad_vivienda (id_contabilidad, id_vivienda) VALUES ( ?, ?)");
                $stmt->bind_param('ii', $_POST['editTag'], $_POST["vivienda"]);
                $stmt->execute();
            }        
        }else{
            $stmt = $db->prepare("DELETE FROM contabilidad_vivienda WHERE id_contabilidad = ?");
            $stmt->bind_param('i', $_POST['editTag']);
            $stmt->execute();
        }
        if($dato['paraVehiculo']){
            $stmt = $db->prepare("SELECT * FROM contabilidad_vehiculo WHERE id_contabilidad = ?");
            $stmt->bind_param('i', $_POST['editTag']);
            $stmt->execute();
            $result = $stmt->get_result();
            $vehiculo = $result->fetch_assoc();
            if(!empty($vehiculo['id'])){
                $stmt = $db->prepare("UPDATE contabilidad_vehiculo SET cuenta_km = ?, litros = ?, id_contabilidad = ?, id_vehiculo = ? WHERE id = ?");
                $stmt->bind_param('idiii', $_POST["cuentakilometros"], $_POST["litros"], $_POST['editTag'], $_POST["vehiculo"], $vehiculo['id']);
                $stmt->execute();
            }else{
                $stmt = $db->prepare("INSERT INTO contabilidad_vehiculo (cuenta_km, litros, id_contabilidad, id_vehiculo) VALUES ( ?, ?, ?, ?)");
                $stmt->bind_param('idii', $_POST["cuentakilometros"], $_POST["litros"], $_POST['editTag'], $_POST["vehiculo"]);
                $stmt->execute();
            }        
        }else{
            $stmt = $db->prepare("DELETE FROM contabilidad_vehiculo WHERE id_contabilidad = ?");
            $stmt->bind_param('i', $_POST['editTag']);
            $stmt->execute();
        }
        
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainOne'){
        //Obtener dato para copiar/editar
        $stmt = $db->prepare("SELECT contabilidad.*, conceptos.id_categoria, categorias.tipoContabilidad, categorias.paraVivienda, categorias.paraVehiculo  FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE contabilidad.id = ? AND categorias.id_usuario LIKE ?");
        $stmt->bind_param('ii', $_POST['table_id'], $id_usuario);
        $stmt->execute();
        $result = $stmt->get_result();
        $dato = $result->fetch_assoc();

        if ($dato['paraVehiculo']){
            $contabilidad_Id = $stmt->insert_id;
            $stmt = $db->prepare("SELECT contabilidad.*, conceptos.id_categoria, categorias.tipoContabilidad, contabilidad_vehiculo.*, categorias.categoria AS categoria , conceptos.concepto AS concepto FROM contabilidad INNER JOIN contabilidad_vehiculo ON contabilidad.id=contabilidad_vehiculo.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE contabilidad.id = ? AND categorias.id_usuario LIKE ?");
            $stmt->bind_param('ii', $_POST['table_id'], $id_usuario);
            $stmt->execute();
            $result = $stmt->get_result();
    
            $i = 0;
            $array_datos = array();
            
            while($dato = $result->fetch_assoc()) {

                $array_datos[$i]['id'] = $dato['id'];
                $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
                $array_datos[$i]['id_categoria'] = $dato['id_categoria'];
                $array_datos[$i]['id_concepto'] = $dato['id_concepto'];
                $array_datos[$i]['fecha'] = $dato['fecha'];
                $array_datos[$i]['importe'] = $dato['importe'];
                $array_datos[$i]['observaciones'] = $dato['observaciones'];
                $array_datos[$i]['id_vehiculo'] = $dato['id_vehiculo'];
                $array_datos[$i]['cuenta_km'] = $dato['cuenta_km'];
                $array_datos[$i]['litros'] = $dato['litros'];
                $i++;
            }
        }else if ($dato['paraVivienda']){
            $contabilidad_Id = $stmt->insert_id;
            $stmt = $db->prepare("SELECT contabilidad.*, conceptos.id_categoria, categorias.tipoContabilidad, contabilidad_vivienda.*, categorias.categoria AS categoria , conceptos.concepto AS concepto FROM contabilidad INNER JOIN contabilidad_vivienda ON contabilidad.id=contabilidad_vivienda.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE contabilidad.id = ? AND categorias.id_usuario LIKE ?");            
            $stmt->bind_param('ii', $_POST['table_id'], $id_usuario);
            $stmt->execute();
            $result = $stmt->get_result();
    
            $i = 0;
            $array_datos = array();
            
            while($dato = $result->fetch_assoc()) {
    
                $array_datos[$i]['id'] = $dato['id'];
                $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
                $array_datos[$i]['id_categoria'] = $dato['id_categoria'];
                $array_datos[$i]['id_concepto'] = $dato['id_concepto'];
                $array_datos[$i]['fecha'] = $dato['fecha'];
                $array_datos[$i]['importe'] = $dato['importe'];
                $array_datos[$i]['observaciones'] = $dato['observaciones'];
                $array_datos[$i]['id_vivienda'] = $dato['id_vivienda'];
                $i++;
            }
        }else{
            $contabilidad_Id = $stmt->insert_id;
            $stmt = $db->prepare("SELECT contabilidad.*, conceptos.id_categoria, categorias.tipoContabilidad, categorias.id_usuario FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE contabilidad.id = ?  AND categorias.id_usuario LIKE ?");
            $stmt->bind_param('ii', $_POST['table_id'], $id_usuario);
            $stmt->execute();
            $result = $stmt->get_result();
    
            $i = 0;
            $array_datos = array();
            
            while($dato = $result->fetch_assoc()) {
    
                $array_datos[$i]['id'] = $dato['id'];
                $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
                $array_datos[$i]['id_categoria'] = $dato['id_categoria'];
                $array_datos[$i]['id_concepto'] = $dato['id_concepto'];
                $array_datos[$i]['fecha'] = $dato['fecha'];
                $array_datos[$i]['importe'] = $dato['importe'];
                $array_datos[$i]['observaciones'] = $dato['observaciones'];
                $i++;
            }
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainCategories'){
        //Obtener categorias
        $a_params = array(); 
        $sql = 'SELECT * FROM categorias WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        if(!empty($_POST["tipoContabilidad"])){
            $sql .= ' AND tipoContabilidad LIKE ?';
            $a_params[] = $_POST["tipoContabilidad"];
        }   
        if(!empty($_POST["grupo"]) && $_POST["grupo"] == 'Vivienda'){
            $sql .= ' AND paraVivienda';
        }   
        if(!empty($_POST["grupo"]) && $_POST["grupo"] == 'Vehiculo'){
            $sql .= ' AND paraVehiculo';
        }   


        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
            $array_datos[$i]['texto'] = $dato['categoria'];
            $array_datos[$i]['paraVivienda'] = $dato['paraVivienda'];
            $array_datos[$i]['paraVehiculo'] = $dato['paraVehiculo'];


            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainConcept'){
        //Obtener conceptos
        $a_params = array(); 
        $sql = 'SELECT categorias.categoria, categorias.id_usuario, categorias.tipoContabilidad, conceptos.*, conceptos_vehiculo.tipo AS veiculo_concepto_tipo FROM conceptos INNER JOIN categorias ON categorias.id = conceptos.id_categoria LEFT JOIN conceptos_vehiculo ON conceptos.id = conceptos_vehiculo.id_concepto WHERE categorias.id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        if(!empty($_POST["id_categoria"])){
            $sql .= ' AND id_categoria IN (';
            
            foreach($_POST["id_categoria"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_categoria"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            } 
            $sql .= ' )';
        }

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();


        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['texto'] = $dato['concepto'];
            $array_datos[$i]['veiculo_concepto_tipo'] = $dato['veiculo_concepto_tipo'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainYears'){
        //Obtener años
        $stmt = $db->prepare('SELECT YEAR(fecha) AS año FROM contabilidad WHERE id_usuario LIKE ? GROUP BY YEAR(fecha) DESC');
        $a_params[] = $id_usuario;
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['año'];
            $array_datos[$i]['texto'] = $dato['año'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainHouses'){
        //Obtener viviendas
        $stmt = $db->prepare('SELECT * FROM viviendas WHERE id_usuario LIKE ?');
        $a_params[] = $id_usuario;
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['texto'] = $dato['calle'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainVehicles'){
        //Obtener vehiculos
        $stmt = $db->prepare('SELECT * FROM vehiculos WHERE id_usuario LIKE ?');
        $a_params[] = $id_usuario;
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['texto'] = $dato['marca'] . " " . $dato['modelo'] . " " . $dato['matricula'];
            $i++;
        }

        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>