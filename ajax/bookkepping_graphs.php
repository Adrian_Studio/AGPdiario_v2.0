<?php 
   
    include('obtenRol.php');
    include('../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
    
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainGeneralCategoryChart'){
        //Obtener graficos generales por categorias
        $a_params = array(); 
        $sql = 'SELECT categorias.categoria AS etiqueta, sum(contabilidad.importe) AS importe FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE categorias.tipoContabilidad LIKE ?';
        $a_params[] = $_POST["tipoContabilidad"];

        include('bookkepping_filters.php');

        $sql .= ' GROUP BY categoria ORDER BY etiqueta';
        
        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['etiqueta'] = $dato['etiqueta'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainGeneralConceptChart'){
        //Obtener graficos generales por conceptos
        $a_params = array(); 
        $sql = 'SELECT categorias.categoria AS categoria , conceptos.concepto AS etiqueta, sum(contabilidad.importe) AS importe FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE categorias.tipoContabilidad LIKE ?';
        $a_params[] = $_POST["tipoContabilidad"];

        include('bookkepping_filters.php');

        $sql .= ' AND conceptos.id_categoria LIKE ? GROUP BY concepto';
        $a_params[] = $_POST["id_categoria"];
        
        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['etiqueta'] = $dato['etiqueta'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainHomeCategoryChart'){
        //Obtener graficos vivienda por categorias
        $a_params = array(); 
        $sql = 'SELECT categorias.categoria AS etiqueta, SUM(contabilidad.importe) AS importe FROM contabilidad INNER JOIN contabilidad_vivienda ON contabilidad.id=contabilidad_vivienda.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE categorias.tipoContabilidad LIKE ?';
        $a_params[] = $_POST["tipoContabilidad"];

        if(!empty($_POST["id_vivienda"])){
            $sql .= ' AND contabilidad_vivienda.id_vivienda IN (';
            foreach($_POST["id_vivienda"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vivienda"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

        include('bookkepping_filters.php');

        $sql .= ' GROUP BY categoria ORDER BY categoria';
        
        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['etiqueta'] = $dato['etiqueta'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainHomeConceptChart'){
        //Obtener graficos vivienda por conceptos
        $a_params = array(); 
        $sql = 'SELECT categorias.categoria AS categoria , conceptos.concepto AS etiqueta, SUM(contabilidad.importe) AS importe FROM contabilidad INNER JOIN contabilidad_vivienda ON contabilidad.id=contabilidad_vivienda.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE categorias.tipoContabilidad LIKE ?';
        $a_params[] = $_POST["tipoContabilidad"];

        if(!empty($_POST["id_vivienda"])){
            $sql .= ' AND contabilidad_vivienda.id_vivienda IN (';
            foreach($_POST["id_vivienda"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vivienda"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

        include('bookkepping_filters.php');

        $sql .= ' AND conceptos.id_categoria LIKE ? GROUP BY concepto';
        $a_params[] = $_POST["id_categoria"];
        
        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['etiqueta'] = $dato['etiqueta'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainVehicleCategoryChart'){
        //Obtener graficos vehiculo por categorias
        $a_params = array(); 
        $sql = 'SELECT categorias.categoria AS etiqueta, SUM(contabilidad.importe) AS importe FROM contabilidad INNER JOIN contabilidad_vehiculo ON contabilidad.id=contabilidad_vehiculo.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE categorias.tipoContabilidad LIKE ?';
        $a_params[] = $_POST["tipoContabilidad"];

        if(!empty($_POST["id_vehiculo"])){
            $sql .= ' AND contabilidad_vehiculo.id_vehiculo IN (';
            foreach($_POST["id_vehiculo"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vehiculo"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

        include('bookkepping_filters.php');

        $sql .= ' GROUP BY categoria ORDER BY categoria';
        
        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['etiqueta'] = $dato['etiqueta'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainVehicleConceptChart'){
        //Obtener graficos vivienda por conceptos
        $a_params = array(); 
        $sql = 'SELECT categorias.categoria AS categoria , conceptos.concepto AS etiqueta, SUM(contabilidad.importe) AS importe FROM contabilidad INNER JOIN contabilidad_vehiculo ON contabilidad.id=contabilidad_vehiculo.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE categorias.tipoContabilidad LIKE ?';
        $a_params[] = $_POST["tipoContabilidad"];

        if(!empty($_POST["id_vehiculo"])){
            $sql .= ' AND contabilidad_vehiculo.id_vehiculo IN (';
            foreach($_POST["id_vehiculo"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vehiculo"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

        include('bookkepping_filters.php');

        $sql .= ' AND conceptos.id_categoria LIKE ? GROUP BY concepto';
        $a_params[] = $_POST["id_categoria"];

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['etiqueta'] = $dato['etiqueta'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainGeneralBalance'){

        $a_params = array(); 
        $sql = 'SELECT contabilidad.fecha AS "day", categorias.tipoContabilidad, sum(contabilidad.importe) as importe FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE true';

        include('bookkepping_filters.php');

        $sql .= ' GROUP BY fecha,tipoContabilidad';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {
            //$array_datos[$i]['id'] = $dato['fecha'];
            if(!isset($array_datos[$dato['day']]) ){
                $array_datos[$dato['day']]['ingreso'] = null;
                $array_datos[$dato['day']]['gasto'] = null;
            }
         
            if($dato['tipoContabilidad'] == 'Gastos' ){
                $array_datos[$dato['day']]['gasto'] = $dato['importe'];
            }else if($dato['tipoContabilidad'] == 'Ingresos' ){
                $array_datos[$dato['day']]['ingreso'] = $dato['importe'];
            }

            $i++;
        }
        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainHomeBalance'){
        
        $a_params = array(); 
        $sql = 'SELECT contabilidad.fecha AS "day", categorias.tipoContabilidad, sum(contabilidad.importe) as importe FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria INNER JOIN contabilidad_vivienda ON contabilidad.id=contabilidad_vivienda.id_contabilidad WHERE true';

        include('bookkepping_filters.php');

        $sql .= ' GROUP BY fecha,tipoContabilidad';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {
            //$array_datos[$i]['id'] = $dato['fecha'];
            if(!isset($array_datos[$dato['day']]) ){
                $array_datos[$dato['day']]['ingreso'] = null;
                $array_datos[$dato['day']]['gasto'] = null;
            }
         
            if($dato['tipoContabilidad'] == 'Gastos' ){
                $array_datos[$dato['day']]['gasto'] = $dato['importe'];
            }else if($dato['tipoContabilidad'] == 'Ingresos' ){
                $array_datos[$dato['day']]['ingreso'] = $dato['importe'];
            }

            $i++;
        }
        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainVehicleBalance'){
        
        $a_params = array(); 
        $sql = 'SELECT contabilidad.fecha AS "day", categorias.tipoContabilidad, sum(contabilidad.importe) as importe FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria INNER JOIN contabilidad_vehiculo ON contabilidad.id=contabilidad_vehiculo.id_contabilidad WHERE true';

        include('bookkepping_filters.php');

        
        $sql .= ' GROUP BY fecha,tipoContabilidad';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {
            //$array_datos[$i]['id'] = $dato['fecha'];
            if(!isset($array_datos[$dato['day']]) ){
                $array_datos[$dato['day']]['ingreso'] = null;
                $array_datos[$dato['day']]['gasto'] = null;
            }
         
            if($dato['tipoContabilidad'] == 'Gastos' ){
                $array_datos[$dato['day']]['gasto'] = $dato['importe'];
            }else if($dato['tipoContabilidad'] == 'Ingresos' ){
                $array_datos[$dato['day']]['ingreso'] = $dato['importe'];
            }

            $i++;
        }
        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainVehicleSummary'){
        
        $a_params = array(); 
        $sql = 'SELECT SUM(totalGastos) AS totalGastos, SUM(totalFinanciacion) AS totalFinanciacion, SUM(totalCombustible) AS totalCombustible, SUM(totalMantenimiento) AS totalMantenimiento, SUM(kilometrajeActual) AS kilometrajeActual, SUM(litrosTotales) AS litrosTotales, ((SUM(totalGastos)-SUM(importeInicial))/(SUM(kilometrajeActual)-SUM(kilometrajeInicial))) AS eur_km, ((SUM(litrosTotales)-SUM(litrosIniciales))/((SUM(kilometrajeActual)-SUM(kilometrajeInicial))/100)) AS l_100km, (SUM(totalCombustible)/sum(litrosTotales)) AS eur_l FROM vista_vehiculos WHERE true';

        if(!empty($_POST["id_vehiculo"])){
            $sql .= ' AND id_vehiculo IN (';
            foreach($_POST["id_vehiculo"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vehiculo"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

        //include('bookkepping_filters.php');

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;

            $i++;
        }
        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>