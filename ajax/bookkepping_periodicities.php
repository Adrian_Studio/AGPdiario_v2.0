<?php 
   
    include('obtenRol.php');
    include('../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'new'){
        $date=str_replace('/','-',$_POST["fecha"]);
        $date = new DateTime($date);

        $stmt = $db->prepare("INSERT INTO contabilidad_periodica (id_concepto, periodicidad_meses, fecha_inicio, id_usuario) VALUES (?, ?, ?, ?)");
        $stmt->bind_param('sssi', $_POST["concepto"], $_POST["periodicidad"], $date->format('Y-m-d'), $id_usuario);
        $stmt->execute();
        $concept_Id = $stmt->insert_id;
     
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'delete'){

        $stmt = $db->prepare("DELETE FROM contabilidad_periodica WHERE id = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();


    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'update'){
        $date=str_replace('/','-',$_POST["fecha"]);
        $date = new DateTime($date);

        $stmt = $db->prepare("UPDATE contabilidad_periodica SET id_concepto = ?, periodicidad_meses = ?, fecha_inicio = ? WHERE id = ?");
        $stmt->bind_param('sssi', $_POST["concepto"], $_POST["periodicidad"], $date->format('Y-m-d'), $_POST["editTag"]);
        $stmt->execute();
        
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainOne'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT categorias.categoria, conceptos.id_categoria, contabilidad_periodica.*, conceptos.concepto FROM contabilidad_periodica INNER JOIN conceptos ON conceptos.id = contabilidad_periodica.id_concepto INNER JOIN categorias ON conceptos.id_categoria = categorias.id WHERE contabilidad_periodica.id LIKE ? AND categorias.id_usuario LIKE ?';
        $a_params[] = $_POST["table_id"];
        $a_params[] = $id_usuario;

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainTable'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT categorias.categoria, conceptos.id_categoria, contabilidad_periodica.*, conceptos.concepto FROM contabilidad_periodica INNER JOIN conceptos ON conceptos.id = contabilidad_periodica.id_concepto INNER JOIN categorias ON conceptos.id_categoria = categorias.id WHERE categorias.id_usuario LIKE ?';
        $a_params[] = $id_usuario;
        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainDataPeriodicities'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT * FROM contabilidad WHERE id_concepto LIKE ? AND fecha >= ? AND fecha <= ? AND id_usuario LIKE ?';
        $a_params[] = $_POST["id_concepto"];
        $a_params[] = $_POST["fechaInicio"];
        $a_params[] = $_POST["fechaFinal"];
        $a_params[] = $id_usuario;


        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>