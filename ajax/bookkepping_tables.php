<?php 
   
    include('obtenRol.php');
    include('../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainGeneralTable'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT contabilidad.*, categorias.categoria AS categoria , conceptos.concepto AS concepto FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE categorias.tipoContabilidad LIKE ?';
        $a_params[] = $_POST["tipoContabilidad"];

        include('bookkepping_filters.php');

        $sql .= ' ORDER BY contabilidad.fecha DESC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['fecha'] = $dato['fecha'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['concepto'] = $dato['concepto'];
            $array_datos[$i]['observaciones'] = $dato['observaciones'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainHomeTable'){
       //Obtener tablas generales de vivienda
       $a_params = array(); 
       $sql = 'SELECT contabilidad.*, contabilidad_vivienda.*, categorias.categoria AS categoria, categorias.tipoContabilidad, conceptos.concepto AS concepto FROM contabilidad INNER JOIN contabilidad_vivienda ON contabilidad.id=contabilidad_vivienda.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria WHERE categorias.tipoContabilidad LIKE ?';
       $a_params[] = $_POST["tipoContabilidad"];

        if(!empty($_POST["id_vivienda"])){
            $sql .= ' AND contabilidad_vivienda.id_vivienda IN (';
            foreach($_POST["id_vivienda"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vivienda"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

       include('bookkepping_filters.php');

       $sql .= ' ORDER BY contabilidad.fecha DESC';

       $stmt = $db->prepare($sql);
       $stmt = bindVariablesSystem($stmt, $a_params);
       $stmt->execute();
       $result = $stmt->get_result();

       $i = 0;
       $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id_contabilidad'];
            $array_datos[$i]['fecha'] = $dato['fecha'];
            $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['concepto'] = $dato['concepto'];
            $array_datos[$i]['observaciones'] = $dato['observaciones'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainGasTable'){
        //Obtener tablas gasolina de vehiculos
        $a_params = array(); 
        $sql = 'SELECT contabilidad.*, contabilidad_vehiculo.*, categorias.categoria AS categoria, categorias.tipoContabilidad, conceptos.concepto AS concepto FROM contabilidad INNER JOIN contabilidad_vehiculo ON contabilidad.id=contabilidad_vehiculo.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria INNER JOIN conceptos_vehiculo ON conceptos_vehiculo.id_concepto=conceptos.id WHERE conceptos_vehiculo.tipo LIKE ?';
        $a_params[] = $_POST["tipoConcepto"];

        if(!empty($_POST["id_vehiculo"])){
            $sql .= ' AND contabilidad_vehiculo.id_vehiculo IN (';
            foreach($_POST["id_vehiculo"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vehiculo"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

        include('bookkepping_filters.php');

        $sql .= ' ORDER BY id_vehiculo, contabilidad.fecha DESC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id_contabilidad'];
            $array_datos[$i]['fecha'] = $dato['fecha'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['concepto'] = $dato['concepto'];
            $array_datos[$i]['observaciones'] = $dato['observaciones'];
            $array_datos[$i]['cuenta_km'] = $dato['cuenta_km'];
            $array_datos[$i]['litros'] = $dato['litros'];
            $array_datos[$i]['id_vehiculo'] = $dato['id_vehiculo'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainMaintenanceTable'){
        //Obtener tablas mantenimiento de vehiculos
        $a_params = array(); 
        $sql = 'SELECT contabilidad.*, contabilidad_vehiculo.*, categorias.categoria AS categoria, categorias.tipoContabilidad, conceptos.concepto AS concepto FROM contabilidad INNER JOIN contabilidad_vehiculo ON contabilidad.id=contabilidad_vehiculo.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria INNER JOIN conceptos_vehiculo ON conceptos_vehiculo.id_concepto=conceptos.id WHERE conceptos_vehiculo.tipo LIKE ?';
        $a_params[] = $_POST["tipoConcepto"];

        if(!empty($_POST["id_vehiculo"])){
            $sql .= ' AND contabilidad_vehiculo.id_vehiculo IN (';
            foreach($_POST["id_vehiculo"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vehiculo"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

        include('bookkepping_filters.php');

        $sql .= ' ORDER BY contabilidad.fecha DESC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id_contabilidad'];
            $array_datos[$i]['fecha'] = $dato['fecha'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['concepto'] = $dato['concepto'];
            $array_datos[$i]['observaciones'] = $dato['observaciones'];
            $array_datos[$i]['cuenta_km'] = $dato['cuenta_km'];
            $array_datos[$i]['litros'] = $dato['litros'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainFinanciationTable'){
        //Obtener tablas financiacion de vehiculos
        $a_params = array(); 
        $sql = 'SELECT contabilidad.*, contabilidad_vehiculo.*, categorias.categoria AS categoria, categorias.tipoContabilidad, conceptos.concepto AS concepto FROM contabilidad INNER JOIN contabilidad_vehiculo ON contabilidad.id=contabilidad_vehiculo.id_contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria INNER JOIN conceptos_vehiculo ON conceptos_vehiculo.id_concepto=conceptos.id WHERE conceptos_vehiculo.tipo LIKE ?';
        $a_params[] = $_POST["tipoConcepto"];

        if(!empty($_POST["id_vehiculo"])){
            $sql .= ' AND contabilidad_vehiculo.id_vehiculo IN (';
            foreach($_POST["id_vehiculo"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vehiculo"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

        include('bookkepping_filters.php');

        $sql .= ' ORDER BY contabilidad.fecha DESC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id_contabilidad'];
            $array_datos[$i]['fecha'] = $dato['fecha'];
            $array_datos[$i]['importe'] = $dato['importe'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['concepto'] = $dato['concepto'];
            $array_datos[$i]['observaciones'] = $dato['observaciones'];
            $array_datos[$i]['cuenta_km'] = $dato['cuenta_km'];
            $array_datos[$i]['litros'] = $dato['litros'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainTotalSummaryTable'){
        //Obtener tablas resumen por años
        $a_params = array(); 
        $sql = 'SELECT contabilidad.*, YEAR(contabilidad.fecha) AS año, categorias.categoria AS categoria, categorias.tipoContabilidad, conceptos.concepto AS concepto FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria';
        if($_POST["grupo"] == 'Vivienda'){
            $sql .= ' INNER JOIN contabilidad_vivienda ON contabilidad.id=contabilidad_vivienda.id_contabilidad';
        }else if($_POST["grupo"] == 'Vehiculo'){
            $sql .= ' INNER JOIN contabilidad_vehiculo ON contabilidad.id=contabilidad_vehiculo.id_contabilidad';
        }
        $sql .= ' WHERE true';

        if(!empty($_POST["id_vivienda"])){
            $sql .= ' AND contabilidad_vivienda.id_vivienda IN (';
            foreach($_POST["id_vivienda"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vivienda"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }else if(!empty($_POST["id_vehiculo"])){
            $sql .= ' AND contabilidad_vehiculo.id_vehiculo IN (';
            foreach($_POST["id_vehiculo"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vehiculo"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }

        include('bookkepping_filters.php');

        $sql .= ' ORDER BY conceptos.id_categoria, contabilidad.id_concepto, fecha DESC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        $array_columnas = array();
        
        while($dato = $result->fetch_assoc()) {
            if( !in_array($dato["año"], $array_columnas) ){
                $array_columnas[] = $dato["año"];
            }

            if(!isset($array_datos["Balance"]["GRUPO"]) ){
                $array_datos["Balance"]["GRUPO"] = "Balance";
                $array_datos["Balance"]["Class"] = "Balance";
            }
            if(!isset($array_datos["Balance"]["Total"]) ){
                $array_datos["Balance"]["Total"] = 0;
            }
            if(!isset($array_datos["Balance"][$dato["año"]]) ){
                $array_datos["Balance"][$dato["año"]] = 0;
            }
            if ($dato["tipoContabilidad"] == 'Ingresos'){
                $array_datos["Balance"]["Total"] += $dato["importe"];
                $array_datos["Balance"][$dato["año"]] += $dato["importe"];
            }else if($dato["tipoContabilidad"] == 'Gastos'){
                $array_datos["Balance"]["Total"] -= $dato["importe"];
                $array_datos["Balance"][$dato["año"]] -= $dato["importe"];
            }
            if(!isset($array_datos[$dato["tipoContabilidad"]]["GRUPO"]) ){
                $array_datos[$dato["tipoContabilidad"]]["GRUPO"] = $dato["tipoContabilidad"];
                $array_datos[$dato["tipoContabilidad"]]["Class"] = $dato["tipoContabilidad"];
            }
            if(!isset($array_datos[$dato["tipoContabilidad"]]["Total"]) ){
                $array_datos[$dato["tipoContabilidad"]]["Total"] = 0;
            }
            if(!isset($array_datos[$dato["tipoContabilidad"]][$dato["año"]]) ){
                $array_datos[$dato["tipoContabilidad"]][$dato["año"]] = 0;
            }
            $array_datos[$dato["tipoContabilidad"]]["Total"] += $dato["importe"];
            $array_datos[$dato["tipoContabilidad"]][$dato["año"]] += $dato["importe"];
            if(!isset($array_datos[$dato["categoria"]]["GRUPO"]) ){
                $array_datos[$dato["categoria"]]["GRUPO"] = $dato["categoria"];
                $array_datos[$dato["categoria"]]["Class"] = 'Categoria';
            }
            if(!isset($array_datos[$dato["categoria"]]["Total"]) ){
                $array_datos[$dato["categoria"]]["Total"] = 0;
            }
            if(!isset($array_datos[$dato["categoria"]][$dato["año"]]) ){
                $array_datos[$dato["categoria"]][$dato["año"]] = 0;
            }
            $array_datos[$dato["categoria"]]["Total"] += $dato["importe"];
            $array_datos[$dato["categoria"]][$dato["año"]] += $dato["importe"];
            if(!isset($array_datos[$dato["concepto"]]["GRUPO"]) ){
                $array_datos[$dato["concepto"]]["GRUPO"] = $dato["concepto"];
                $array_datos[$dato["concepto"]]["Class"] = 'Concepto';
            }
            if(!isset($array_datos[$dato["concepto"]]["Total"]) ){
                $array_datos[$dato["concepto"]]["Total"] = 0;
            }
            if(!isset($array_datos[$dato["concepto"]][$dato["año"]]) ){
                $array_datos[$dato["concepto"]][$dato["año"]] = 0;
            }
            $array_datos[$dato["concepto"]]["Total"] += $dato["importe"];
            $array_datos[$dato["concepto"]][$dato["año"]] += $dato["importe"];

            $i++;
        }
        $array_datos['columnas']= $array_columnas;

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainAnnualSummaryTable'){
        //Obtener tablas resumen por meses
        $a_params = array(); 
        $sql = 'SELECT contabilidad.*, MONTH(contabilidad.fecha) AS mes, categorias.categoria AS categoria, categorias.tipoContabilidad, conceptos.concepto AS concepto FROM contabilidad INNER JOIN conceptos ON conceptos.id=contabilidad.id_concepto INNER JOIN categorias ON categorias.id=conceptos.id_categoria';
        if($_POST["grupo"] == 'Vivienda'){
            $sql .= ' INNER JOIN contabilidad_vivienda ON contabilidad.id=contabilidad_vivienda.id_contabilidad';
        }else if($_POST["grupo"] == 'Vehiculo'){
            $sql .= ' INNER JOIN contabilidad_vehiculo ON contabilidad.id=contabilidad_vehiculo.id_contabilidad';
        }
        $sql .= ' WHERE true';

        if(!empty($_POST["id_vivienda"])){
            $sql .= ' AND contabilidad_vivienda.id_vivienda IN (';
            foreach($_POST["id_vivienda"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vivienda"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }else if(!empty($_POST["id_vehiculo"])){
            $sql .= ' AND contabilidad_vehiculo.id_vehiculo IN (';
            foreach($_POST["id_vehiculo"] as $key=>$valor){
                $sql .= '?';
                if($key != count($_POST["id_vehiculo"]) - 1 ){
                    $sql .= ','; 
                }
                $a_params[] = $valor;
            }
            $sql .= ' )';
        }
        
        include('bookkepping_filters.php');
        
        $sql .= ' ORDER BY categorias.tipoContabilidad DESC, conceptos.id_categoria, contabilidad.id_concepto, fecha DESC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        $array_columnas = array();
        
        while($dato = $result->fetch_assoc()) {
            if( !in_array($dato["mes"], $array_columnas) ){
                $array_columnas[] = $dato["mes"];
            }

            if(!isset($array_datos["Balance"]["GRUPO"]) ){
                $array_datos["Balance"]["GRUPO"] = "Balance";
                $array_datos["Balance"]["Class"] = "Balance";
            }
            if(!isset($array_datos["Balance"]["Total"]) ){
                $array_datos["Balance"]["Total"] = 0;
            }
            if(!isset($array_datos["Balance"][$dato["mes"]]) ){
                $array_datos["Balance"][$dato["mes"]] = 0;
            }
            if ($dato["tipoContabilidad"] == 'Ingresos'){
                $array_datos["Balance"]["Total"] += $dato["importe"];
                $array_datos["Balance"][$dato["mes"]] += $dato["importe"];
            }else if($dato["tipoContabilidad"] == 'Gastos'){
                $array_datos["Balance"]["Total"] -= $dato["importe"];
                $array_datos["Balance"][$dato["mes"]] -= $dato["importe"];
            }
            if(!isset($array_datos[$dato["tipoContabilidad"]]["GRUPO"]) ){
                $array_datos[$dato["tipoContabilidad"]]["GRUPO"] = $dato["tipoContabilidad"];
                $array_datos[$dato["tipoContabilidad"]]["Class"] = $dato["tipoContabilidad"];
            }
            if(!isset($array_datos[$dato["tipoContabilidad"]]["Total"]) ){
                $array_datos[$dato["tipoContabilidad"]]["Total"] = 0;
            }
            if(!isset($array_datos[$dato["tipoContabilidad"]][$dato["mes"]]) ){
                $array_datos[$dato["tipoContabilidad"]][$dato["mes"]] = 0;
            }
            $array_datos[$dato["tipoContabilidad"]]["Total"] += $dato["importe"];
            $array_datos[$dato["tipoContabilidad"]][$dato["mes"]] += $dato["importe"];
            if(!isset($array_datos[$dato["categoria"]]["GRUPO"]) ){
                $array_datos[$dato["categoria"]]["GRUPO"] = $dato["categoria"];
                $array_datos[$dato["categoria"]]["Class"] = 'Categoria';
            }
            if(!isset($array_datos[$dato["categoria"]]["Total"]) ){
                $array_datos[$dato["categoria"]]["Total"] = 0;
            }
            if(!isset($array_datos[$dato["categoria"]][$dato["mes"]]) ){
                $array_datos[$dato["categoria"]][$dato["mes"]] = 0;
            }
            $array_datos[$dato["categoria"]]["Total"] += $dato["importe"];
            $array_datos[$dato["categoria"]][$dato["mes"]] += $dato["importe"];
            if(!isset($array_datos[$dato["concepto"]]["GRUPO"]) ){
                $array_datos[$dato["concepto"]]["GRUPO"] = $dato["concepto"];
                $array_datos[$dato["concepto"]]["Class"] = 'Concepto';
            }
            if(!isset($array_datos[$dato["concepto"]]["Total"]) ){
                $array_datos[$dato["concepto"]]["Total"] = 0;
            }
            if(!isset($array_datos[$dato["concepto"]][$dato["mes"]]) ){
                $array_datos[$dato["concepto"]][$dato["mes"]] = 0;
            }
            $array_datos[$dato["concepto"]]["Total"] += $dato["importe"];
            $array_datos[$dato["concepto"]][$dato["mes"]] += $dato["importe"];

            $i++;
        }
        $array_datos['columnas']= $array_columnas;

        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>