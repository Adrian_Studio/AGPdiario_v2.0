<?php 
   
    include('../obtenRol.php');
    include('../../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'new'){

        $stmt = $db->prepare("INSERT INTO categorias (tipoContabilidad, categoria, paraVivienda, paraVehiculo, id_usuario) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param('ssiii', $_POST["editTag"], $_POST["categoria"], $_POST["paraVivienda"], $_POST["paraVehiculo"], $id_usuario);
        $stmt->execute();
     
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'delete'){

        $stmt = $db->prepare("DELETE FROM categorias WHERE id = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'update'){

        $stmt = $db->prepare("UPDATE categorias SET categoria = ? WHERE id = ?");
        $stmt->bind_param('si', $_POST["categoria"], $_POST["editTag"]);
        $stmt->execute();
        
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainOne'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT * FROM categorias WHERE id LIKE ? AND id_usuario LIKE ?';
        $a_params[] = $_POST["table_id"];
        $a_params[] = $id_usuario;

        $sql .= ' ORDER BY categoria ASC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['paraVivienda'] = $dato['paraVivienda'];
            $array_datos[$i]['paraVehiculo'] = $dato['paraVehiculo'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainTable'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT * FROM categorias WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        $sql .= ' ORDER BY categoria ASC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {
            
            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['paraVivienda'] = $dato['paraVivienda'];
            $array_datos[$i]['paraVehiculo'] = $dato['paraVehiculo'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'checkIf'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT * FROM categorias WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        if(!empty($_POST["grupo"]) && $_POST["grupo"] == 'paraVivienda') {
            $sql .= ' AND paraVivienda';
        }
        if(!empty($_POST["grupo"]) && $_POST["grupo"] == 'paraVehiculo') {
            $sql .= ' AND paraVehiculo';
        }

        $sql .= ' ORDER BY categoria ASC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['paraVivienda'] = $dato['paraVivienda'];
            $array_datos[$i]['paraVehiculo'] = $dato['paraVehiculo'];
            $i++;
        }

        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>