<?php 
   
    include('../obtenRol.php');
    include('../../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'new'){

        $stmt = $db->prepare("INSERT INTO conceptos (id_categoria, concepto) VALUES (?, ?)");
        $stmt->bind_param('is', $_POST["categoria"], $_POST["concepto"]);
        $stmt->execute();
        $concept_id = $stmt->insert_id;

        $stmt = $db->prepare("INSERT INTO conceptos_vehiculo (id_concepto, tipo) VALUES (?, ?)");
        $stmt->bind_param('is', $concept_id, $_POST["tipoConcepto"]);
        $stmt->execute();
     
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'delete'){

        $stmt = $db->prepare("DELETE FROM conceptos WHERE id = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();

        $stmt = $db->prepare("DELETE FROM conceptos_vehiculo WHERE id_concepto = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();


    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'update'){

        $stmt = $db->prepare("UPDATE conceptos SET id_categoria = ?, concepto = ? WHERE id = ?");
        $stmt->bind_param('isi', $_POST["categoria"], $_POST["concepto"], $_POST["editTag"]);
        $stmt->execute();

        $stmt = $db->prepare("UPDATE conceptos_vehiculo SET tipo = ? WHERE id_concepto = ?");
        $stmt->bind_param('si', $_POST["tipoConcepto"], $_POST["editTag"]);
        $stmt->execute();
        
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainOne'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT categorias.tipoContabilidad, categorias.paraVivienda, categorias.paraVehiculo, conceptos.id, conceptos.id_categoria, conceptos.concepto, conceptos_vehiculo.tipo FROM `conceptos` INNER JOIN categorias ON categorias.id=conceptos.id_categoria LEFT JOIN conceptos_vehiculo ON conceptos.id=conceptos_vehiculo.id_concepto WHERE conceptos.id LIKE ?  AND id_usuario LIKE ?';
        $a_params[] = $_POST["table_id"];
        $a_params[] = $id_usuario;

        $sql .= ' ORDER BY concepto ASC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
            $array_datos[$i]['categoria'] = $dato['id_categoria'];
            $array_datos[$i]['concepto'] = $dato['concepto'];
            $array_datos[$i]['tipo'] = $dato['tipo'];
            $array_datos[$i]['paraVivienda'] = $dato['paraVivienda'];
            $array_datos[$i]['paraVehiculo'] = $dato['paraVehiculo'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainTable'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT categorias.tipoContabilidad,categorias.categoria ,conceptos.id ,conceptos.concepto, conceptos_vehiculo.tipo FROM `conceptos` INNER JOIN categorias ON categorias.id=conceptos.id_categoria LEFT JOIN conceptos_vehiculo ON conceptos.id=conceptos_vehiculo.id_concepto WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;
        $sql .= ' ORDER BY concepto ASC';

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
            $array_datos[$i]['categoria'] = $dato['categoria'];
            $array_datos[$i]['concepto'] = $dato['concepto'];
            $array_datos[$i]['tipo'] = $dato['tipo'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainCategories'){
        //Obtener categorias
        $a_params = array(); 
        $sql = 'SELECT * FROM categorias WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        if(!empty($_POST["tipoContabilidad"])){
            $sql .= ' AND tipoContabilidad LIKE ?';
            $a_params[] = $_POST["tipoContabilidad"];
        }   
        if(!empty($_POST["grupo"]) && $_POST["grupo"] == 'Vivienda'){
            $sql .= ' AND paraVivienda';
        }   
        if(!empty($_POST["grupo"]) && $_POST["grupo"] == 'Vehiculo'){
            $sql .= ' AND paraVehiculo';
        }  
        if(!empty($_POST["grupo"]) && $_POST["grupo"] == 'General') {
            $sql .= ' AND paraVivienda LIKE false AND paraVehiculo LIKE false';
        }


        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['tipoContabilidad'] = $dato['tipoContabilidad'];
            $array_datos[$i]['texto'] = $dato['categoria'];
            $array_datos[$i]['paraVivienda'] = $dato['paraVivienda'];
            $array_datos[$i]['paraVehiculo'] = $dato['paraVehiculo'];


            $i++;
        }

        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>