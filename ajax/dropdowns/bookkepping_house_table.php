<?php 
   
    include('../obtenRol.php');
    include('../../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'new'){

        $stmt = $db->prepare("INSERT INTO viviendas (calle, portal, piso, id_usuario) VALUES (?, ?, ?, ?)");
        $stmt->bind_param('sssi', $_POST["calle"], $_POST["portal"], $_POST["piso"], $id_usuario);
        $stmt->execute();
        $concept_Id = $stmt->insert_id;
     
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'delete'){

        $stmt = $db->prepare("DELETE FROM viviendas WHERE id = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();


    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'update'){

        $stmt = $db->prepare("UPDATE viviendas SET calle = ?, portal = ?, piso = ? WHERE id = ?");
        $stmt->bind_param('sssi', $_POST["calle"], $_POST["portal"], $_POST["piso"], $_POST["editTag"]);
        $stmt->execute();
        
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainOne'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT * FROM viviendas WHERE id LIKE ? AND id_usuario LIKE ?';
        $a_params[] = $_POST["table_id"];
        $a_params[] = $id_usuario;

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainTable'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT * FROM viviendas WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>