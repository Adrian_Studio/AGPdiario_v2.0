<?php 
   
    include('obtenRol.php');
    include('../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
    
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'new'){
        $date_ad=str_replace('/','-',$_POST["fecha_adquisicion"]);
        $date_ad = new DateTime($date_ad);     
        $date_ad = $date_ad->format('Y-m-d');
        if($_POST["fecha_desecho"]){
            $date_des = str_replace('/','-',$_POST["fecha_desecho"]);
            $date_des = new DateTime($date_des);
            $date_des = $date_des->format('Y-m-d');
        }else{
            $date_des = NULL;
        };

        $stmt = $db->prepare("INSERT INTO inventario (id_ubicacion, id_tipo_elemento, elemento, lugar_adquisicion, modelo, identificativo, fecha_adquisicion, valor_adquisicion, fecha_desecho, valor_residual, estado, cantidad, observaciones, id_usuario) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('iisssssdsdsisi', $_POST["ubicacion"], $_POST["tipoElemento"], $_POST["elemento"], $_POST["lugar_adquisicion"], $_POST["modelo"], $_POST["identificativo"], $date_ad, $_POST["valor_adquisicion"], $date_des, $_POST["valor_residual"], $_POST["estado"], $_POST["cantidad"], $_POST["observaciones"], $id_usuario);
        $stmt->execute();
      
        
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'update'){
        $date_ad=str_replace('/','-',$_POST["fecha_adquisicion"]);
        $date_ad = new DateTime($date_ad);
        $date_ad = $date_ad->format('Y-m-d');
        if($_POST["fecha_desecho"]){
            $date_des = str_replace('/','-',$_POST["fecha_desecho"]);
            $date_des = new DateTime($date_des);
            $date_des = $date_des->format('Y-m-d');
        }else{
            $date_des = NULL;
        };

        $stmt = $db->prepare("UPDATE inventario SET id_ubicacion = ?, id_tipo_elemento = ?, elemento = ?, lugar_adquisicion = ?, modelo = ?, identificativo = ?, fecha_adquisicion = ?, valor_adquisicion = ?, fecha_desecho = ?, valor_residual = ?, estado = ?, cantidad = ?, observaciones = ? WHERE id = ?");
        $stmt->bind_param('iisssssdsdsisi', $_POST["ubicacion"], $_POST["tipoElemento"], $_POST["elemento"], $_POST["lugar_adquisicion"], $_POST["modelo"], $_POST["identificativo"], $date_ad, $_POST["valor_adquisicion"], $date_des, $_POST["valor_residual"], $_POST["estado"], $_POST["cantidad"], $_POST["observaciones"], $_POST['editTag']);
        $stmt->execute();


    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'delete'){

        $stmt = $db->prepare("DELETE FROM inventario WHERE id = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();


    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainTable'){

        $stmt = $db->prepare("SELECT inventario.*, ubicaciones.ubicacion, tipos_elemento.tipo FROM inventario INNER JOIN ubicaciones ON ubicaciones.id=inventario.id_ubicacion INNER JOIN tipos_elemento ON tipos_elemento.id=inventario.id_tipo_elemento WHERE inventario.id_usuario LIKE ?");
        $stmt->bind_param('i', $id_usuario);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $elementos = array();
        while($item = $result->fetch_assoc()) {
            $elementos[$i]['id'] = $item['id'];
            $elementos[$i]['ubicacion'] = $item['ubicacion'];
            $elementos[$i]['tipoElemento'] = $item['tipo'];
            $elementos[$i]['elemento'] = $item['elemento'];
            $elementos[$i]['lugar_adquisicion'] = $item['lugar_adquisicion'];
            $elementos[$i]['modelo'] = $item['modelo'];
            $elementos[$i]['identificativo'] = $item['identificativo'];
            $elementos[$i]['fecha_adquisicion'] = $item['fecha_adquisicion'];
            $elementos[$i]['valor_adquisicion'] = $item['valor_adquisicion'];;
            $elementos[$i]['fecha_desecho'] = $item['fecha_desecho'];
            $elementos[$i]['valor_residual'] = $item['valor_residual'];
            $elementos[$i]['estado'] = $item['estado'];
            $elementos[$i]['cantidad'] = $item['cantidad'];
            $elementos[$i]['observaciones'] = $item['observaciones'];
            $i++;
        }

        echo json_encode($elementos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainOne'){

        $stmt = $db->prepare("SELECT * FROM inventario INNER JOIN ubicaciones ON ubicaciones.id=inventario.id_ubicacion INNER JOIN tipos_elemento ON tipos_elemento.id=inventario.id_tipo_elemento WHERE inventario.id = ? AND inventario.id_usuario LIKE ?");
        $stmt->bind_param('ii', $_POST['table_id'], $id_usuario);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $elementos = array();
        while($item = $result->fetch_assoc()) {
            $elementos[$i]['id'] = $item['id'];
            $elementos[$i]['id_ubicacion'] = $item['id_ubicacion'];
            $elementos[$i]['id_tipo_elemento'] = $item['id_tipo_elemento'];
            $elementos[$i]['elemento'] = $item['elemento'];
            $elementos[$i]['lugar_adquisicion'] = $item['lugar_adquisicion'];
            $elementos[$i]['modelo'] = $item['modelo'];
            $elementos[$i]['identificativo'] = $item['identificativo'];
            $elementos[$i]['fecha_adquisicion'] = $item['fecha_adquisicion'];
            $elementos[$i]['valor_adquisicion'] = $item['valor_adquisicion'];;
            $elementos[$i]['fecha_desecho'] = $item['fecha_desecho'];
            $elementos[$i]['valor_residual'] = $item['valor_residual'];
            $elementos[$i]['estado'] = $item['estado'];
            $elementos[$i]['cantidad'] = $item['cantidad'];
            $elementos[$i]['observaciones'] = $item['observaciones'];
            $i++;
        }
    

        echo json_encode($elementos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainPlaces'){
       
        $a_params = array(); 
        $sql = 'SELECT * FROM ubicaciones WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['texto'] = $dato['ubicacion'];
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainElementTypes'){
        
        $a_params = array(); 
        $sql = 'SELECT * FROM tipos_elemento WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['texto'] = $dato['tipo'];
            $i++;
        }

        echo json_encode($array_datos);

    }

    Conexiones::closeConnection($db);

?>

