<?php 
   
    include('obtenRol.php');
    include('../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'new'){
        $date=str_replace('/','-',$_POST["fecha"]);
        $date = new DateTime($date);

        $stmt = $db->prepare("INSERT INTO notas (nota, aviso_activo, fecha_aviso, tiempo_aviso_dias, periodicidad_activa, periodicidad_meses, clasificacion, id_usuario) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('sisiiisi', $_POST["nota"], $_POST["aviso_activo"], $date->format('Y-m-d'), $_POST["tiempo_aviso"], $_POST["periodicidad_activa"], $_POST["periodicidad_meses"], $_POST["clasificacion"], $id_usuario);
        $stmt->execute();
     
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'delete'){

        $stmt = $db->prepare("DELETE FROM notas WHERE id = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();


    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'update'){
        $date=str_replace('/','-',$_POST["fecha"]);
        $date = new DateTime($date);

        $stmt = $db->prepare("UPDATE notas SET nota = ?, aviso_activo = ?, fecha_aviso = ?, tiempo_aviso_dias = ?, periodicidad_activa = ?, periodicidad_meses = ?, clasificacion = ? WHERE id = ?");
        $stmt->bind_param('sisiiisi', $_POST["nota"], $_POST["aviso_activo"], $date->format('Y-m-d'), $_POST["tiempo_aviso"], $_POST["periodicidad_activa"], $_POST["periodicidad_meses"], $_POST["clasificacion"], $_POST["editTag"]);
        $stmt->execute();
        
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainOne'){

        $a_params = array(); 
        $sql = 'SELECT * FROM notas WHERE id LIKE ? AND id_usuario LIKE ?';
        $a_params[] = $_POST["table_id"];
        $a_params[] = $id_usuario;

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainTable'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT * FROM notas WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainNotesWithAlert'){

        $stmt = $db->prepare("SELECT * FROM notas WHERE aviso_activo AND id_usuario LIKE ?");
        $a_params[] = $id_usuario;
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        while($dato = $result->fetch_assoc()) {
            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>