<?php 

function roles(){
    session_start();
    if(!empty($_SESSION['rol']) && $_SESSION['rol'] == 'Administrador'){
        return true;
    }else{
        return false;
    }
}
function usuario(){
    if(!empty($_SESSION['rol'])){
        return $_SESSION['id'];
    }else{
        return false;
    }
}

function bindVariablesSystem($statement, $params){
    if ($params != null)
    {
        // Generate the Type String (eg: 'issisd')
        $types = '';
        foreach($params as $param)
        {
            if(is_int($param)) {
                // Integer
                $types .= 'i';
            } elseif (is_float($param)) {
                // Double
                $types .= 'd';
            } elseif (is_string($param)) {
                // String
                $types .= 's';
            } else {
                // Blob and Unknown
                $types .= 'b';
            }
        }
  
        // Add the Type String as the first Parameter
        $bind_names[] = $types;
  
        // Loop thru the given Parameters
        for ($i=0; $i<count($params);$i++)
        {
            // Create a variable Name
            $bind_name = 'bind' . $i;
            // Add the Parameter to the variable Variable
            $$bind_name = $params[$i];
            // Associate the Variable as an Element in the Array
            $bind_names[] = &$$bind_name;
        }
         
        // Call the Function bind_param with dynamic Parameters
        call_user_func_array(array($statement,'bind_param'), $bind_names);
    }
    return $statement;
}

?>