<?php 
   
    include('obtenRol.php');
    include('../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'new'){
        $date=str_replace('/','-',$_POST["fecha"]);
        $date = new DateTime($date);

        $stmt = $db->prepare("INSERT INTO viajes (fecha, latitud, longitud, lugar, codigo_pais, categoria, acompañantes, id_usuario) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('sssssssi', $date->format('Y-m-d'), $_POST["latitud"], $_POST["longitud"], $_POST["lugar"], $_POST["pais"], $_POST["categoria"], $_POST["acompañantes"], $id_usuario);
        $stmt->execute();
        $concept_Id = $stmt->insert_id;
     
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'delete'){

        $stmt = $db->prepare("DELETE FROM viajes WHERE id = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();


    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'update'){
        $date=str_replace('/','-',$_POST["fecha"]);
        $date = new DateTime($date);

        $stmt = $db->prepare("UPDATE viajes SET fecha = ?, latitud = ?, longitud = ?, lugar = ?, codigo_pais = ?, categoria = ?, acompañantes = ? WHERE id = ?");
        $stmt->bind_param('sssssssi', $date->format('Y-m-d'), $_POST["latitud"], $_POST["longitud"], $_POST["lugar"], $_POST["pais"], $_POST["categoria"], $_POST["acompañantes"], $_POST["editTag"]);
        $stmt->execute();
        
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainOne'){

        $a_params = array(); 
        $sql = 'SELECT * FROM viajes WHERE id LIKE ? AND id_usuario LIKE ?';
        $a_params[] = $_POST["table_id"];
        $a_params[] = $id_usuario;

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainTable'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT viajes.*, paises.nombre AS pais FROM viajes INNER JOIN paises ON paises.codigo=viajes.codigo_pais WHERE id_usuario LIKE ?';
        $a_params[] = $id_usuario;

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainPlaces'){

        $stmt = $db->prepare("SELECT viajes.*, paises.nombre FROM viajes INNER JOIN paises ON paises.codigo = viajes.codigo_pais WHERE id_usuario LIKE ? ORDER BY FIELD(categoria, 'Trabajo', 'Visitado', 'Programado', 'Viviendo')");
        $a_params[] = $id_usuario;

        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        while($dato = $result->fetch_assoc()) {
            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainProgramedTrips'){

        $stmt = $db->prepare("SELECT viajes.*, paises.nombre FROM viajes INNER JOIN paises ON paises.codigo = viajes.codigo_pais WHERE categoria LIKE 'Programado' AND id_usuario LIKE ? ORDER BY fecha");
        $a_params[] = $id_usuario;

        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        while($dato = $result->fetch_assoc()) {
            $array_datos[$i] = $dato;
            $i++;
        }

        echo json_encode($array_datos);

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainCountries'){

        $stmt = $db->prepare("SELECT * FROM paises ORDER BY nombre");
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        while($dato = $result->fetch_assoc()) {
            $array_datos[$i]['id'] = $dato['codigo'];
            $array_datos[$i]['texto'] = $dato['nombre'];
            $i++;
        }

        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>