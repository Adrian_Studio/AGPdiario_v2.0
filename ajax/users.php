<?php 
   
    include('obtenRol.php');
    include('../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    $usuario = roles();
    $id_usuario = usuario();
     /**
     * $_POST['tipo'] == 'obtain' --> Obtiene los datos de una transaccion en concreto
     * $_POST['tipo'] == 'update' --> Actualiza la informacion de una transaccion
     */
    if(!empty($_POST['tipo']) && $_POST['tipo'] == 'new' && $usuario){
        $imagenEscapes = '';
        if(!empty($_FILES['image-file']['tmp_name'])){
            $imagenEscapes = file_get_contents($_FILES['image-file']['tmp_name']);

            /********************************************************************* Comienzo del resize de la imagen *********************************************************************/
            // Create image from file
            switch(strtolower($_FILES['image-file']['type']))
            {
                case 'image/jpeg':
                    $image = imagecreatefromjpeg($_FILES['image-file']['tmp_name']);
                    break;
                case 'image/png':
                    $image = imagecreatefrompng($_FILES['image-file']['tmp_name']);
                    break;
                case 'image/gif':
                    $image = imagecreatefromgif($_FILES['image-file']['tmp_name']);
                    break;
                default:
                    exit('Unsupported type: '.$_FILES['image-file']['type']);
            }

            // Target dimensions
            $max_width = 370;
            $max_height = 280;

            // Get current dimensions
            $old_width  = imagesx($image);
            $old_height = imagesy($image);

            // Calculate the scaling we need to do to fit the image inside our frame
            $scale      = min($max_width/$old_width, $max_height/$old_height);

            // Get the new dimensions
            $new_width  = ceil($scale*$old_width);
            $new_height = ceil($scale*$old_height);

            // Create new empty image
            $new = imagecreatetruecolor($new_width, $new_height);

            // Resize old image into new
            imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);

            // Catch the imagedata
            ob_start();
            imagejpeg($new, NULL, 90);
            $data = ob_get_clean();

            // Destroy resources
            imagedestroy($image);
            imagedestroy($new);
            //$imageData = base64_encode(file_get_contents($image));

            $imagenEscapes = $data;

        }
        $password = md5($_POST['password']);

        $stmt = $db->prepare('INSERT INTO usuarios (Usuario, Pass, Nombre, Apellido1, Privilegios, Foto) VALUES (?, ?, ?, ?, ?, ?)');
        $stmt->bind_param('ssssss', $_POST['nombre'], $password, $_POST['nombreReal'], $_POST['apellido'], $_POST['privilegios'], $imagenEscapes);
        $stmt->execute();
        /*********************************************************************Fin resizing de la imagen *********************************************************************/
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'update'){
        
        $imagenEscapes = '';
        $nombreImagen = '';
        if(!empty($_FILES['image-file']['tmp_name'])){
            $imagenEscapes = file_get_contents($_FILES['image-file']['tmp_name']);
            $nombreImagen = $_FILES['image-file']['name'];

            /*********************************************************************Comienzo del resize de la imagen *********************************************************************/
            // Create image from file
            switch(strtolower($_FILES['image-file']['type']))
            {
                case 'image/jpeg':
                    $image = imagecreatefromjpeg($_FILES['image-file']['tmp_name']);
                    break;
                case 'image/png':
                    $image = imagecreatefrompng($_FILES['image-file']['tmp_name']);
                    break;
                case 'image/gif':
                    $image = imagecreatefromgif($_FILES['image-file']['tmp_name']);
                    break;
                default:
                    exit('Unsupported type: '.$_FILES['image-file']['type']);
            }

            // Target dimensions
            $max_width = 370;
            $max_height = 280;

            // Get current dimensions
            $old_width  = imagesx($image);
            $old_height = imagesy($image);

            // Calculate the scaling we need to do to fit the image inside our frame
            $scale      = min($max_width/$old_width, $max_height/$old_height);

            // Get the new dimensions
            $new_width  = ceil($scale*$old_width);
            $new_height = ceil($scale*$old_height);

            // Create new empty image
            $new = imagecreatetruecolor($new_width, $new_height);

            // Resize old image into new
            imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);

            // Catch the imagedata
            ob_start();
            imagejpeg($new, NULL, 90);
            $data = ob_get_clean();

            // Destroy resources
            imagedestroy($image);
            imagedestroy($new);
            //$imageData = base64_encode(file_get_contents($image));

            $imagenEscapes = $data;
            /*************INSERTAMOS LA IMAGEN SI ES QUE HA CAMBIADO*************/
            $stmt = $db->prepare("UPDATE usuarios SET Foto=? WHERE id=?");
            $stmt->bind_param('si', $imagenEscapes, $_POST["editTag"]);
            $result = $stmt->execute();

            $_SESSION['foto'] = "data:image/jpeg;base64,".base64_encode($imagenEscapes);

        }

        if(!$usuario){
            $privileges = 'Usuario';
        }else{
            $privileges = $_POST['privilegios'];
        }

        if(!empty($_POST['password']) && !empty($_POST['password2']) ){
            if(!empty($_POST['password'])==!empty($_POST['password2'])){
                $password = md5($_POST['password']);
                $stmt = $db->prepare('UPDATE usuarios SET Nombre=?, Apellido1=?, Pass=?, Privilegios=? WHERE id=?');
                $stmt->bind_param('ssssi', $_POST['nombreReal'], $_POST['apellido'], $password, $privileges, $_POST['editTag']);
                $stmt->execute();
            }
        }else{
            $stmt = $db->prepare('UPDATE usuarios SET Nombre=?, Apellido1=?, Privilegios=? WHERE id=?');
            $stmt->bind_param('sssi', $_POST['nombreReal'], $_POST['apellido'], $privileges, $_POST['editTag']);
            $stmt->execute();
        }

        /*********************************************************************Fin resizing de la imagen *********************************************************************/
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainOne'){

        $stmt = $db->prepare('SELECT * FROM usuarios WHERE id=?');
        $stmt->bind_param('i', $_POST['datos']);
        $stmt->execute();
        $result = $stmt->get_result();

        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {

            $array_datos[0]['tipo'] = 'update';
            $array_datos[0]['titulo'] = 'Editar usuario';

            $array_datos[0]['nombre'] = $dato['Usuario'];
            $array_datos[0]['nombreReal'] = $dato['Nombre'];
            $array_datos[0]['apellido'] = $dato['Apellido1'];

            if(empty($dato['Foto'])){
                $array_datos[0]['Foto'] = "url(data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAW4AAAEgCAIAAAAbkg7EAAAgAElEQVR4nO2dCYPiNtL3dfgE+pieyeTY3Xe//6d6sjvZJHP0DfiU9FZJsnHT9JwGG1O/kB4aaJCx/XdVqarEy7JkBEEQP4YYegAEQUwBkhKCIHqApIQgiB4gKSEIogdISgiC6AGSEoIgeoCkhCCIHiApIQiiB0hKCILoAZISgiB6gKSEIIgeICkhCKIHSEoIgugBkhKCIHqApIQgiB4gKSEIogdISgiC6AGSEoIgeoCkhCCIHiApIQiiB0hKCILoAZISgiB6gKSEIIgeICkhCKIHSEoIgugBkhKCIHqApIQgiB4gKSEIogdISgiC6IFg6AEQg1HXdVVV8FNrrZQyFm1p77uf8GIhBOfc/XR32vtSSrgfBEEYhvBz6M0ihoF2/EkASuGEw2mH+wmCYHigjVTagBbAy4oSxIM5WxU0oixVWanWdI1CGUUSxMX9KriOI3gKtKYWnEmhualBeVpNCS1wB7RmoO0mDgccLuXQYyD2Qm6B/VsUBfyqjDQMbqKsDOjFal3t6XPnsxCUJQo5Z5ozJTmIEYvjOIqixLKnzyWGhaRkUuQNIB9cRtoERcVWWa2UHnBU4AAlsYxDFgjFdAlqAspCsjIxSEqOnnHKx2cAs2VLVtI0hZ9Dj4v4IUhKjhUQjpWFiVBpOXL5eAknK1Ggma7m8/lisQA/aOhBEd8DScmRUVWVU5Ba80qFq8xFRo+eKJTzVIayCoRZWChYe1yQlBwHSimnIFWtNYuWa50V9dCD2guzJJynnJsijoK5xc0uESOHpGTsrNfrx8fHPC+MSNbFHmdexkYaB0nMJCvmswSMlNlsNvSIiM9BUjJeQEHu7++FRAW5fSiGHs5gnM3D85kwujg/Pz87Oxt6OMRuSErGCCjIw8NDbaJ1ztf5qZghnweMlHnKAl5eXFyApgw9HGIbkpIRobUGBQEdYXJ299hXPLVNT4W9Df/BA/Ze88gLf+T/38C7T7X/8idPNq/pfKR9mO/4mO5rdr5gJ1EoLxaC6+zC8vV/SOwbkpJRoJQCBXl8XBqRPqx0UX5fSNUKwLYC8CcnOpzBjZw0f7GlGfalnL0sM6zznNn+hzcf2h3WM2F6LgE4rq/WhTgKzueCqTV4PCAoNNczBkhKBgZOIevOPNYsuXv8wcQQY99Pw6nbCAqXQnIu7SnOndJorjTX7qObs9w4GwFOZndHigBu7jWmeY1/R9YW9WFePP4tfKB22wLv4Op3hJ12QW2A52CjXJ2gewdXCujfrfnEb5ISh5Ti8iwIWO4EhSyUYSEpGZLHx8fr6xsWLG7uyx/MLrOnsTvtnUTgT47nbWBPb+Feg0ojlBbaWw+bP/f+ipcSLiUXzfuY9gOYPfm5M3W4MsZaTygzvLFEnIJw+6Fwp1U305UMpyCtfvHmnb9jw0FQri4iVi9fv76ioOyAkJQMQ1EU19fXtQ7uluZ73Zkn2FOUWXXA89P2B3DPwMPC6YB7RMNvAs9nwYU9e70I4Ztof3oLa8mYBv8+/oS3L8OPqA3TzNsgkvsXOM3iXpo4uFPwWWZLtrrdDFyDAva9UuIAl+dywUOprq6uKAd/EEhKDg2cPDc3N+useMyDHpNENDYK8Kc6aw0Qe666F3Du24sw71xwKy6trWC6rw8CEUrhznb3586psR8EL9Jaadac/PadBZop1vxB8wTjMfbFRjFWs8b06BojrU45Z6eXPLT5LDxL6lkag6CQv3NgSEoOivNojJx/uu05T2QzIWJ9D2duuNZFzRnrmxaBwWFDr047NnaHsQLgJACsBCkN6AXqBj7OhWykROF/dV3bhiRx60xZf8opiGg+COwfZVjVxkHayAjr+DXMyhP7Mauky5tXMVcr8ncODEnJgejdo9nCnaKtywAeh5TcnuHYIw1urIlhcCaZkVZjrA1inQ9h3R3XDw1eEkgthGp8EOsjNbFS20UJpUSGURjEIALK0QRfm7gqvA+4LcqGVJ6YIU4y2j5s8Aj8NetPShj5O0NAXdQOwe3t7XKV9evRbIETKigaBgQgkFvuAoiBFQXwSriRIBQMGxOFaGig5ADY7CzwHc9AU2YpjxMO0uA8HislbnIGhMOo2oCUrPMyyyvbzFHUteHKSpOdPhLchXUrF3fdotWUfo2RLiDW72/Q36nV9dlidnl52ftHEFuQVbJf4Hr74cOHUoUfbvad+Y5GgZUSA7YFxi5csAJPbpAQOGnxBg/GEq7ZsQt2AlHkmifKwCqQsxSSmEchGjVwsruQqfBvyPC9FAPDJC8ruOG7K412SoWmCt4BMau1n/41XD/Tkq2ISdspdk/fy9urOJLVzz//TGWBe4WkZI+sVqvr65tcpfePe6+g8ROrzMVB0AKx/gb+5MIInN3FYCowi5I0TkA2QD+iEP6PgsDO/XI/eesDHhg9Za0Q2DQR+0HWVMGpa4mPwscq7dSkrqq6LKqyrIqisrJSg9SU9ZP4SKsa7SSOazS91yjp+SJKZPbTT2+oJnB/kJTsi5ubm6yor+/3Ehl5DkoGxjvBcxHoc+jKmBou/yAiYcijOIoTMEAkODDzOErgISGkkJu28e5EbvwRji6Rf6SlnRtyGW0GZMTbHDhlYyO8GKwBw6REJcGgynqtH9fwiFeWbof6VkrY0xDsngBL7OocOxhcXV3t9YNOFpKS/oFz5v3794ol76/zPt+3SdXoPtR50qAlYdBUQKeGK8ENuC2gIEkapmkUx+DHCDiLY8nh1uaJuEjtJknM3TE2I4Sxtmhn81GN6QJejoEbTts0c8Dw0Tb5Fe0U6+IUBVvnpihAU8r1OoNvBt7Iu1EgVpgIq5p3bcVkKym//dwevsKfXycBL96+fUuLbPQOSUnPLJfL29u7ZRkvV/19sU3+mbu/efxJVNPY0AZmjYG1EUYijkBB4tk8Qh1JgsC6MAzFBm7+BIYz3069mGZ1G+6Dq/h5bWThSbZre04rVmmcoHFzwD7J1TyVHfgbbBZZsMdl8fi4yrK8qirdeE04YDSm3MxxW0K4mdXeghv+44JycQbeXXZ19WqxWPzoexEdSEr65NOnT3mhP9ypnnus2nNd25PMFdhwb0DgSjbGPWpPQsnWYaDSWQrnyXw+S+I4DAPhZ3jdXInLHtuctu6/pgLvmWnAXrQObCpJW2X8tH6veaHB0Ao4MmCpMdCQvChxJmsJklLUNcoIypcMBQ/te9kpKBfqddEaOxpwnoRm0ohepITZXPu3lzKJxZs3b3p4O8JCZl5v/Pnnn4WKPt3uYbrXSolXE2sd+FNKM5diBoJiM0l4EkSzRM/myWKRzNIkCAMblfDv0qSQMffT/f+lGuAXn+XsxfBG53ElmBaCBZG0IV4hAxukCUQOBkppFwXUNs/eZ8g17yo2UmWHjor5NP/++wGh/+tav3kVV3/99euvv/bwjgRZJX3x7t27Us9u7nsNjrRg7EPY6VxfSIe+h53k1XjVZ+C9xFEEHs3Z3MxTHlvgzG1tjLZwjrG95HG8BJgjzCauaYzg4CbUyhSFBqtktSpW6yxbF/AIx5Q5n3YvpA3UMJcep6364dR2oPtxcLpcXSSxzP75z3/2+aanClklPwpcWN+9+6Nii7uH/ehIi0tLZa7ihWmOiaZwTZdcRJGczeL5LL08C2Ypbxf3ZU9z1YfCentoVAgmwgDzasMQs1psPSHLshLME2OnmQWXYMFgFSATmAtjp4qE5P1YI88A6X99Ofv99//861//pKYnPwhJyQ8BNt3ff79flek636eO2Jik6xogbGyU+R4hOpA4zTmbRfNFvEjjNJFhiH/RyoebbTXf3g2kn4HbaeWmotilrGDRsT1v0WcJQvDLstUSvJ3aWiE4UmEk85Ed7tXEvV1PDk6X67tslqT/+9+fv/zyMy3B8yOQlHw/WZbd3Nw+5vHB2q86098mkWA0U3DQETmfx2eLBH4mEXgHpulYwrqJYQdI3NiNwUlfaYMcxk0s+1Q6BubJfB4lSRjIALZrvc6qsqyxVNCV6HBh26BoN3Hjig33swV298Xy46fXr69o7dHvhqTkO8FJ3/vlxzuu1EF0xMU6XOgRc1A1ZxrchFkan5/NQUriWDqfpk1Id3/XFZHDq4mf3DFNlAO2QDdTRpxHcPQFnOsYfgfHZrnSKi+UqjH9DY9M8OPEs0axewHUpKiEMrevX53P5/O9f94UISn5Hu7v75fr4uOdOdTCmtY/0S5xw7gsDhGINIkWixR0JLFpI36OxieybeRjyM4dLs/NNB2Z/JC8YWI9GJ4kMHRQE3BwsOK4rMEwwbQXIQLs/chc8r79a7EHD6cBduWne/C2Vkop6mj/HZCUfDPg19w/Fp/u+04eeRnjzz4PRhuEjKNwPk/hBqci6IhrfcheMD22ruwHFRfz5L5oB+q6vWEfWZYkQulY2f4FJrNFgjbZjrt6ZN/92ux70LBD39/AzzwMwzRN9/xpU4Ok5Nu4x8bwxfubAdamaeZ1sbIujsP5LAaTBAyTAP0abbNgweuRPl11F0MFXxtDqb3r7ru0EWNzYngcRfMZNi5xCfe1amVRWqdNW9duj1aJw6oJ6Nvj5XlFtsk3QVLyDSyBNdoj+3jzruHQPeF96ZybzsAb+DkiCsPZbJamYRRKnO3F809xF6M0T5JHtkInz99/v5iOVdJU/nmHhzntQ5UwRgYBT9NIqRkWFpfYq8A3f2tbynLWpu7vG9jFUuZSSoqbfD0kJV8L+DU2zrqX+MiWvbAjaOqSSuxMSCRlEkUpXMdDnOcwvuOQwus2zhC75SM2UtJ9N3Zg74Y3lkT7ma4xtb25XDttS4fgXhAIUJMiT/KiqiplM+kNl9ur6hyAxtN5ADWhOZ2vhKTkqyjL8ubm1s7X7DE+0m1Q6Bp5NLO4KBKsquCfOAkvF/PL81kaC3BtbBmutl3OpKvIY0xsveeWlBwUX+i39ZhoqxK5qynmrK5xe9NYilcLw1Rd5UVRC4b5J7rG3pEiPOjAfRSW3/z00xvKN/kaSEq+DJzYf//9/jGP9zfv6xqyOt3odhjrvoIZJblIwnCeRmkSBgGehNo2Zxa2YaK94j87cQdRkM1n7xiO+8k7v9v5YRxmYLdwlkZxFKiqxNpFTKDxcVdx2O0ANblbhUq9/8c/fqNc2C9CLeq+zLt3f+wvD61bHdNtpNy6Nr4Nqn0wCIMkjcLIH9juT1GDfI+zTf3bEeFGbPvMuskazIWNojiwNUQKG1yboWa0YafDrocDYIDPPjZISr7Au3fvKrY4WD7rzuCr6zYmA5Gk4Wye2mw0G4DVrosI57uu/keDcdNS3LVexf7VGFRO4jiCR7DtwMHCrbuAXa/F2R9/kJp8AZKSz/HXX3+VerbXOr2vtEoYZprLFFybNAxC23fVtMlozi3yt/0NdU+49fu4LS5yWxOGwWyWxkkspbAzU8Z9F0NZXNd3WaFSOBgG+vzjgKTkRT59+lTU0b76Bjyj1RG2IwEES2ijKAApiWLbZdnnnrt2A67Qz7U+Oz4paXLWNrcA7K8khlsQBE1S7MCOGxwGcDDAITHsMMYMSclulstlXuiPtwfSkZbu3G37CEZTpYiTEPsqBtYGsQUqvNt9yOyaLDkSOk4dVvBh/DXAhpI43Y2tCLytNaycwMEAhwQcGIOOYrzQDM4O6rq+vb37+HCIoP3zXLLWu+l2Y8fmRnEYxcFGPnwDRb/Wt4+XHOVCuaaTyWZcyzRwdkBH4iQK11Vd2f5OI5DJD3dKq9s0TWlC5zlklezg/fv3yzI+XIlNx7XZagjglAUXvgpxTgOu1dYfME0W+RMTxi3ce3yYtuTZsGaFY3g4DIMEu8FFGEMZx4bBIZGrFA6PoQcyRkhKtrm5ualZ0me/+M+yM8l16wVC2vV5AykkrpJlc+dbKfGv2pnCcWyYtjAHVzYOOKgn2GI2ZWYUUgLcPxaljuAgGXogo4Ok5Amr1Sor6g/9rl/zJZ5P3LSJau5BMKfRIGlSYW3/NNOFuwkdPZKL9zfC28Cq6Vho4OOAYYLNpWHDjZ+vGsXmfbwp1nm1Xq+HHsi4ICnZoJS6vr65vj/s8do0LGSbPFfO/IIyPoQA55JdF9zlXfizrvWBOt7QSM61b8R6N67hJHMqir2SsP9aEOC8lZ8I7qvffB/cPLCPHz+5YBbhICnZ8OHDB/CED7Mu5zYBGPSsxoW97SrhxstKIOH8yQKZX14ESRxyhkvGcAyW2+7s3K+/62KxQrKXGwyMG966Niggwi5dAf/HAV/MZBwWnO990eVvAg4SCppscaSHXv/c3t4WKjzAOuHbuEuxwHbsfgEql2zmKmqwFrgWUqWpwEZp3N2cjvBujNa+eMis0O/HWSW+PxwGSTiunoVNHAPBo5CHgZbC9isZUwLew7KEA+bu7m7ogYwFmgxGyrJ8WGYfb4dwDza9k9v6XRdB9W6PXWBXjse23wO87Udv4z5ms9iXa8OIywvKEYaBPt4UkpWz2YxKhxlZJY5Pnz6ty8MWsb/EM8XAmKuUExaSz4NWil2pFH8Zn5o85sH19fXQoxgFJCXs8fExr4ODzf7uojOD01gprfNiZ2+CgSpjD4KNuHIbR7bmmWnnsNxqOIHFNq0fnZas1tW6FJQCy0hKgOvrm7vHvfRY/Gpct0Xe+X0jHf5EGmhkB4b7vpOsGwMKQ/wGRuvh3S/1p09kmJy8lIB1mqv0YImtL9I0PN2qx8P16yxDjesQuHngnQaHjRgFoRPTESWqdXEpsOTmnLSUFEXxuCweVwO0j9/CtwfAkON2rpoMpFtIfIynUS+4NT07ErqZmbJJN6HVklHllWwBhxAcSGU5oI88PCctJXAlWeajqMtyC3PufEpaDjyewXH1RL7Bms3PG62OOOBAOnHD5HSl5PHxsdJhVgyRkLYDH2/sGvoutcRLCR+lcb9vrCFilUSOfPPhQCpqecrx10k74Z/l+vrmZjWCdADjmv9wtygMiruPOLr0eY1ZJVJMoFbvJUwnUuLya3inEpgbLnEuWHDsGL35Errd50bCzYNi6nqxWAw9kGE4UasEkxTlYvhoK/MN121alm1MaHy8xD+Ja3puimyGG+U+4Zspm+7Eje/MYiMpwi+tvmkN1Z3kYk8bNQwFHk5ycbL5r6coJUqph4fHm4cxBcm0YbaHqTCuf4Cpq0qrOpQysCvcHGfN71fhckcYk8yvUrq1ag5WFQSBCMIA1++wKwQ9a1i5o/vcIMBBBYeWUsPmFgzDKUrJwwNYoiOYAHZ4q8TaI9pZJXahrLrWSqNt77wdfqRlv18GbQ5bWNTcnlglTaUidlrAHLVnbbQdrin/4GoCB1VtEjjAhh3GIJyclMABB9eN28fhJ4C/wJTzW7+Xp19I1xIZz5d1t6zv7x8GF7XDc3JSAleMrD5cs8WvwzTlbKyp5GuWsGsEpU0DPVHMk1aVO4Vjy04ZCji0KpOeoGFyclJyd3f/uBrJBLCntddNJw+tG4Yc/vwYGs53fB9bVsl4DIHHtbq9Pbng62lJyf39vWLp0KN4SmcVLL9Ew1jOiHHxkoiM0MdRuDrpyRkmpyUlsHfvV+OLrvvAKvdtGN1j/hHORtXwZyhcw7jjMdHgMIPr1tCjOCgnJCWPj4+KjS1KwpreYa2Hs72KBXeJJmyyMzhfw3MHxzxjVCpjp3Kik0p+PSEpgavE/XJcOrIN3/7tGBfu3BMvBZ5HG1B6WJmTMkxORUrW67WQSVmNzrsx7QxN05LQxV47uRNklfhZrZ0MPbIXwYONR6ezxsWpSAl4N6txNS1v6aSB22IcFy/xBfac5oGRVkm6D26lq40k4bXLuuSn4+OcRDmfUirPi7uDrAH8rfiOPrb7oFscq5nX7EQEXEf26bo7WxKwY2bX/7qJjLBmyqY7gzM2I+XuoYg4Zvr73rSTZvpbyOyae0YkQ4/icxim7bJ7quk072tltfbrAB/rcllfDSjCVvK7kwz3CH47SplmhfbuwuxslFXCLXDgweE39CgOwalIyXqk3g3zoRA4O4yCW2uVOLemObV8k7WhB7svuhMx3Ueazcf7tpDPdBVny68ZoVUCFBUnKZkIFVDr1Xr0RTed6+rzU2KEJ8k+6G5mJ7Qqtl7z3BgZQ5OBneDKW2UNDD2QvTN9KYFrgmIjaHH0Esb2PXq2MoOPw/r/2RFlZ30fWzMy/GkTE2u3vejjjfyb0Tw+BcPkJKRktR5xOomLu9pVLNtTaTMN7K609gyacNLrzvnd7q9WSTZODevkzrevH6dVwnCtHH0K8zgTl5KiKGrNR9PA9WU6p8DTkIFLKTGTVhJPVxSeP+tCrccIHH7acDgUhx7Ifpm4lIBFUqlxrOD5Evb06YhF29QUf4Pzx7YNm/bszZczVsEg0fg96K2XbYVIRuvpFHU4eR9n+lKyykaX4bqDJtMK80e8U+NmK5SqcRqUm+lbJY4dnovW7azN1sTNmPPTusBBSFJyxGRZZngwwmT5J7jc1kYnupdVl0xRo5SoqaeVIDvlwPgvoW5zSYYY2o8CByEcinmeDz2QPTJlKcFAiTqidF7fYZ23djvaJHgW1aoe8RW3T55nl4CCVHWFTbPRKnnyMnf/ebx2nCgTTDtcMnEpycvRn4KNVdKZ92Q2Td6nY7ke6/qlVXUnRKsjvF1AzH0D1jDrpqVtTeKwEeeVtJQVm7ZVckQX7W8G9twqG/0GuhIcV7znH7GRAubCrsYJCXMrlD+JySKbk4c//XFE+I1D/CodTWTI9W+plXZdZlx+SfPKJ9M9fJMiPFJWWZ3IMa2X0jeTtUrwCsBHnJn2FKMxNCJsWqc1PzTc5UKig12wda6VwdfgbUfUxNiF69xtvJflLwFiahfvEJpxv7mgDYqJ5aqC85B3KuKeezRH4OCAGopwwj7O6C/a3wtICXinYFcOPZAvgSt8Mlv8y0FKFK6sZa/AuLanhCttUbGsgMsynGes6UO/+Wt7TdZ+gT98/givDU3iDPNLAVkpxY0ShgmlxSpTRaGEEDvLbcyuZXHGiWYhHJZxHA89kL1whEfe13EcgZKnmLYI+OnDYJOUFZ5scGEW3L+yExqYUvMB3vy01YzKVJVpctOObG8+Z9rhkilbJUcQKLEY1AjRhhJZd8rTTWFUdQE+TiQi2f6Je7K1UHgTJTlCUeGu676NqjLWTGPhXXAKigK2XulmU5+bHuM3RlqmHS6ZplVyRIGSnbMS3eCik5I8r+pascZmMR2Yi0e6BkrHeenmLt3MNAuO2/pGeAQ2GXYl/HSdBtjxqMZOph0umayU2EDJMcGb1mHPc8OrqsoLd0aZpxUq3dKd5nZsGFcbjXDW1EcbuyJ7iRpaqPpYq2+e48IlQ49iL0xTSvDcO5JAiXla7frcXHfpnkVRlnBi1dtFbdYnaKaKj1RLHAaDrPaet0qqCr0b2HANF3PDj3fLupQVgx059Cj2wpFdur8SEP68PI5N28qP6NaYsMZU0dYwgZOqKGQURRynelrFcRM3mNNmi/6OKHTgsZYWb3phu3b7WMdYFmWeFVUJ8mm/B9en8hiDQR1WWT0LR1+n/l0cx/n2TcA1nDm/9EjoqsbzZ3lTIpxl2RqLnLmQkWRdD8j+ZK1ncKz4Vi2YY27AAMsyvCDU6N0In8l3/CilXRLz9BpHT217AKxZ0WNsLr+TTWufpvJ161k7uYMpFUVRrNdrMI+1Vh3bY5PtOua08c/BvbXRWCYcvomyrEFIYJOtR2fDsWYiaqJNADbm0KPonwlKCewnw49GStiO6ZjNVE63VA1EBAyTyha2NX9oq3XMjjbLRwS3MzPcNCsCwV1t4HoADh3uyjaWxCfS/knIaUrJBB0ce/wdk0S+NHfDmtAJ2CVwmlVVXQksFW61g7fnX9c4GRNfJW2NVcL4ZqobLEswTOzcjWTHPgn8lFrxSXaNnqCUwH4";
                $array_datos[0]['Foto'] .= "q6yO7OPNn+VdPAyjGykdS1GKV8yjXIjChZH4JUPcGRgojtpqzj4SXCu026xCKTdy1Uma9rpaPRZFppYSQEdzslpZMj00qvwc4OMkqOQ6sVTLuJoxP2Vk/0k1+tULCDY8rFqwLFmZ1EEqRCCm1lyAt+CgNsc34P/MaUAjurQ+lTZHXy1WxWoJ7A38nOQ8Y3hSzM8LHryQM3FOySo4DkJLl+siskq/BTQCXdZWtTRSoMEgSjMh2XzHU0HrApbPWNZgkeZbltVI45S0w5gU+HWjN0c8DN6yyahGPu7PfdzE1KQG9d8ffNPAzwWazDk5ZVmtWBVLHsQxlLMDP8Rlq2jVQGnjET9n0YPlMC2gseQaZ5ErpLC8el6v1OlPg20iJtdEgJVrZ/gPTmSQAPxQO1CCY1Nk3qY1hzrvBjZqU6uMkDZxrAov76qrOtJLSJEkYR1EQSL7JmT82B8AHS7hbX72sqtU6AyUBQWFGyDDkXOLqp+jbaPsFTARt5PSkZDJ7x4OdUEcZNfgx3FQNeNkokZVSWVGu12Vhy1PawOuRFuG4LNYKXJsMpCQHswu7L7pnmkxgX/N8fBu3G8Pl9CKvUzvrMNV1EtkHXZxzoLUCq4QHgguJMYWstAlrdrJK8KYu/6hwK4ZZsyrLq9Uqz7JCoTsn4Ka5QekUmCSEPeWGHmyPgE/ncrKnxKRMLGYTzI+vCuXL+K7RzEYiMfNTqbyoV+syiasohMeEnQTRtpjleEJFTSAFzJB1VoBJUhTgn4JUwjYKDbrJbLRZ2uQ0bDt59H0GWo4xmfDzTM0qmd4eanHei5QykGCZBLVi1iMo80K7eiMD13B+XJuPfktdszxnYGGt86rCBX+kkAGzsXNtuh1YJjOH49vQDD2KnpmaVWKboU5tJzHfacz7Aq4y2NSmKOvlKo8isElCnBkWx6UjPr21LNXDY5nltoWCcfU2OF2lDcdaZ7RNbGB26MH2SKUmeMmbmpTYBSGHHsQeMK5VGpanwKklMZeeY3b5ep1HAQtkLIMoDsUok13biZonjzH3GGdFWd0/rCm+WisAAB6aSURBVMpC2d4rwsqJwQ3BcgHjdqfw+jLKzft26nqCUjKRfdMywV3UwG2pvSsgxt+FhLOsLMs1yAl2LTy2YoEGTClcrmwCKPo7yujaNjtyz2LnJz3Bi/j0rngTtEpsCvakMDZlXGD8AMx+o1WFkxxcBIFgRuSlflgqGWoZCgzBchdf8Gvi2GsFxx5lxtXp2+v7oa4goANwQ4+Mu+4BzGerYW0RiIheLte392tlg+UYMZbSdXjlztMxwtYDc2eVTMnJmZ44Tk1KRr5E2/cBG4VTN3he4eSNxtXIFQ+kDAOtg6qul5mWkY5nGEMJpHMnNL7GOkYC19NhWC2t26K5gwHiUYNEwM2tkdUk0nHYiCxXH69XD8tMSO4mZ3Aznfq5hXEYn1Lucsskw65Tc3CwkV81tRl7x0vtSGzz17oo6tVKgYWi/GtEZ02ZhiE0tqPsTUN5u/ReWep1VuOcjZ7a9fmLwCE6PSmZoFUyPSlxrQacwdVpK81bcbF9pAtwFoIgCkSAfg6+Er0E61LYsOXTJXMONHL8ZPTCXNhU4NUYy3/LEoQPBpyV4OS4dgpbV7WpWZZPKKc4hTM1KbFiPzVTq7WHm05IfgNdCNY9W5blarUKgyoKZmEY4JqZzE+husUh2hz0A48c+8hbQ8T1ecZlgLVeZeXDcr1aryuMFtsWTlPbaV9gelbJie3ASbAVDHJGChyaeZ6vV3mGp6erWpE7KuCGuNq3QWCnY7ViWV4sswx+ukDsAGMi+mZqUjK9Rt6OrWp9v6iFtUqExalJUVTrdVFguw8nMdhXzc99DLEKqJ014nYRG+5uSrNK6aKscAVP+AVtLMFe6LY/YaZ3oE5te+DsicKpbVTr17RBk+6J5+ZQAXhBXek8K7OsqjEEgXPA3Jb6Na2FfFvYw43cV/Viwplral3jUuoKq39ruxCwsJM2vGnrehrYWNbUbLGpnXVwOkXh1AJALS8df7DVgQXz0AsFaoI9lp84403G18E9dKsmfjqp1m7tztIl1HFMlJEn6ODAIUpWydh5aWGqo6bbj74NtTojpX2W2aQM2KFKmTwH9wHOVu2WtvAL7Phe0we2Svjmhv0WdV5UOa6ljnPWtmrIuI4rJyUnzi0dehQ9M7ntmdweaunGR7r+ji078tn0uPmGV2Vd5FVV4owjFu3o1ic6fG8k9G78uoF2SYq8KLI8r1Xt1FEZrZotOuzABmZ62zs1X2B6e2iL1qboLuvHsFjAZYXiT1y+o7KLFJrAZp63V/0h7TVtB1aVFXpftcKhcrShBMe+8gMObBCmd82bmpTY2Yypzdh31+vrGiOss/AFmh/MZZPYTgt1VVT13ES4HJc9an0nk8N2knblPt4qsZlpZVEqpXHpUrwxt1wYn0yrxa8jCHYsV3LsTE1KYA/FoXgcehj7wHkBUvqaFF993724GZu+YQ/RWqtK14Y7sRHcr+Hnp5IPNuY2dd9YLVMVeDY+SuIWvxFS4tSOnuC6MJ8hlCQlowcjeWZqx2VrlXRskCduDnNWifFJpaadMrZVtc2i3ryppDu0lLBmiC12AZ+mCMBuBvbAntzZ9RL2G5hameLUpGR6Yt/S3bQXVvOz2WDOidms2d2+4iCj/Ax8g3/gyfAGH9/hoBmcIwBDCVPbJmJqgJPaOqqTYWqnXRiGclrraRHTgxs1sfW02CSlhLOpxUqIiQGHKEnJ2IE9pDVZJcTIMSQlRwAYJotZNPQoCGI38xSO0HDoUfTPNKWEH9nKUsQJIQSbnknCpjcZzKyPEwWTDZdsFeN10kyaaj3XsmxzY35du5HUzPllgu1dX2WI98cwtMMQBZyskuPAWiVTy50nJoMUepJSMkGrBKXETDDy2rYaeP6Uy/syWNYSoFmiaqGlNAILXVxjVWZzSY1oX3+YMeOqPXYRH2VYpZQyynCNa3ja3FZs1sT9+E6nONjoCc4Es0laJbCfpJiUlGylyXdzRjv9R7DtQMiD0IRSC2kwDUo0BXWuhZot6TtoFEnjmlpYLKSMsSsC+3pCnz6PSf2m8XlOQkcYrl9ak1VyHLg8Qilx0aahx9In3ZRzdyq2bUqap7RxJfvCtmi0VR6u5Txz1oktyrEVfwc6b32/JbTqeRQGoUWWthcjDF/VTkOmlvj5MtIW8k0va55N0ioB4jiep9MR/qd60S2L2yys6xogYcdUXYuAhZGUvoGoq+/D8hy34ObB/QhjF+zkARhNgbQ2I55PzPV30+akjBI4LJMkGXoUe2GCVgkQAWE19Ch6xglHV0S27BSMiOga4yUyBimBy7/tLmCa0AjfKqA7AE2JoXes0FJy/azB47EDwyH6FisnoSZRiAfn0KPYC9OUEhB+ucyGHkX/tGrixKFt78oaKamVEsKAjoCUwuV/69z0QnLQ1kdGdjobgZsTBkIGghe27ytnoHcwcKWU7bI2fS0RrEqS86FHsRem6eCAlBhVyu1T6VjprlnRdi1p27tuorAcS06DUMRJEMXgSrTL4hnXd2CQkVtfBmdyBApHEEcR3MAysWmE+CDnp7J2BR6QugLve+iB7IVpWiXMqslciYdlOfRAemNXuyOz9TMMRZKEaRpGEbgSxgdKUE9s/u/Bk8Fc0FfbBccFl1EoZ0lcV6rIS4UYjbFxfiIzwTZQMtkQ82SlBLQ/KicSLmnXrGh/dfddqLU7QwwbPT9LkjSyJgm8RrE2y9Wwwye88mYxL7tKH+OCJ3GgdVoUlVLaLohT4VI4J6AjzAZKphpzZROWkomFS7Yu2k5K8LJur+c4LSJxcmQ2TxcL3HTr3GmD08N2fRyXn+6skoOetmhziKatLNwCKdIkPFvMMUmNibLCpb9OwLlBJhwoYdOWEhsuCYfKLtlZLPPsRf4Hd+thvjjSTUv2dnng7to3URTFSHR2lszSIAztIrw+q4M3zg3KjuAD2SbM5bTiJweSzWcJtogWQZblWVGV2DvabF6+GZ7Z+GU7vkDTfi3NNu2aojJP/h3KAJp2oIRNWErY5MIlzZQu7+S7CqcjaToDewREZJ7Cr64XtLar8mnW9qc0zc/DWiU2vMpt4FWhhcQxqSSCE4pHUgiwprRZl/V07MeXmHaghE1bSg4WLnm+dObzTLBmjU1fwsv9zIWbeAETQuDCuu6aamOV/OnVFG2MGnPQ0SixCeeh5HEs4yhM0mQ+AzUJQESigEu/iBbH+hsm/fLjcB/+dcJyUClxQuaiOabpZs0C2IiIByIMA1w4WATgrGm44TpgtU2CNW7lUgA2SGgGj23tSvfdbb5Fm/GmGuvHC2ena3bz2YoNEuSddqCETVtKYM8Fq/xgH7cVFn2SP8aclmB2J/MJngzX3sZAJLxeaCUxA8NOt3BXKcM7yaw2dInd4UBN7Ep2QSSTJJrN49ksStI4ibB7HMMT1wi/T7ld/m5zGcRzaYCLovBq4qSz84QUTEYsjoIonqXzsCqrPK+yrMjWJc7uaFczhM09wEtlrNS89BJh/HIX7i0NiqdbZgylxDS9FkBZuF9ijDn9tS6GMGqYBAjJ6zg+G+SjD8PEpQS80ygMymq/PQe607GOpxnurghGwClhq2K0tThQGuDorzFMgBOlYPmjyDAXAdF2eSw7i9u4NWEoQzjzAhnHQZKCGQIiEkZRIPEkwbcFl8baHqyNG2wYbIakWadi9wDw0Qi+FSFUDOaVStM4Tyu7fLqqSjdbXKm6NlKB5cL9Ij548982SDNYLExx9Ohs+oyVYsN9ai93ooxLjZlaG1zIywv2ob4ASxQKbmqySo6Y+XxeKFVWewmXPE/0aH99Ki7SHt/NSpumqc81eNq7vHF7fPsyWnvg20U8Bbcy4c38WQTWRwyCAlIS2VsYwOPc2ut2PmTYNYG/F/C5Alx1MAhAK8MArC0wS4pc5XlZFBXcryulrYnh/Dzjc2X85LY3T5yHYwNDxn2zLsbLmh3R1PrYDzx0xGieBvP5ZAOujulLyePqeq8f0aZXPZ+vdVISSDxT7BVWW0tEuakUsN2FjTsy5uraalfi5sxyLFeBkyuUETovONE7T+CaHcObBQGaIS7awjbBF/yjQ19t+8A5LbYeh4tIRJGIMcvOFEVUFGWZ1wBISl4XTcmisV1QnI5Y38V6OCCmtTPp/LrIppni4U5DOPPm4OG/pDioFouLQ3/qYZm4lMRwBRcmiYO86L9F41ZFzFYHAP8U47jatl/8m9t1SFlzlBt4yi39a30cPCWwbDaQYSDB/nDzuxEW5tleJFIELlNe+HOC2byRJiLgLf/eN3P/wHmufPTD/gQTDKt1wiBNpapRZfOyWsEuxKisqsDjqSoboIVnlIbvUNnOT/i9BKLxgsDgM94k4U3rR7sfbML+Ib8oOPykUFOt4muZuJQwa5iUusqLg35oG3ltOnZo78Q7W9xP5OArDaarshisD7BAArTwwQyxN4yDgFkSSC4DLxIuYOi9GN2+S5swcfgGAr3gPBZ7x/+LTQmwVQJcByIJX1uUBFESYg8FKyVlWVXgtZY13EF1qdFW0fD1COMaKeBXxd27NrEmXEgZlya2dZAH/ZYWMzGfTzlK4jgJKXl4/LCPd96Zgco6doqd5sUgiDVanCdinKDglC13JW1YdH++SFK4eIGUgIKEQYgVNLzpj2O8i998kHDmO2/LakxTE3eUQmIz/JtNbZo6YbijTZHlOMkN3wpjIdhw4ODUoCZVDe5PnhdlgSGVuq7KWle6NsY1eXKCIp0TZBUKfUrWhFkO+U0JU8znrw73eQMxfSkJQzg55flC7ilXbau4rgWbcrRSYuAQ11ZHUAjA3g2jwKoGnCDwn1ykQQynC/fJrK55QNOeRDfTwr51kf9c7wy4jovtqXGMYoLb3STPWQuuSQ3RTDevwLASx3awGKMFSyWJpErCapaio4OWiVrn5cM6w/wUcHtqbZ1HhYrLJWcBhq4Fb9P0Dsb5IoqjaTZz3WL6W8isYZJXPedTdotZt3qaNXIgfIV9XXOm4Hh2jX9APqLY3iLsToghVSmiQASyDRQa34nVlfSiaW4ndNwEp+kaKCg6xykfHVzpcuuJuBLAdtUNnBd3/ZN0M9dl2ylgTzYRhUxjBBbzdWZVGc/DCmyTWpVFVRZ1VWG2GwajjGqKkA7dzCAODRx+h/3MYTgVKbm5uWXf1kLUbF+8ePc5N0Pg8xOeH50upd1dCRmvQFRsmQxWyiQJhlKxYaJs2j4ba5JrP8/Q+kkuJwLOos6i99ZFcokqzEVfO1lw/qO/ZSvHgGup0qmi4c13YOfNfQKw8asIcDvV6792O9XlHmQYqZ6FIColSkldZGVuk1PKAqeTFdp2LovWNBYk75bsPKnnYf6jOsVA3RE/S9t5Ga7z+fzNN34jR8lJSAmc0GmaXBpx99Bb9LXJZ3W/eCe/Gz1x2a5giJydXaQp9iG03gwYIQI7EgqbYYKXXH+a4D0ujO8Qb1hb4Od8HDRPrIlumtxZ3pxojdWuzVAprT8I37asmjW3ePOMFVpl2t6SHets84UILYS2TWSDJAhUEmNUpdZ5Brc6z0sM1tZ1qWp2KMG9PI/TVE+yKfRzTkJKgMViUav13Ve+2kVHn8QgvOPBfaKCP8oxoorXOZdpafsPtrMwNjM1lBK85SQObBGM/Y+xrinDu/eawhzWPoIvbA/E5mObS/Vxztds04pD97HOv63ZJdrvflsJGovGLdbh8kxYgA+BdzOLTJGqogjrqs6LalnkOA9kS35wItln4juHtClw4jZV0BU6bD6z43oZ+zdfypqFj4YD7we/n2PhVKRkNpvd3t5+U4IJmMyGu56O1gTnPjPKiof2+deK2+JbnBsQ0oShiOMwnUXpDH7GGFV15Wi7P4E358rO5RuaM4zb7K3nzzx74RFf+74oiXgahztLATqlPXLbh4VdCO5hzOJYsgU+lZf1wyrIM5z0ybKiLEwFTg9W5eCiHwITdwLuJ5VRbYyuMfgrmpx94fNtceLNz5+9OPTFLGKmnM1++rav4mg5FSkBzs/PS7X+qgQTa0nbY8bNnmhhZ24lXrjQGwF/RGlMkJKu61CIxTFxHMRYSB5GMSaVYW/Vdh5yxwHHd97deui52fGSIXLE9slXDH3n1f/Lf8e3/gdNCc5FmiQxSEmSREVeF6WqSq0qU8MeVaXBGguXeMttTylpU91AVGpbMmWHYifnXNDqM4NIIn1xMfEM1y4nJCVnZ2d3d3dfVd3nLBCM82mbloBXI1cDYg1ibUOhcAFjsV0mI4wCLK7DeZkADBPMKPM5H8oucym+MeJL7AsQYiyADJhzQqtUY55bAe5PVRR1if0hbc/ZmoE1GYTYmg4jp1ZLXGIyF51G//zFrNkoBPOmXCx+PujmDcoJSQkAVwnNiw83X5VgonWFKZfSLgHFjM20xLRKuGLFSZREoB1Y4O9qZNASCezVynroNr21LfCdeMb0EeHz5zkHxQ+CSCUM7BGXPpvncMOGbpj5llV+8ljb6wruROOyX/imO775TIXxxUJeXJzEHHDLaUkJ+Di3t/+JoxjM2s+9DhOsXVkdLpwJbjNeklQFN5zWDcPFLD07m4EzHEZYLmLTSGw2ti/6dQrifraJ7cQYgD0JMsFs0TUW++CagSFPEpmmoQ/NZuWDzLDnrF2KVBtf3iTdhYK7HWw+E5mK4agw+fn528Nt1gg4LSlhzjAR1eelxE06BjaSbwyYvHZqRjL0YOJwliaLxXwxB49bbiZumwq9diaTu3V6X/DzieEwvC01sPM9tiUBDwO4SAhVY9oxODYZTh6XBebkK+1buvmgi/b5uPi3Oz/g8iw4P4Gimy1OUUru7v77Fe2jjcQ8a2Vw4TgGXk4Sx2CMzOezWRqhUyN9lrdx84VucYa28saG+41pH9jzVhFfB8faYWmaXWJT4bSzOmBvhwGzDWJwnfQ0L9dZsV6LvMB8FDs1rFw6oU8sZHynhyMl19Xq4uL/DbB5g3JyUgLHwfn5mRH6090X5nJsrMTgYnfYMjBdzNM5ikgYSJvDalyXENPkH7UGSDtd4OYPD91lh/gM1kGxlqTP/ds0qbMXA1QJge1gZBzKJAmTOFhl+XqdZ3lVVXb5r8Y3eukjLhfhxXk6jZSfb+LkpIRZw2S5/BMOl88bJoKhjqTgzswXYI+kSRyFUtrjkLfVunbSsPkLbkybhmL/NZ2yEmIE2AxhqwJNDfLmqdaCtCUJmDMbCrhsBHjxCIXI1uDyVAqn73yuLVYbblklYJIEPD8//+2QGzUSTlFKQETAMNFcfbzd3UQajjCJ/dzjZBYu5rPzs7MkDoWfI2Y+4dWni3DTpr8yH2Btu4i4upLDNscgvkQj903u7JPuDNxHv7RtdAfHgG1gJyNsHAO2SVZiEgrG4F00dnvXXp3H52dYJHHA7RkLpyglrImY4FROUbv6/fa48BcrLi4uUUNcAgK2F3G1q7bxatMAERspMnvMdZMumyaujbdDjIdGR3hT4dReAHxHKuwkrRT6tsy2NcB2mmkqwzCN42gZVo/LLMvy2ijOt5UkjiRTy8vLfx9+s8bAiUoJHAevX1/x++x9yZ3VYPBCg55zGIB2RLNZ/OpiMZtF3PbccoUzfHPUPakS2bzt5v07v5OcjAe+Efonraue70a7rI4Q4M+IEBsv2Qa7IhBcSVGvszovMHIiGpPGYC4Jv7q4OuDGjIsTlRJmk1+Xy+ViFmV5hR3bjWKmFlws5unrq/PLixmGV5/9lTc1miNv+xWkHePnM7Yid6VMmDbvO9CAHyN8H4co4GLGpAhmcXT/oP4uaqWMbfaNz85nYSQ1HFSH2ozRcbpSAlxdXVXq+mFVCa7hcpMk6SxNzs8X83kSyDY7+ikUQT1qPrv3nsc+tNa8ad/oOvpHIfwIa80vq3iN2bFVjZPE7GKmX78+ib4kL3HSUhLH8TyN31zwm/t1FKcXZ/Ozs1maxtideOixEcPSXUXAN0qyvozEpUj4nEcgO+EyfFyudL5+c5miVTL1nvKf56SlhFnD5OHxv4tZMlvMzi/mi3ls19wlH4VAtpYlwV5WjoAFocGGBFyHgZYsv7o6ocq9nZy6lMDB8eb11Sor4znoSWQXmmmmckhQTpWXEoHax3H594At5iHnsyKq5ump9Df6DEfcLqcvzs7OjCpnCa6baVuUaNsj3jdMI06Ntuk3b2gXSHsCLmbA56nUdXE6rdI+A0kJAm7O/TWulWNsNahtTKFJSQjH08Xk27kdbKl3++nD69evhx7gKDh1B8cRx3EYhvl6NUtT69Vod00iF+cEabPpedMHvLs4iWkWauVM51kRWQYc7Xggq8Tz6tWr25sbrWuJnUewFzz1GTlNni8m33o67lfjVufR9e3N7eXl5SCDHCEkJRt+/vnnP/74s1kJj3TkdHnu0bBGUJqV0vif/3sPB8zQIx0RvCz3svzlkbJer4sif/vWdfcNqSfrKdMVEfez8XHYp08fwSmezWYDD3FMkFXyBDg44EBZryu3cvXQwyHGhbNHsmwNmkI6sgVJyTZXV1cfP94oRTFXgnVDJA6l1KdPn+AgGWpIo4WkZAdv377944//kZScOPwp7sF3795RiGQnFCvZzXK51FpTygDRBeyRIAjm89NaleIrIatkN4vFAkQ2z3e3WSNOkCzL6romHXkJkpIXefPmzYcPH+DoGXogxPDAYQAmCVmpn4Gk5HP8+uuv4Bsr9dn1t4ipAwcAHAa//PLL0AMZNSQlX+Cf//znf/7zn25rcuKkgF0PBwAcBkMPZOyQlHyZ3377jdTkNIGd/vvvv//jH/8YeiBHAEnJl5FS/vTTT3/88cfQAyEOjZv6FTua/BLb0GTw15Ln+Wq1Iof5dPj777/n83mSnNzqv98Hye3Xklg+fvw49ECIQwA72u3xoQdyNJCUfANwjQJb9/b2duiBEPsFdjHsaEoh+S";
                $array_datos[0]['Foto'] .= "bIwflmsixTSr169WrogRB7AXQkCAKyR74Vskq+mTRNtdbk6UwS2K2wc0lHvgOSku/h/Pw8DMO///576IEQfQI7FHYr7NyhB3KUkJR8J3PLu3fvKN9kAsBO/O9//+v26dBjOVZISr4fMIOvrq4oe+3YcXlob968Ib/mR6Cw64+ilPrzzz///e9/S0ndG48P2H1gj/z222+Uh/aDkJT0wx9//PGvf/0rCGgxkGOirmtwUam+phdISnrjr7/+evv2LRnJx0KWZZ8+faL05b4gKekTODSjKKKuFuMH9hSYJLSneoT8wz558+YNuNy///47tTgZLbBrYAeBK0o60i9klfQPHKzv378HWaGZxbGxWq3AHvn5558pRt47JCX74ubmhnP+9u3boQdCeD58+GCMoXUn9gQ5OPsCDtk4jv/v//6PusMODuwC2BGwO0hH9gdZJftFaw3ODrjl5OwMBTg119fX1MFo35CUHILb29uqqn755Rdy0Q+JUurvv/+Ooujy8nLosUwfkpIDURTFzc3N2dkZdSc4DCDfj4+PYA+ClAw9lpOApOSgwMF9d3f366+/pmk69FgmS5Zlf/31F0j2YrEYeiwnBEnJAIDrDrY3+Tu94zyaIAgovHp4SEqGgfyd3iGPZlhISobE+Tu//fYbVe78CHme//nnn5eXlyDNQ4/ldCEpGR5Qk9Vq9fbt29lsNvRYjoz1ev3hw4f5fE5zNINDUjIKwMl/eHhYLpcgKBQs/BrAoPv48SN8V+fn5xRyGgMkJSPCGHNvAUEBW51zPvSIRgd8RU5EQEEuLi7oKxoPJCVjBCwUEJSrqys4YShH06G1hq/l5uYGFIQ6OY8QkpLxAv4OCAqcOWDGh2E49HAGo67rB4v7KoYeDrEbkpKxs16vQVNgN4GRMp/PT6fnI5ghsO3X19ewyaAgFJMeOSQlxwGcVysL3H/16hWcV1N1fJyCuNVU3WoSU93SiUFScmSAtQ+CAnYKuDyvX7+O43gyocc8z8EGgQ0EGwQUhOZljguSkmOlKApnp4CFAudekiTH6PsopUBBHh8fwRIB+YANoVzVI4Wk5OiBUxFkJbfA2Xh2djZyWXHyAYYVKEjSAObV0OMifgiSkknRasrYZAXcFhgbyceEISmZLE5WYP/CT2OMm1GGExh+CiH2F4kAo0NrXVUVfC78BPngnMPngudC8jFhSEpOAnduA2AdtD/BWknTtDVb4ISXFvcnvKF9E9PgflUW94jzWdbrNdwBqYI3DC3uDk3BnAIkJadL3QHkAOTG6YK7oy3uDrwY5ABkRVjcHSc0osEJxxicKWIQSEoIgugBsjwJgugBkhKCIHqApIQgiB4gKSEIogdISgiC6AGSEoIgeoCkhCCIHiApIQiiB0hKCILoAZISgiB6gKSEIIgeICkhCKIHSEoIgugBkhKCIHqApIQgiB4gKSEIogdISgiC6AGSEoIgeoCkhCCIHiApIQiiB0hKCILoAZISgiB6gKSEIIgeICkhCKIHSEoIgugBkhKCIHqApIQgiB4gKSEIogdISgiC6IH/D1nFh2tQA+D9AAAAAElFTkSuQmCC)";
            }else{
                $array_datos[0]['Foto'] = "url(data:image/jpeg;base64,".base64_encode($dato['Foto']).")";
            }
            $array_datos[0]['privilegios'] = $dato['Privilegios'];
            
        }

        echo json_encode($array_datos[0]);
    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'delete' && $usuario){
        
        $stmt = $db->prepare("DELETE FROM usuarios WHERE id = ?");
        $stmt->bind_param('i', $_POST['table_id']);
        $stmt->execute();

    }else if(!empty($_POST['tipo']) && $_POST['tipo'] == 'obtainTable'){
        //Obtener tablas generales de ingresos y de gastos
        $a_params = array(); 
        $sql = 'SELECT * FROM usuarios';

        if(!$usuario){
            $sql .= ' WHERE id LIKE ?';
            $a_params[] = $id_usuario;
        }

        $stmt = $db->prepare($sql);
        $stmt = bindVariablesSystem($stmt, $a_params);
        $stmt->execute();
        $result = $stmt->get_result();

        $i = 0;
        $array_datos = array();
        
        while($dato = $result->fetch_assoc()) {
            $array_datos[$i]['id'] = $dato['id'];
            $array_datos[$i]['fecha'] = $dato['Fecha'];
            $array_datos[$i]['usuario'] = $dato['Usuario'];
            $array_datos[$i]['nombre'] = $dato['Nombre'];
            $array_datos[$i]['apellido'] = $dato['Apellido1'];
            $array_datos[$i]['privilegios'] = $dato['Privilegios'];
            $array_datos[$i]['opcionBorrar'] = $usuario;
            $array_datos[$i]['foto'] = "url(data:image/jpeg;base64,".base64_encode($dato['Foto']).")";
            $i++;
        }

        echo json_encode($array_datos);

    }
    
    Conexiones::closeConnection($db);

?>