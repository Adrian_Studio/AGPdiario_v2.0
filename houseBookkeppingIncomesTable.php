<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>AGPdiario - Contabilidad</title>
  <link rel="shortcut icon" type="image/png" href="favicon.ico">

  <!-- Custom CSS -->
  <link href="css/masters/master.css" rel="stylesheet">
  <link href="css/masters/master_bookkeping.css" rel="stylesheet">

</head>

<body id="page-top" onload="inicializarVentana('Vivienda','Tabla','Ingresos','General')">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php include("pageIncludes/general/menu.php"); ?>
    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include("pageIncludes/general/toolbar.php"); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">
            Contabilidad
            <a href="#" class="popoverReference data-addition" title="Añadir gasto" data-toggle="modal" data-target="#dataEnterModal" data-content="Añadir datos de gasto" onclick="addInAdditionModal(this.title,this.id);" id="Gastos"><i class="fa fa-minus-circle"></i></a>
            <a href="#" class="popoverReference data-addition" title="Añadir ingreso" data-toggle="modal" data-target="#dataEnterModal" data-content="Añadir datos de ingreso" onclick="addInAdditionModal(this.title,this.id);" id="Ingresos"><i class="fa fa-plus-circle"></i></a>          
          </h1>
          <!-- DataTable -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">
                Ingresos generales
                <a href="#" class="popoverReference" title="Filtrar" data-toggle="modal" data-target="#dataFilterModal" data-content="Filtrar datos" onclick="addInFilterModal();" id="Filter"><i class="fa fa-filter"></i><sup><i class="fa fa-exclamation filter-no-applied"></i></sup></a>
              </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display" id="datatable" width="100%" cellspacing="0">
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <?php include("pageIncludes/general/footer.php"); ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Add activity Modal-->
  <?php include("pageIncludes/bookkeeping/addActivityModal.php"); ?>
  <?php include("pageIncludes/bookkeeping/filterActivityModal.php"); ?>

  <!-- Javascript Master -->
  <script src="js/masters/master.js"></script>
  <script src="js/masters/master_bookkeeping.js"> </script>
</body>

</html>
