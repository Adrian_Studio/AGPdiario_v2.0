function copiarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_general.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);
                console.log(data);
                rellenarRellenable('categoria',obtenerRellenable('categorias', data[0].tipoContabilidad),'campoVacio')
                rellenarRellenable('concepto',[],'campoVacio')
                tipoContabilidad = data[0].tipoContabilidad;
                $('#dataEnterModal #categoria').selectpicker('val', data[0].id_categoria);
                $('#dataEnterModal #concepto').selectpicker('val', data[0].id_concepto);
                //$('#dataEnterModal #fecha').val(data[0].fecha);
                $('#dataEnterModal #importe').val(data[0].importe);
                $('#dataEnterModal #observaciones').val(data[0].observaciones);
                $('#dataEnterModal #vivienda').selectpicker('val', data[0].id_vivienda);
                $('#dataEnterModal #vehiculo').selectpicker('val', data[0].id_vehiculo);
                $('#dataEnterModal #litros').val(data[0].litros);
                $('#dataEnterModal #lugar').val(data[0].lugar);
                $('#dataEnterModal #cuentakilometros').val(data[0].cuenta_km);
                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    $("#tipo").val('new');
    $('#editTag').val(tipoContabilidad);
    $('#dataEnterModalLabel').text("Copiar elemento");
   
        //$(this).find('form')[0].reset();
}

function editarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    $("#tipo").val('update');
    $("#editTag").val(a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_general.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);
                console.log(data);
                rellenarRellenable('categoria',obtenerRellenable('categorias', data[0].tipoContabilidad),'campoVacio')
                rellenarRellenable('concepto',[],'campoVacio')
                tipoContabilidad = data[0].tipoContabilidad;
                $('#dataEnterModal #categoria').selectpicker('val', data[0].id_categoria);
                $('#dataEnterModal #concepto').selectpicker('val', data[0].id_concepto);
                $('#dataEnterModal #fecha').val(data[0].fecha.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                $('#dataEnterModal #importe').val(data[0].importe);
                $('#dataEnterModal #observaciones').val(data[0].observaciones);
                $('#dataEnterModal #vivienda').selectpicker('val', data[0].id_vivienda);
                $('#dataEnterModal #vehiculo').selectpicker('val', data[0].id_vehiculo);
                $('#dataEnterModal #litros').val(data[0].litros);
                $('#dataEnterModal #lugar').val(data[0].lugar);
                $('#dataEnterModal #cuentakilometros').val(data[0].cuenta_km);
                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });

    $('#dataEnterModalLabel').text("Editar elemento");
   
        //$(this).find('form')[0].reset();
}

function borrarItem(a){
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {   
            var parametros = {
                "tipo" : 'delete',
                "table_id" : a
            }; 

            $.ajax({
                data:  parametros,
                url:   'ajax/bookkepping_general.php',
                type:  'post',
                async: false,
                beforeSend: function () {
                    $("#resultado").html("Procesando, espere por favor...");
                    
                },
                success:  function (response) {
                    try {
                        
                        applyFilter();
                        console.log("Elemento con id = "+a+" borrado con exito"); 
                        Swal.fire({
                            position: 'top',
                            type: 'success',
                            title: 'Base de datos actualizada',
                            showConfirmButton: false,
                            timer: 1000
                        })
                        lookForPeriodicities();
                        
                    } catch (e) {
                        $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                        /*alertas();*/
                        console.log(e);
                    }  
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        }
    }) 
}