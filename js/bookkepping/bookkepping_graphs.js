var piechart_dch;
var piechart_dch_2;
var piechart_izq;
var piechart_izq_2;
var barChart_bal;
var arrayLenght;
var r,g,b;


/*Funcion general para crear los graficos*/
function createChart() {
    
    switch(datosVentana['grupo']) {
        case 'General':
            datospiechart_izq = datosPieChart('categoriaGeneral','Ingresos');
            $('.piechart_izq_title').text("Ingresos por categorías ( " + euros(datospiechart_izq[2]) + " )");
            piechart_izq = pieChart('piechart_izq',datospiechart_izq[0],datospiechart_izq[1],datospiechart_izq[3],datospiechart_izq[4],'evtOnClick');
            datospiechart_dch = datosPieChart('categoriaGeneral','Gastos');
            $('.piechart_dch_title').text("Gastos por categorías ( " + euros(datospiechart_dch[2]) + " )");
            piechart_dch = pieChart('piechart_dch',datospiechart_dch[0],datospiechart_dch[1],datospiechart_dch[3],datospiechart_dch[4],'evtOnClick');
            datosBarChart_bal = datosBarChart('general');
            break;
        case 'Vivienda':
            $('.filtroVivienda').css('display', 'block');
            datospiechart_izq = datosPieChart('conceptoVivienda','Ingresos',2);
            $('.piechart_izq_title').text("Ingresos por conceptos → Alquileres ( " + euros(datospiechart_izq[2]) + " )");
            piechart_izq = pieChart('piechart_izq',datospiechart_izq[0],datospiechart_izq[1],datospiechart_izq[3],datospiechart_izq[4],undefined);
            datospiechart_dch = datosPieChart('categoriaVivienda','Gastos',[4,5,6]);
            $('.piechart_dch_title').text("Gastos por categorías ( " + euros(datospiechart_dch[2]) + " )");
            piechart_dch = pieChart('piechart_dch',datospiechart_dch[0],datospiechart_dch[1],datospiechart_dch[3],datospiechart_dch[4],'evtOnClick');
            datosBarChart_bal = datosBarChart('vivienda');
            break;
        case 'Vehiculo':
            $('.filtroVehiculo').css('display', 'block');
            datospiechart_izq = datosPieChart('categoriaVehiculo','Gastos',[7]);
            $('.piechart_izq_title').text("Gastos por categorías ( " + euros(datospiechart_izq[2]) + " )");
            piechart_izq = pieChart('piechart_izq',datospiechart_izq[0],datospiechart_izq[1],datospiechart_izq[3],datospiechart_izq[4],undefined);
            datospiechart_dch = datosPieChart('conceptoVehiculo','Gastos',7);
            $('.piechart_dch_title').text("Gastos por conceptos → Vehículos ( " + euros(datospiechart_dch[2]) + " )");
            piechart_dch = pieChart('piechart_dch',datospiechart_dch[0],datospiechart_dch[1],datospiechart_dch[3],datospiechart_dch[4],undefined);
            datosBarChart_bal = datosBarChart('vehiculo');
            rellenarVehicleSummary()
            break;
    };
};

/************************************************************************************/
/*Funciones para obtencion de los datos de los graficos*/
function datosPieChart(tipoGrafico,tipoContabilidad,id_categoria){
    
    var etiquetas = [];
    var datos = [];
    var total = 0;  
    var backgroundColor = [];
    var borderColor = [];
    switch(tipoGrafico){
        case 'categoriaGeneral': 
            var parametros = {
                "tipo" : 'obtainGeneralCategoryChart',
                "tipoContabilidad": tipoContabilidad,
                "fechaInicio": filtros['fechaInicio'],
                "fechaFin": filtros['fechaFinal'],
                "año": filtros['años'],
                "mes": filtros['meses'],
                "categoria": filtros['categorias'],
                "concepto": filtros['conceptos'],
            };
        break;
        case 'categoriaVivienda':
            var parametros = {
                "tipo" : 'obtainHomeCategoryChart',
                "tipoContabilidad": tipoContabilidad,
                "fechaInicio": filtros['fechaInicio'],
                "fechaFin": filtros['fechaFinal'],
                "año": filtros['años'],
                "mes": filtros['meses'],
                "categoria": filtros['categorias'],
                "concepto": filtros['conceptos'],
                "id_vivienda": filtros['viviendas'],
                "id_categoria": id_categoria,
            };
        break;
        case 'categoriaVehiculo': 
            var parametros = {
                "tipo" : 'obtainVehicleCategoryChart',
                "tipoContabilidad": tipoContabilidad,
                "fechaInicio": filtros['fechaInicio'],
                "fechaFin": filtros['fechaFinal'],
                "año": filtros['años'],
                "mes": filtros['meses'],
                "categoria": filtros['categorias'],
                "concepto": filtros['conceptos'],
                "id_vehiculo": filtros['vehiculos'],
                "id_categoria": id_categoria,
            };
        break;
        case 'conceptoGeneral': 
            var parametros = {
                "tipo" : 'obtainGeneralConceptChart',
                "tipoContabilidad": tipoContabilidad,
                "fechaInicio": filtros['fechaInicio'],
                "fechaFin": filtros['fechaFinal'],
                "año": filtros['años'],
                "mes": filtros['meses'],
                "categoria": filtros['categorias'],
                "concepto": filtros['conceptos'],
                "id_categoria": id_categoria,
            };
        break;
        case 'conceptoVivienda':
            var parametros = {
                "tipo" : 'obtainHomeConceptChart',
                "tipoContabilidad": tipoContabilidad,
                "fechaInicio": filtros['fechaInicio'],
                "fechaFin": filtros['fechaFinal'],
                "año": filtros['años'],
                "mes": filtros['meses'],
                "categoria": filtros['categorias'],
                "concepto": filtros['conceptos'],
                "id_vivienda": filtros['viviendas'],
                "id_categoria": id_categoria,
            };
        break;
        case 'conceptoVehiculo': 
            var parametros = {
                "tipo" : 'obtainVehicleConceptChart',
                "tipoContabilidad": tipoContabilidad,
                "fechaInicio": filtros['fechaInicio'],
                "fechaFin": filtros['fechaFinal'],
                "año": filtros['años'],
                "mes": filtros['meses'],
                "categoria": filtros['categorias'],
                "concepto": filtros['conceptos'],
                "id_vehiculo": filtros['vehiculos'],
                "id_categoria": id_categoria,
            };
        break;
    }

    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_graphs.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var color;
                data = jQuery.parseJSON(response);
                
                arrayLenght = data.length;
                $.each(data, function(index, element) {
                    etiquetas[index] = element.etiqueta;// + " ( " + euros(element.importe) + " ) ";
                    datos[index] = element.importe;
                    total += element.importe;
                    color = dynamicColors();
                    borderColor[index] = color[0];
                    backgroundColor[index] = color[1];
                });
                r = undefined;
                g = undefined;
                b = undefined;
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                /*alertas();*/
                console.log(e);
            }  

        
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    //console.log([etiquetas, datos, total])
    return [etiquetas, datos, total, backgroundColor, borderColor];
}

function datosBarChart(tipo){
    var evtOnClick;
    var etiquetas = [];
    var ingresos = [];
    var gastos = [];  
    var balance = [];  
    switch(tipo){
        case 'general': 
            var parametros = {
                "tipo" : 'obtainGeneralBalance',
                "fechaInicio": filtros['fechaInicio'],
                "fechaFin": filtros['fechaFinal'],
                "año": filtros['años'],
                "mes": filtros['meses'],
                "categoria": filtros['categorias'],
                "concepto": filtros['conceptos'],
            };
        break;
        case 'vivienda':
            var parametros = {
                "tipo" : 'obtainHomeBalance',
                "fechaInicio": filtros['fechaInicio'],
                "fechaFin": filtros['fechaFinal'],
                "año": filtros['años'],
                "mes": filtros['meses'],
                "categoria": filtros['categorias'],
                "concepto": filtros['conceptos'],
                "id_vivienda": filtros['viviendas'],
            }
        break;
        case 'vehiculo': 
            var parametros = {
                "tipo" : 'obtainVehicleBalance',
                "fechaInicio": filtros['fechaInicio'],
                "fechaFin": filtros['fechaFinal'],
                "año": filtros['años'],
                "mes": filtros['meses'],
                "categoria": filtros['categorias'],
                "concepto": filtros['conceptos'],
                "id_vehiculo": filtros['vehiculos'],
            }
        break;
    }
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_graphs.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                data = jQuery.parseJSON(response);
                var i = 0;
                $.each(data, function(index, element) {
                    etiquetas[i] = index;
                    ingresos[i] = element.ingreso;
                    gastos[i] = element.gasto;
                    if(!balance[i-1]){
                        if(element.ingreso && !element.gasto){
                            balance[i] = element.ingreso;
                        }else if(!element.ingreso && element.gasto){
                            balance[i] = - element.gasto;
                        }else if(element.ingreso && element.gasto){
                            balance[i] = element.ingreso - element.gasto;
                        }
                    }else{            
                        balance[i] = balance[i-1] + element.ingreso - element.gasto;
                    }   
                       
                    i++;                  
                });
                balance[i] = balance[i-1]; //Por algun motivo no coge el ultimo valor del balance si no se hace esto

            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                /*alertas();*/
                console.log(e);
            }  

        
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    barChart_bal = barChart('barChart_bal',etiquetas,ingresos,gastos,balance);
    $('.barChart_bal_title span').text("Balance ( " + euros(balance.pop()) + " )");
    


    return [evtOnClick,etiquetas, ingresos, gastos];
}

/************************************************************************************/
/*Funciones de graficos*/
function pieChart(nombre,etiquetas,datos,backgroundColor,borderColor,evtOnClick){
    if (window[nombre] != undefined){
        window[nombre].destroy();
    }
    $("."+nombre).css("display", "block");
    var ctx = $('#'+nombre);

    var myChart = new Chart(ctx,{
        type: 'doughnut',
        data: {
            labels: etiquetas,
            datasets: [{
                label: nombre,
                data: datos,
                backgroundColor: backgroundColor,
                borderColor: borderColor,
                borderWidth: 1
            }]
        },
        options: {
            aspectRatio: 3,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    display: false
                }]
            },
            legend: {
                display: (window.innerWidth < 500) ? false : true,
                position: 'right',
                fullWidth: true,
                hidden: false,
                labels: {
                    fontSize: 12
                }
            },
            onResize: function(chart,size){
                //console.log(chart);
                if(size.width < 400){
                    chart.legend.options.display = false;
                }else{
                    chart.legend.options.display = true;
                }
                //console.log(data.legend.options.display)
            },
            tooltips: {
                callbacks: {
                  title: function(tooltipItem, data) { 
                    conceptoSeleccionado = data['labels'][tooltipItem[0]['index']];
                    return conceptoSeleccionado;
                    
                  },
                  label: function(tooltipItem, data) {
                    var dataset = 0;
                    $.each(data['datasets'][0]['data'], function(index, element){
                        dataset += parseInt(element);
                    });
                    //var dataset = data['datasets'][0]['_meta'][1]['total'];
                    var percent = Math.round( (data['datasets'][0]['data'][tooltipItem['index']] * 100) / dataset);
                    return euros(data['datasets'][0]['data'][tooltipItem['index']]);
                  },
                  afterLabel: function(tooltipItem, data) {
                    var dataset = 0;
                    $.each(data['datasets'][0]['data'], function(index, element){
                        dataset += parseInt(element);
                    });
                    //var dataset = data['datasets'][0]['_meta'][1]['total'];
                    var percent = Math.round( (data['datasets'][0]['data'][tooltipItem['index']] * 100) / dataset);
                    return '(' + percent + '%)';
                  },
                  footer: function(tooltipItem, data) {
                    //console.log(tooltipItem,data)
                    return ".............................";
                  }
                },
            },
            onClick: function(evt,shape){
                if (evtOnClick == 'evtOnClick' && shape.length == 1){
                    var categoria = shape[0]._chart.controller.legend.legendItems[shape[0]._index].text
                    if (nombre == 'piechart_izq'){var tipoContabilidad = 'Ingresos'}
                    else if (nombre == 'piechart_dch'){var tipoContabilidad = 'Gastos'}
                    arrayCategorias = obtenerRellenable('categorias');
                    id_categoria = (arrayCategorias.filter(array => (array.tipoContabilidad == tipoContabilidad && array.texto == categoria)))[0].id
                    datospieChart_sub = datosPieChart('concepto' + datosVentana['grupo'],tipoContabilidad,id_categoria);
                    $('.'+nombre+'_2_title').text(tipoContabilidad+" por conceptos → "+ categoria +" ( " + euros(datospieChart_sub[2]) + " )");
                    if (nombre == 'piechart_izq'){
                        if (piechart_izq_2 != undefined){
                            piechart_izq_2.destroy();
                        }
                        piechart_izq_2 = pieChart(nombre+'_2',datospieChart_sub[0],datospieChart_sub[1],datospieChart_sub[3],datospieChart_sub[4])
                    }
                    else if (nombre == 'piechart_dch'){
                        if (piechart_dch_2 != undefined){
                            piechart_dch_2.destroy();
                        }
                        piechart_dch_2 = pieChart(nombre+'_2',datospieChart_sub[0],datospieChart_sub[1],datospieChart_sub[3],datospieChart_sub[4])
                    };
                }
            }
        }
    });   
    return myChart;
}

function barChart(nombre,etiquetas,ingresos,gastos,balance){
    //console.log(unidad)
    if (window[nombre] != undefined){
        window[nombre].destroy();
    }
    $("."+nombre).css("display", "block");
    var ctx = $('#'+nombre);
    var myChart = new Chart(ctx,{
        responsive: true,
        type: 'line',
        data: {
            labels: etiquetas,
            datasets: [{
                label: "Balance",
                fill: true,
                lineTension: 0.1,
                backgroundColor: "rgba(255, 164, 32, 0.3)",
                borderColor: 'rgba(255,164,32,0.3)',
                pointBackgroundColor: 'rgba(255,164,32,0.6)',
                data: balance,
                spanGaps: false,
                pointRadius: 1,
              }, {
                label: "Ingresos",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(0, 170, 0, 0.3)",
                borderColor: "rgba(0, 170, 0, 0.6)",
                data: ingresos,
                spanGaps: true,
                showLine: false,
                pointRadius: 2,
              }, {
                label: "Gastos",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(255, 0, 0, 0.3)",
                borderColor: "rgba(255, 0, 0, 0.6)",
                data: gastos,
                spanGaps: false,
                showLine: false,
                pointRadius: 2,
              }
            ]
        },
        options: {     
            aspectRatio: 3,       
            scales: {
                // We use this empty structure as a placeholder for dynamic theming.
                xAxes: [{
                  display: (window.innerWidth < 500) ? false : true,
                  type: 'time',
                  time: {
                    displayFormats: {
                      quarter: 'MMM YYYY'
                    }
                  }
                }],
                yAxes: [{
                  //type: 'logarithmic',
                  
                  type: 'linear',
                  //stacked: true,
                  ticks: {
                      beginAtZero: true,
                      fontSize: (window.innerWidth < 500) ? 10 : 12,
                      userCallback: function(tick) {
                        return tick.toLocaleString("es",{style:"decimal", maximumFractionDigits: 1}) + " €";
                      },
                  },
                  scaleLabel: {
                      //labelString: 'Voltage',
                      //display: false
                  }
                }],
            },
            legend:{
                display: (window.innerWidth < 500) ? false : true,
                labels: {
                    fontSize: (window.innerWidth < 500) ? 6 : 12,
                }
            },
            onResize: function(chart,size){
                console.log(chart);
                if(size.width < 400){
                    chart.legend.options.display = false;
                    chart.options.scales.xAxes[0].display = false;
                    //chart.options.scales.yAxes[0].ticks.fontSize = 10; Da problemas
                    chart.legend.options.labels.fontSize = 6;
                    //chart.options.scales.yAxes[0].display = false;
                }else{
                    chart.legend.options.display = true;
                    chart.options.scales.xAxes[0].display = true;
                    //chart.options.scales.yAxes[0].ticks.fontSize = 10; Da problemas
                    chart.legend.options.labels.fontSize = 12;
                    //chart.options.scales.yAxes[0].display = true;
                }
                //console.log(data.legend.options.display)
            },
            tooltips: {
                callbacks: {
                  title: function(tooltipItem, data) {
                    return tooltipItem[0].label.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
                  },
                  /*beforeLabel: function(tooltipItem, data) {
                    if(tooltipItem.datasetIndex != 0){
                        return data.datasets[0].label +': ' +euros(data.datasets[0].data[tooltipItem.index]);
                    }                   

                  },*/
                  label: function(tooltipItem, data) {
                    return data.datasets[tooltipItem.datasetIndex].label +': ' +euros(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) 

                  },
                  footer: function(tooltipItem, data) {
                      //console.log(tooltipItem,data)
                    return ".............................................";
                  }
                },
            },
        },
    });
    return myChart;
}

function rellenarVehicleSummary(){
    console.log('entra')
    var parametros = {
        "tipo" : 'obtainVehicleSummary',
        "id_vehiculo": filtros['vehiculos'],
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_graphs.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                
                data = jQuery.parseJSON(response);
                console.log(data[0])
                $('#precioKm').html(numberToShow(data[0].eur_km, 3) + ' <sup>€/km</sup>');
                $('#consumo').html(numberToShow(data[0].l_100km, 2) + ' <sup>l/100km</sup>');
                $('#costeLitro').html(numberToShow(data[0].eur_l, 3) + ' <sup>€/l</sup>');
                $('#costeTotal').html(numberToShow(data[0].totalGastos, 2) + ' <sup>€</sup>');
                $('#kilometrosRecorridos').html(numberToShow(data[0].kilometrajeActual, 2) + ' <sup>km</sup>');
                $('#litrosRepostados').html(numberToShow(data[0].litrosTotales, 2) + ' <sup>l</sup>');
                $('#costeFinanciacion').html(numberToShow(data[0].totalFinanciacion, 2) + ' <sup>€</sup>');
                $('#costeMantenimiento').html(numberToShow(data[0].totalMantenimiento, 2) + ' <sup>€</sup>');
                $('#costeCombustible').html(numberToShow(data[0].totalCombustible, 2) + ' <sup>€</sup>');
              
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                /*alertas();*/
                console.log(e);
            }  

        
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
}

function dynamicColors() {
    var min = 70;
    var max = 220;
    if(arrayLenght <= 6){
        var escalon = (max - min);
    }else{
        var escalon = Math.ceil((max - min) / (((arrayLenght/3) - 1) / 2));
    }
    
    if(!r || !g || !b){
        r = min;
        g = min;
        b = max;
    }else if (!(r <= min) && (g >= max) && (b <= min)){
        r -= escalon;
    }else if((r <= min) && (g >= max) && !(b >= max)){
        b += escalon;
    }else if((r <= min) && !(g <= min) && (b >= max)){
        g -= escalon;
    }else if(!(r >= max) && (g <= min) && (b >= max)){
        r += escalon;
    }else if((r >= max) && (g <= min) && !(b <= min)){
        b -= escalon;
    }else if((r >= max) && !(g >= max) && (b <= min)){
        g += escalon;
    }

    return ["rgb(" + r + "," + g + "," + b + ", 0.4)" , "rgb(" + r + "," + g + "," + b + ", 0.2)"];
 };