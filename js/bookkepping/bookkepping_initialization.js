var datosVentana=[];
var filtros={FechaInicio: "",fechaFinal: "", años: "", meses: "", categorias: Array(0), conceptos: Array(0), viviendas: Array(0), vehiculos: Array(0)};
var savedArrays=[];
var initializedFilter = false;

/* Funciones ejecutadas al cargar la pagina */
$(document).ready(function() {
    savedArrays['meses'] = [
        { id: 1, texto: 'Enero', numero: 1, mesLargo: 'Enero', mesCorto: 'ene'},
        { id: 2, texto: 'Febrero', numero: 2, mesLargo: 'Febrero', mesCorto: 'feb'},
        { id: 3, texto: 'Marzo', numero: 3, mesLargo: 'Marzo', mesCorto: 'mar'},
        { id: 4, texto: 'Abril', numero: 4, mesLargo: 'Abril', mesCorto: 'abr'},
        { id: 5, texto: 'Mayo', numero: 5, mesLargo: 'Mayo', mesCorto: 'may'},
        { id: 6, texto: 'Junio', numero: 6, mesLargo: 'Junio', mesCorto: 'jun'},
        { id: 7, texto: 'Julio', numero: 7, mesLargo: 'Julio', mesCorto: 'jul'},
        { id: 8, texto: 'Agosto', numero: 8, mesLargo: 'Agosto', mesCorto: 'ago'},
        { id: 9, texto: 'Septiembre', numero: 9, mesLargo: 'Septiembre', mesCorto: 'sep'},
        { id: 10, texto: 'Octubre', numero: 10, mesLargo: 'Octubre', mesCorto: 'oct'},
        { id: 11, texto: 'Noviembre', numero: 11, mesLargo: 'Noviembre', mesCorto: 'nov'},
        { id: 12, texto: 'Diciembre', numero: 12, mesLargo: 'Diciembre', mesCorto: 'dic'}];

    $("#enterForm").on('submit',(function(e) {
        e.preventDefault();
        $.ajax({
            url: "ajax/bookkepping_general.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                $('#dataEnterModal').modal('hide');  
                applyFilter();
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'Base de datos actualizada',
                    showConfirmButton: false,
                    timer: 1000
                })
                lookForPeriodicities();
            },
            error: function (request, status, error) {
                console.log("Error: "+error);
            }          
        });
    }));
    $("#dataEnterModal").on('hidden.bs.modal',(function(e) {
        $("#enterForm")[0].reset();
        $('.vivienda').css('display', 'none');
        $('#vivienda').prop('required', false);
        $('.vehiculo').css('display', 'none');
        $('#vehiculo').prop('required', false);
        $('.usoVehiculo').css('display', 'none');
        $('#litros').prop('required',false);
        $('#cuentakilometros').prop('required',false);
        $('#concepto').prop('disabled', true);
        rellenarRellenable('concepto','');
        rellenarRellenable('vivienda','');
        rellenarRellenable('vehiculo','');
    })); 
    $("#filterForm").on('submit',(function(e) {
        e.preventDefault();
        applyFilter();
        $("#dataFilterModal").modal('hide');
    }));

    $('#categoria').on('changed.bs.select', function(){
        rellenarRellenable('concepto',obtenerRellenable('conceptos', [$('#categoria').val()]),'campoVacio'); //El filtro hay que introducirlo como array, motivo de los "[]"
        var selectedCategory = savedArrays['categorias'].find(function(element) { return element.id == $('#categoria').val(); });
        if(selectedCategory && selectedCategory.paraVivienda){
            rellenarRellenable('vivienda',obtenerRellenable('viviendas'),'campoVacio');
            $('.vivienda').css('display', 'block');
            $('#vivienda').prop('required',true);
        }else{
            $('.vivienda').css('display', 'none');
            $('#vivienda').prop('required',false);
            rellenarRellenable('vivienda','');
        };
        if(selectedCategory && selectedCategory.paraVehiculo){
            rellenarRellenable('vehiculo',obtenerRellenable('vehiculos'),'campoVacio');
            $('.vehiculo').css('display', 'block');
            $('#vehiculo').prop('required',true);
        }else{
            $('.vehiculo').css('display', 'none');
            $('.usoVehiculo').css('display', 'none');
            $('#vehiculo').prop('required',false);
            rellenarRellenable('vehiculo','');
            $('#cuentakilometros').val('');
            $('#litros').val('');
        };
        $('#concepto').selectpicker('deselectAll');
    });
    $('#categoria').on('hidden.bs.select', function(){
        if ($('#categoria').val().length == 0 ){
            rellenarRellenable('concepto','');
            rellenarRellenable('vivienda','');
            rellenarRellenable('vehiculo','');
        }
    });

    $('#filt_categoria').on('hidden.bs.select', function(){
        let selectedConcepts = $('#filt_concepto').val();
        if ($('#filt_categoria').val().length == 0 ){
            rellenarRellenable('filt_concepto',obtenerRellenable('conceptos', extractColumn(obtenerRellenable('categorias'),'id')))
        }else{
            rellenarRellenable('filt_concepto',obtenerRellenable('conceptos', $('#filt_categoria').val())) //El filtro hay que introducirlo como array, motivo de los "[]"
        }
        $('#filt_concepto').selectpicker('val', selectedConcepts);
    });

    $('#concepto').on('changed.bs.select', function(){
        var selectedConcept = savedArrays['conceptos'].find(function(element) { return element.id == $('#concepto').val(); });
        if(selectedConcept && selectedConcept.veiculo_concepto_tipo == 'Mantenimiento'){
            $('.combustible').css('display', 'none');
            $('.usoVehiculo').css('display', 'flex');
            $('.mantenimiento').css('display', 'block');
            $('#litros').prop('required',false);
            $('#litros').val('');
            $('#cuentakilometros').prop('required',false);
        }else if(selectedConcept && selectedConcept.veiculo_concepto_tipo == 'Combustible'){
            $('.mantenimiento').css('display', 'none');
            $('.usoVehiculo').css('display', 'flex');
            $('.combustible').css('display', 'block');
            $('#litros').prop('required',true);
            $('#cuentakilometros').prop('required',true);
        }else{
            $('.usoVehiculo').css('display', 'none');
            $('#litros').prop('required',false);
            $('#litros').val('');
            $('#cuentakilometros').prop('required',false);
            $('#cuentakilometros').val('');
        }
        $('#concepto').on('hidden.bs.select', function(){
            if ($('#concepto').val().length == 0 ){
                $('.usoVehiculo').css('display', 'none');
                $('#litros').prop('required',false);
                $('#litros').val('');
                $('#cuentakilometros').prop('required',false);
                $('#cuentakilometros').val('');
            }
        });
    });

    $('#fechaActivity').datetimepicker({format:'DD/MM/YYYY', locale: 'es'});

    $('#filt_fechaInicial').datetimepicker({format:'DD/MM/YYYY', locale: 'es',}).on('dp.change', function (e) {
        $('#filt_fechaFinal').data("DateTimePicker").minDate(e.date);
    });

    $('#filt_fechaFinal').datetimepicker({format:'DD/MM/YYYY', locale: 'es', useCurrent: false}).on('dp.change', function (e) {
        $('#filt_fechaInicial').data("DateTimePicker").maxDate(e.date);
    });;

});


function inicializarVentana(grupo,modoExposicion,tipoContabilidad,tipoTabla){
    //Grupo: General/Vivienda/Vehículo
    //ModoExposicion: Grafico/Tabla
    //TipoContabilidad: Ingresos/Gastos/undefined
    //TipoTabla: General/ResumenTotal/ResumenAnual/Mantenimiento/Conmbustible/Financiacion/undefined
    datosVentana['grupo'] = grupo;
    datosVentana['modoExposicion'] = modoExposicion;
    if(tipoContabilidad){
        datosVentana['tipoContabilidad'] = tipoContabilidad;
    }else{
        datosVentana['tipoContabilidad'] = '%';
    }
    datosVentana['tipoTabla'] = tipoTabla;
    if(datosVentana['tipoTabla'] == 'ResumenAnual'){
        filtros['años']=new Date().getFullYear();
    }
    if(modoExposicion == 'Tabla'){
        createTable(); 
    }else if(modoExposicion == 'Grafico'){
        createChart();
    }
    
    
};


/* Funciones ejecutadas al cargar la pagina (Segundo Plano) */
function addInAdditionModal(Titulo,Ingresos_Gastos){
    $("#tipo").val('new');
    $('#editTag').val(datosVentana['tipoContabilidad']);
    $('#dataEnterModalLabel').text(Titulo);
    rellenarRellenable('categoria',obtenerRellenable('categorias',Ingresos_Gastos),'campoVacio');
}

function addInFilterModal(){
    if(!initializedFilter){
        let categoryFiller = obtenerRellenable('categorias',datosVentana['tipoContabilidad']);
        rellenarRellenable('filt_categoria',categoryFiller);
        rellenarRellenable('filt_concepto',obtenerRellenable('conceptos', extractColumn(categoryFiller,'id')))
        rellenarRellenable('filt_año',obtenerRellenable('años'),'campoVacio');
        rellenarRellenable('filt_mes',savedArrays['meses'],'campoVacio');
        if(datosVentana['grupo'] == 'Vivienda'){
            rellenarRellenable('filt_vivienda',obtenerRellenable('viviendas'));
            $('.vivienda').css('display', 'block');
        }else if(datosVentana['grupo'] == 'Vehiculo'){
            rellenarRellenable('filt_vehiculo',obtenerRellenable('vehiculos'));
            $('.vehiculo').css('display', 'block');
        }
        if(datosVentana['tipoTabla'] == 'Combustible'){
            $('.grupoDatos').css('display', 'none');
            $('.periodoDatos').css('display', 'none');
            $('#dataFilterModalLabel').text('Filtrar elementos')
            $('#filterSwicher').css('display', 'none');
        }
        initializedFilter = true;
    }
    $('#filt_fechaIni').val(filtros['fechaInicio']);
    $('#filt_fechaFin').val(filtros['fechaFinal']);
    $('#filt_año').selectpicker('val', filtros['años']);
    $('#filt_mes').selectpicker('val', filtros['meses']);
    $('#filt_categoria').selectpicker('val', filtros['categorias']);
    $('#filt_concepto').selectpicker('val', filtros['conceptos']);
    $('#filt_vivienda').selectpicker('val', filtros['viviendas']);
    $('#filt_vehiculo').selectpicker('val', filtros['vehiculos']);
    
}

function switchFilter(){
    $(".switchFilter").toggleClass("hidden");
    $("#filt_fechaIni").val("");
    $('#filt_fechaFinal').data("DateTimePicker").minDate(false);
    $("#filt_fechaFin").val("");
    $('#filt_fechaInicial').data("DateTimePicker").maxDate(false);
    $('#filt_año').selectpicker('val', "");
    $('#filt_mes').selectpicker('val', "");
    if ($("#dataFilterModalLabel").text() == 'Filtrar elementos entre fechas'){
        $("#dataFilterModalLabel").text('Filtrar elementos por mes y año')
    }else{
        $("#dataFilterModalLabel").text('Filtrar elementos entre fechas')
    }
}

/*************************************************************************************/
function obtenerRellenable(tipo,filtro){
    var array;
    switch(tipo){
        case 'categorias': 
            var parametros = {
                "tipo" : 'obtainCategories',
                "grupo": datosVentana['grupo'],
                "tipoContabilidad": filtro,
            };     
        break;
        case 'conceptos': 
            var parametros = {
                "tipo" : 'obtainConcept',
                "id_categoria": filtro
            }; 
            
        break;
        case 'años':
            var parametros = {
                "tipo" : 'obtainYears',
            }; 
        break;
        case 'viviendas': 
            var parametros = {
                "tipo" : 'obtainHouses',
            }; 
        break;
        case 'vehiculos': 
            var parametros = {
                "tipo" : 'obtainVehicles',
        }; 
    break;
    }
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_general.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            array = jQuery.parseJSON(response);
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    
    savedArrays[tipo] = array;
    
    return array;
}

function applyFilter(){
    filtros['fechaInicio']=$('#filt_fechaIni').val().replace(/\//g, "-").replace( /(\d{2})-(\d{2})-(\d{4})/, "$3-$2-$1");
    filtros['fechaFinal']=$('#filt_fechaFin').val().replace(/\//g, "-").replace( /(\d{2})-(\d{2})-(\d{4})/, "$3-$2-$1");
    filtros['años']=$('#filt_año').val();
    filtros['meses']=$('#filt_mes').val();
    filtros['categorias']=$('#filt_categoria').val();
    filtros['conceptos']=$('#filt_concepto').val();
    filtros['viviendas']=$('#filt_vivienda').val();
    filtros['vehiculos']=$('#filt_vehiculo').val();
    //console.log(filtros['fechaInicio'] || filtros['fechaFinal'])
    if(filtros['fechaInicio'] || filtros['fechaFinal'] || filtros['años'] || filtros['meses'] || filtros['categorias'].length || filtros['conceptos'].length || filtros['viviendas'].length || filtros['vehiculos'].length){
        $('#Filter sup i').removeClass('filter-no-applied');
    }else{
        $('#Filter sup i').addClass('filter-no-applied');
    }
    
    updatePage()
}

function cancelFilter(){
    filtros['fechaInicio']="";
    filtros['fechaFinal']="";
    if(datosVentana['tipoTabla'] == 'ResumenAnual'){
        filtros['años']=new Date().getFullYear();
    }else{
          filtros['años']="";  
    }
    filtros['meses']="";
    filtros['categorias']=[];
    filtros['conceptos']=[];
    filtros['viviendas']=[]
    filtros['vehiculos']=[];
    $('#Filter sup i').addClass('filter-no-applied');
    updatePage()
}

function updatePage(){
    if(datosVentana['modoExposicion'] == 'Tabla'){
        createTable(); 
    }else if(datosVentana['modoExposicion'] == 'Grafico'){
        createChart(datosVentana['grupo']);
    }
}