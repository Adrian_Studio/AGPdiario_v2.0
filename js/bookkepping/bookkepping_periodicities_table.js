var savedArrays =[];

$(document).ready(function() {

    $("#enterForm").on('submit',(function(e) {
        e.preventDefault();
        $.ajax({
            url: "ajax/bookkepping_periodicities.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                $('#dataEnterModal').modal('hide');  
                createTable();
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'Base de datos actualizada',
                    showConfirmButton: false,
                    timer: 1000
                })
                lookForPeriodicities();
            },
            error: function (request, status, error) {
                console.log("Error: "+error);
            }          
        });
    }));
    $("#dataEnterModal").on('hidden.bs.modal',(function(e) {
        $("#enterForm")[0].reset();
    })); 

    $('#categoria').on('hidden.bs.select', function(){
        let selectedConcepts = $('#concepto').val();
        if ($('#categoria').val().length == 0 ){
            rellenarRellenable('concepto',obtenerRellenable('conceptos', []),'campoVacio'); //El filtro hay que introducirlo como array, motivo de los "[]"
        }else{
            rellenarRellenable('concepto',obtenerRellenable('conceptos', [$('#categoria').val()]),'campoVacio'); //El filtro hay que introducirlo como array, motivo de los "[]"
        }
        $('#concepto').selectpicker('val', selectedConcepts);
    });

    $('#fechaPeriodicity').datetimepicker({format:'DD/MM/YYYY', locale: 'es'});
});

function addInAdditionModal(Titulo){
    $("#tipo").val('new');
    $('#dataEnterModalLabel').text(Titulo);
    rellenarRellenable('categoria',obtenerRellenable('categorias'),'campoVacio');
    rellenarRellenable('concepto',obtenerRellenable('conceptos'),'campoVacio');
}

/*Funcion para crear las tablas*/
function createTable() {

    datosTabla = tableData();
    columnas = datosTabla[0];
    datos = datosTabla[1];
    
    if ( $.fn.DataTable.isDataTable( '#datatable' ) ) {
        $('#datatable').DataTable().destroy();
        $('#datatable').empty();
    };

    table = $('#datatable').DataTable({
        responsive: true,
        columnDefs: [
            { render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' ), targets: 3 },
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 4 },
            { responsivePriority: 3, targets: 1 },
        ],
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        data: datos,
        columns: columnas,
        "pagingType": ($(window).width() > 768) ? 'simple_numbers' : 'simple',
    });

    return (table);
 };

/************************************************************************************/
/*Funciones para obtencion de los datos de las tablas*/

function tableData(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainTable',
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_periodicities.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = JSON.parse(response);

                datosColumnas = [{data: 'Categoria', title: 'Categoría'}, {data: 'Concepto', title: 'Concepto'}, {data: 'Periodicidad (Meses)', title: 'Periodicidad (Meses)'}, {data: 'Fecha inicio', title: 'Fecha inicio'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    datosTabla[index] = {Categoria: element.categoria, Concepto: element.concepto, 'Periodicidad (Meses)': element.periodicidad_meses, 'Fecha inicio': element.fecha_inicio, Edit: ' <a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'}
                });
            }catch (e){
                console.log(e);
            }
            

        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    //console.log([datosColumnas,datosTabla])
    return [datosColumnas,datosTabla];
}

function editarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    $("#tipo").val('update');
    $("#editTag").val(a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_periodicities.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);
                //console.log(data);
                rellenarRellenable('categoria',obtenerRellenable('categorias'),'campoVacio');
                rellenarRellenable('concepto',obtenerRellenable('conceptos'),'campoVacio')
                $('#dataEnterModal #categoria').selectpicker('val', data[0].id_categoria);
                $('#dataEnterModal #concepto').selectpicker('val', data[0].id_concepto);
                $('#dataEnterModal #fecha').val(data[0].fecha_inicio.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                $('#dataEnterModal #periodicidad').val(data[0].periodicidad_meses);

                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });

    $('#dataEnterModalLabel').text("Editar elemento");
   
        //$(this).find('form')[0].reset();
}

function borrarItem(a){
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            var parametros = {
                "tipo" : 'delete',
                "table_id" : a
            }; 

            $.ajax({
                data:  parametros,
                url:   'ajax/bookkepping_periodicities.php',
                type:  'post',
                async: false,
                beforeSend: function () {
                    $("#resultado").html("Procesando, espere por favor...");
                    
                },
                success:  function (response) {
                    try {
                        
                        createTable();
                        console.log("Elemento con id = "+a+" borrado con exito"); 
                        Swal.fire({
                            position: 'top',
                            type: 'success',
                            title: 'Base de datos actualizada',
                            showConfirmButton: false,
                            timer: 1000
                        })
                        lookForPeriodicities();
                        
                    } catch (e) {
                        $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                        /*alertas();*/
                        console.log(e);
                    }  
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        } 
    })
}

function obtenerRellenable(tipo,filtro){
    switch(tipo){
        case 'categorias': 
            var parametros = {
                "tipo" : 'obtainCategories',
            };     
        break;
        case 'conceptos': 
            var parametros = {
                "tipo" : 'obtainConcept',
                "id_categoria": filtro
            }; 
        break;
    }
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_general.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            array = jQuery.parseJSON(response);
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    
    savedArrays[tipo] = array;
    
    return array;
}