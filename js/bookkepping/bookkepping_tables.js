/*Funcion para crear las tablas*/
function createTable() {
    switch(datosVentana['tipoTabla']) {
        case 'General':
            switch (datosVentana['grupo']) {
                case 'General':
                    datosTabla = datosTablaGenerales();
                    columnas = datosTabla[0];
                    datos = datosTabla[1];
                    break;
                case 'Vivienda':
                    datosTabla = datosTablaVivienda();
                    columnas = datosTabla[0];
                    datos = datosTabla[1];
                    break;
                default:
                    break;
            }
            break;
        case 'Financiacion':
            $('.filtroVehiculo').css('display', 'block');
            datosTabla = datosTablaFinanciacion();
            columnas = datosTabla[0];
            datos = datosTabla[1];
            break;
        case 'Mantenimiento':
            $('.filtroVehiculo').css('display', 'block');
            datosTabla = datosTablaMantenimiento();
            columnas = datosTabla[0];
            datos = datosTabla[1];
            break;
        case 'Combustible':
            $('.filtroVehiculo').css('display', 'block');
            datosTabla = datosTablaCombustible();
            columnas = datosTabla[0];
            datos = datosTabla[1];
            break;
        case 'ResumenTotal':
            $('.filtroCategorizacion').css('display', 'none');
            $('.filtroFechas').css('display', 'none');
            $('.filtroAño').css('display', 'block');
            datosTabla = datosTablaResumenTotal();
            columnas = datosTabla[0];
            datos = datosTabla[1];
            clase = datosTabla[2];
            
            break;
        case 'ResumenAnual':
            $('.filtroCategorizacion').css('display', 'none');
            $('.filtroFechas').css('display', 'none');
            $('.filtroAño').css('display', 'block');
            $('#filtroAño').selectpicker('deselectAll')
            $('#filtroAño').selectpicker('val', moment().format('YYYY'));
            datosTabla = datosTablaResumenAnual();
            columnas = datosTabla[0];
            datos = datosTabla[1];
            clase = datosTabla[2];

            break;
         default:
            break;
    };
    
    if ( $.fn.DataTable.isDataTable( '#datatable' ) ) {
        $('#datatable').DataTable().destroy();
        $('#datatable').empty();
    };
    if (datosVentana['tipoTabla'] == 'ResumenTotal' || datosVentana['tipoTabla'] == 'ResumenAnual') {
        table = $('#datatable').DataTable({
            responsive: true,
            columnDefs: [
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: 1 },
            ],
            "language": {
                "decimal": ",",
                "thousands": "."
            },
            data: datos,
            columns: columnas,
            "createdRow": function( row, data, dataIndex ) {
                  $(row).addClass( clase[dataIndex] );
              },
            //order: [[ 0, "desc" ]],
            "ordering": false,
            //"autoWidth": false,
            "pageLength": -1,
            "paging": false,
            "searching": false,
            "info": false,
        });
    }else if (datosVentana['tipoTabla'] == 'Combustible') {
        table = $('#datatable').DataTable({
            responsive: true,
            columnDefs: [
                { render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' ), targets: 0 },
                { type: 'formated-sorting', targets: [1,2,3,4,5,6,7,8] },
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: 5 },
                { responsivePriority: 3, targets: 3 },
                { responsivePriority: 4, targets: 2 },
                { responsivePriority: 5, targets: 1 },
                { responsivePriority: 6, targets: 4 },
            ],
            "language": {
                "decimal": ",",
                "thousands": "."
            },
            data: datos,
            columns: columnas,
            order: [[ 0, "desc" ]],
            "pagingType": ($(window).width() > 768) ? 'simple_numbers' : 'simple',
        });
    }else{
        table = $('#datatable').DataTable({
            responsive: true,
            columnDefs: [
                { render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' ), targets: 0 },
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: 5 },
                { responsivePriority: 3, targets: 3 },
                { responsivePriority: 4, targets: 2 },
                { responsivePriority: 5, targets: 1 },
                { responsivePriority: 6, targets: 4 },
            ],
            "language": {
                "decimal": ",",
                "thousands": "."
            },
            data: datos,
            columns: columnas,
            order: [[ 0, "desc" ]],
            "pagingType": ($(window).width() > 768) ? 'simple_numbers' : 'simple',
        });
    }

    return (table);
 };

/************************************************************************************/
/*Funciones para obtencion de los datos de las tablas*/

function datosTablaGenerales(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainGeneralTable',
        "tipoContabilidad": datosVentana['tipoContabilidad'],
        "fechaInicio": filtros['fechaInicio'],
        "fechaFin": filtros['fechaFinal'],
        "año": filtros['años'],
        "mes": filtros['meses'],
        "categoria": filtros['categorias'],
        "concepto": filtros['conceptos'],
        "id_vivienda": filtros['viviendas'],
        "id_vehiculo": filtros['vehiculos'],
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_tables.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = JSON.parse(response);
                //console.log(data);
                datosColumnas = [{data: 'Fecha', title: 'Fecha'}, {data: 'Categoría', title: 'Categoría'}, {data: 'Concepto', title: 'Concepto'}, {data: 'Importe', title: 'Importe'}, {data: 'Observaciones', title: 'Observaciones'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    datosTabla[index] = {Fecha: element.fecha, Categoría: element.categoria, Concepto: element.concepto, Importe: euros_sup(element.importe), Observaciones: element.observaciones, Edit: ' <a href="#" class="popoverReference edit-icon" title="Copiar elemento" data-content="Copiar datos del elemento" onclick="copiarDatos(this.id);" id='+element.id+'><i class="fa fa-copy"></i></a><a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'}
                });
            }catch (e){
                console.log(e);
            }
            

        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    //console.log([datosColumnas,datosTabla])
    return [datosColumnas,datosTabla];
}

function datosTablaVivienda(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainHomeTable',
        "tipoContabilidad": datosVentana['tipoContabilidad'],
        "fechaInicio": filtros['fechaInicio'],
        "fechaFin": filtros['fechaFinal'],
        "año": filtros['años'],
        "mes": filtros['meses'],
        "categoria": filtros['categorias'],
        "concepto": filtros['conceptos'],
        "id_vivienda": filtros['viviendas'],
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_tables.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = JSON.parse(response);
                datosColumnas = [{data: 'Fecha', title: 'Fecha'}, {data: 'Categoría', title: 'Categoría'}, {data: 'Concepto', title: 'Concepto'}, {data: 'Importe', title: 'Importe'}, {data: 'Observaciones', title: 'Observaciones'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    datosTabla[index] = {Fecha: element.fecha, Categoría: element.categoria, Concepto: element.concepto, Importe: euros_sup(element.importe), Observaciones: element.observaciones, Edit: ' <a href="#" class="popoverReference edit-icon" title="Copiar elemento" data-content="Copiar datos del elemento" onclick="copiarDatos(this.id);" id='+element.id+' ><i class="fa fa-copy"></i></a><a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'}
                });
            }catch (e){
                console.log(e);
            }
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    return [datosColumnas,datosTabla];
}

function datosTablaCombustible(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainGasTable',
        "tipoConcepto": datosVentana['tipoTabla'],
        "fechaInicio": filtros['fechaInicio'],
        "fechaFin": filtros['fechaFinal'],
        "año": filtros['años'],
        "mes": filtros['meses'],
        "categoria": filtros['categorias'],
        "concepto": filtros['conceptos'],
        "id_vehiculo": filtros['vehiculos'],
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_tables.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try{
                var recorrido, km_lit, lit_100km, euros_km, euros_lit;
                var data = JSON.parse(response);
                datosColumnas = [{data: 'Fecha', title: 'Fecha'}, {data: 'Cuentakilometros', title: 'Kilómetros'}, {data: 'Litros', title: 'Litros'}, {data: 'Importe', title: 'Importe'}, {data: 'Km_recorridos', title: 'Km recorridos'}, {data: 'Km_L', title: 'Km/L'}, {data: 'L_100Km', title: 'L/100Km'}, {data: 'eur_Km', title: '€/Km'}, {data: 'eur_L', title: '€/L'}, {data: 'Observaciones', title: 'Observaciones'}, {data: 'Edit', title: 'Editar'}]
                data = JSON.parse(response);
                $.each(data, function(index, element) {
                    if (!data[index+1] || data[index].id_vehiculo != data[index+1].id_vehiculo){
                        recorrido = null;
                        km_lit = null;
                        lit_100km = null;
                        euros_km = null;
                    }else{
                        recorrido = element.cuenta_km - data[index+1].cuenta_km;
                        km_lit = numberToShow(recorrido / element.litros, 2) + ' <sup>km/l</sup>';
                        lit_100km = numberToShow((element.litros * 100) / recorrido, 2) + ' <sup>l/100km</sup>';
                        euros_km = numberToShow(element.importe / recorrido, 3) + ' <sup>€/l</sup>';
                        recorrido =numberToShow(recorrido, 0)+ ' <sup>km<s/up>';
                    }
                    euros_lit = numberToShow(element.importe / element.litros, 3)+ ' <sup>€/km</sup>';
                    cuentaKilometros = numberToShow(element.cuenta_km, 0)+ ' <sup>km</sup>';
                    litros= numberToShow(element.litros, 2)+ ' <sup>l</sup>';
                    importe = euros_sup(element.importe)
                    datosTabla[index] = {Fecha: element.fecha, Cuentakilometros: cuentaKilometros, Litros: litros , Importe: importe, Km_recorridos: recorrido, Km_L: km_lit, L_100Km: lit_100km, eur_Km: euros_km, eur_L: euros_lit, Observaciones: element.observaciones, Edit: ' <a href="#" class="popoverReference edit-icon" title="Copiar elemento" data-content="Copiar datos del elemento" onclick="copiarDatos(this.id);" id='+element.id+' ><i class="fa fa-copy"></i></a><a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'}
                });
                //console.log(data);
            }catch (e){
                console.log(e);
            }
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    return [datosColumnas,datosTabla];
}

function datosTablaMantenimiento(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainMaintenanceTable',
        "tipoConcepto": datosVentana['tipoTabla'],
        "fechaInicio": filtros['fechaInicio'],
        "fechaFin": filtros['fechaFinal'],
        "año": filtros['años'],
        "mes": filtros['meses'],
        "categoria": filtros['categorias'],
        "concepto": filtros['conceptos'],
        "id_vehiculo": filtros['vehiculos'],
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_tables.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try{
                var data = JSON.parse(response);
                datosColumnas = [{data: 'Fecha', title: 'Fecha'}, {data: 'Concepto', title: 'Concepto'}, {data: 'Importe', title: 'Importe'}, {data: 'Cuentakilometros', title: 'Cuentakilómetros'}, {data: 'Observaciones', title: 'Observaciones'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    if(element.cuenta_km == 0){ var cuenta_km = null;}else { var cuenta_km = numberToShow(element.cuenta_km, 0)+ ' km';;};
                    datosTabla[index] = {Fecha: element.fecha, Concepto: element.concepto, Importe: euros_sup(element.importe), Cuentakilometros: cuenta_km, Observaciones: element.observaciones, Edit: ' <a href="#" class="popoverReference edit-icon" title="Copiar elemento" data-content="Copiar datos del elemento" onclick="copiarDatos(this.id);" id='+element.id+' ><i class="fa fa-copy"></i></a><a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'}
                });
                //console.log(data);
            }catch (e){
                console.log(e);
            }
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    return [datosColumnas,datosTabla];
}

function datosTablaFinanciacion(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainFinanciationTable',
        "tipoConcepto": datosVentana['tipoTabla'],
        "fechaInicio": filtros['fechaInicio'],
        "fechaFin": filtros['fechaFinal'],
        "año": filtros['años'],
        "mes": filtros['meses'],
        "categoria": filtros['categorias'],
        "concepto": filtros['conceptos'],
        "id_vehiculo": filtros['vehiculos'],
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_tables.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try{
                var data = JSON.parse(response);
                datosColumnas = [{data: 'Fecha', title: 'Fecha'}, {data: 'Categoria', title: 'Categoría'}, {data: 'Concepto', title: 'Concepto'}, {data: 'Importe', title: 'Importe'}, {data: 'Observaciones', title: 'Observaciones'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    datosTabla[index] = {Fecha: element.fecha, Categoria: element.categoria, Concepto: element.concepto, Importe: euros_sup(element.importe), Observaciones: element.observaciones, Edit: ' <a href="#" class="popoverReference edit-icon" title="Copiar elemento" data-content="Copiar datos del elemento" onclick="copiarDatos(this.id);" id='+element.id+' ><i class="fa fa-copy"></i></a><a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'}
                });
                //console.log(data);
            }catch (e){
                console.log(e);
            }
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    return [datosColumnas,datosTabla];
}
function datosTablaResumenTotal(){
    var datosTabla = [];
    var datosColumnas = [];
    var datosClase = [];
    var parametros = {
        "tipo" : 'obtainTotalSummaryTable',
        "año": filtros['años'],
        "grupo": datosVentana['grupo'],
        "categoria": filtros['categorias'],
        "concepto": filtros['conceptos'],
        "id_vivienda": filtros['viviendas'],
        "id_vehiculo": filtros['vehiculos'],
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_tables.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try{
                var i=0;
                data = JSON.parse(response);
                datosColumnas.push({data: 'GRUPO', title: 'GRUPO'});
                datosColumnas.push({data: 'Total', title: 'Total'});
                datosColumnas.push({data: 'Media', title: 'Media'});
                $.each(data['columnas'], function(index, element) {
                    datosColumnas.push({data: element.toString(), title: element.toString()});
                });

                $.each(data , function(index, element) {
                    //console.log(data.length);
                    if(element['GRUPO'] != undefined){

                        datosTabla[i] = [];
                        datosTabla[i]["GRUPO"] = '<b>' + element['GRUPO'] + '</b>';
                        datosTabla[i]["Total"] = euros_sup(element['Total']);
                        
                        for (var j = 1; j<datosColumnas.length; j++){
                            
                            if(element[datosColumnas[j]['data']] != undefined){
                                datosTabla[i][datosColumnas[j]['data']] = '<b>' + euros_sup(element[datosColumnas[j]['data']]) + '</b>';
                            }else{
                                datosTabla[i][datosColumnas[j]['data']] = null;
                            }
                        }
                        datosTabla[i]["Media"] = euros_sup(element['Total']/(datosColumnas.length - 3));
                        datosClase[i] = element['Class']
                        i++;
                    }
                });
            }catch (e){
                console.log(e);
            }               
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    
    return [datosColumnas,datosTabla,datosClase];
}

function datosTablaResumenAnual(){
    var datosTabla = [];
    var datosColumnas = [];
    var datosClase = [];
    var parametros = {
        "tipo" : 'obtainAnnualSummaryTable',
        "año": filtros['años'],
        "grupo": datosVentana['grupo'],
        "categoria": filtros['categorias'],
        "concepto": filtros['conceptos'],
        "id_vivienda": filtros['viviendas'],
        "id_vehiculo": filtros['vehiculos'],
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_tables.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try{
                var i=0;
                data = JSON.parse(response);
                datosColumnas.push({data: 'GRUPO', title: 'GRUPO'});
                datosColumnas.push({data: 'Total', title: 'Total'});
                datosColumnas.push({data: 'Media', title: 'Media'});
                datosColumnas.push({data: 1, title: 'Enero'});
                datosColumnas.push({data: 2, title: 'Febrero'});
                datosColumnas.push({data: 3, title: 'Marzo'});
                datosColumnas.push({data: 4, title: 'Abril'});
                datosColumnas.push({data: 5, title: 'Mayo'});
                datosColumnas.push({data: 6, title: 'Junio'});
                datosColumnas.push({data: 7, title: 'Julio'});
                datosColumnas.push({data: 8, title: 'Agosto'});
                datosColumnas.push({data: 9, title: 'Septiembre'});
                datosColumnas.push({data: 10, title: 'Octubre'});
                datosColumnas.push({data: 11, title: 'Noviembre'});
                datosColumnas.push({data: 12, title: 'Diciembre'});
                
                $.each(data , function(index, element) {
                    //console.log(data.length);
                    
                    if(element['GRUPO'] != undefined){
                        
                        datosTabla[i] = [];
                        datosTabla[i]["GRUPO"] = '<b>' + element['GRUPO'] + '</b>';
                        datosTabla[i]["Total"] = euros_sup(element['Total']);

                        for (var j = 1; j<datosColumnas.length; j++){
                            
                            if(element[datosColumnas[j]['data']] != undefined){
                                datosTabla[i][datosColumnas[j]['data']] = '<b>' +euros_sup(element[datosColumnas[j]['data']]) + '</b>';
                            }else{
                                datosTabla[i][datosColumnas[j]['data']] = null;
                            }
                        }
                        datosTabla[i]["Media"] = euros_sup(element['Total']/12);
                        datosClase[i] = element['Class']
                        i++;
                    }
                });
                
            }catch (e){
                console.log(e);
            }               
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    
    return [datosColumnas,datosTabla,datosClase];
}

