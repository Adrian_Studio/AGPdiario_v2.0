$(document).ready(function() {

    $("#enterForm").on('submit',(function(e) {
        e.preventDefault();
        $.ajax({
            url: "ajax/dropdowns/bookkepping_category_table.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                $('#dataEnterModal').modal('hide');  
                createTable();
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'Base de datos actualizada',
                    showConfirmButton: false,
                    timer: 1000
                })
            },
            error: function (request, status, error) {
                console.log("Error: "+error);
            }          
        });
    }));
    $("#dataEnterModal").on('hidden.bs.modal',(function(e) {
        $('#paraVivienda').val(false);
        $('#paraVehiculo').val(false);
        $('.edit-warning').css('display', 'none');
        $("#enterForm")[0].reset();
    })); 
});

function addInAdditionModal(Titulo,tipoContabilidad){
    $("#tipo").val('new');
    $('#editTag').val(tipoContabilidad);
    $('#dataEnterModalLabel').text(Titulo);
    $('#check-paraVivienda')[0].disabled = false;
    $('#check-paraVehiculo')[0].disabled = false;
}

/*Funcion para crear las tablas*/
function createTable() {

    datosTabla = tableData();
    columnas = datosTabla[0];
    datos = datosTabla[1];
    
    if ( $.fn.DataTable.isDataTable( '#datatable' ) ) {
        $('#datatable').DataTable().destroy();
        $('#datatable').empty();
    };

    table = $('#datatable').DataTable({
        responsive: true,
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 4 },
            { responsivePriority: 3, targets: 1 },
        ],
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        data: datos,
        columns: columnas,
        order: [[ 1, "asc" ]],
        "pagingType": ($(window).width() > 768) ? 'simple_numbers' : 'simple',
    });

    return (table);
 };

/************************************************************************************/
/*Funciones para obtencion de los datos de las tablas*/

function tableData(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainTable',
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/dropdowns/bookkepping_category_table.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = JSON.parse(response);
                //console.log(data);
                datosColumnas = [{data: 'Tipo contabilidad', title: 'Tipo contabilidad'}, {data: 'Categoría', title: 'Categoría'}, {data: 'Para vivienda', title: 'Para vivienda'}, {data: 'Para vehiculo', title: 'Para vehículo'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    datosTabla[index] = {'Tipo contabilidad': element.tipoContabilidad, Categoría: element.categoria, 'Para vivienda': (element.paraVivienda) ? 'Sí' : '', 'Para vehiculo': (element.paraVehiculo) ? 'Sí' : '', Edit: ' <a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'}
                });
            }catch (e){
                console.log(e);
            }
            

        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    //console.log([datosColumnas,datosTabla])
    return [datosColumnas,datosTabla];
}

function editarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    $("#tipo").val('update');
    $("#editTag").val(a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/dropdowns/bookkepping_category_table.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);
                $('#dataEnterModal #categoria').val(data[0].categoria);
                if (data[0].paraVivienda){
                    $('#check-paraVivienda')[0].checked = true;
                    $('#paraVivienda').val(1);
                } else {
                    $('#check-paraVivienda')[0].checked = false;
                    $('#paraVivienda').val(0);
                }
                if (data[0].paraVehiculo){
                    $('#check-paraVehiculo')[0].checked = true;
                    $('#paraVehiculo').val(1);
                } else {
                    $('#check-paraVehiculo')[0].checked = false;
                    $('#paraVehiculo').val(0);
                }
                $('#check-paraVivienda')[0].disabled = true;
                $('#check-paraVehiculo')[0].disabled = true;
                $('.edit-warning').css('display', 'block');
                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });

    $('#dataEnterModalLabel').text("Editar elemento");
   
        //$(this).find('form')[0].reset();
}

function borrarItem(a){
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {   
            var parametros = {
                "tipo" : 'delete',
                "table_id" : a
            }; 

            $.ajax({
                data:  parametros,
                url:   'ajax/dropdowns/bookkepping_category_table.php',
                type:  'post',
                async: false,
                beforeSend: function () {
                    $("#resultado").html("Procesando, espere por favor...");
                    
                },
                success:  function (response) {
                    try {
                        
                        createTable();
                        console.log("Elemento con id = "+a+" borrado con exito"); 
                        Swal.fire({
                            position: 'top',
                            type: 'success',
                            title: 'Base de datos actualizada',
                            showConfirmButton: false,
                            timer: 1000
                        })
                        
                    } catch (e) {
                        $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                        /*alertas();*/
                        console.log(e);
                    }  
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        } 
    })
}
