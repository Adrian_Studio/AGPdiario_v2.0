(function($) {
  "use strict"; // Start of use strict
  //Abrir el menu en la pagina actual
  var url = window.location;
  var lookedId = url.pathname.substr(url.pathname.lastIndexOf('/')+1,url.pathname.length );

  var element = $('ul.navbar-nav a').filter(function() {
      return this.id == lookedId;
  });

  while (true) {
    
    if (element.parent().is('.nav-item')) {
      element = element.parent().addClass('active');
    } else if (element.parent().parent().parent().is('.nav-item')) {
      element.addClass('active');
      element.parent().parent().parent().addClass('active');
      if(varibleValue(url.href,'menu') == 'extended' && !(window.innerWidth < 768)){
        element.parent().parent().parent().children('.nav-link').removeClass('collapsed');
        element.parent().parent().parent().children('div').addClass('show');
      }
      element = element.parent();
    } else {
      break;
    }
  }

  //console.log(document.cookie)
  document.cookie = "menu=extended"

  //console.log(document.cookie);
  $(window).scroll(function() {
    //$(".menu").css("top", window.pageYOffset);
    //console.log(window.pageYOffset);

  });
  if(varibleValue(url.href,'menu') == 'compressed'){
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  }
  checkBookeppingGroups()
  
})(jQuery); // End of use strict

function redirectPage(url){
  var menu = 'menu=';
  if($(".sidebar").hasClass("toggled")){
    menu += 'compressed'
  }else{
    menu += 'extended'
  }
  var variables = menu;
  //variables += '&' + otra;

  location.href = url + "?" + variables;
}
function varibleValue(url,variable){
  return url.substr(url.indexOf(variable)+variable.length+1,url.length);
}
 
function checkBookeppingGroups(){
  if(findIfGrupo('paraVivienda')){
    $('.nav-item.collapseHouseBookkepping').removeClass('hidden');
  };
  if(findIfGrupo('paraVehiculo')){
    $('.nav-item.collapseVehicleBookkepping').removeClass('hidden');
  };
}

function findIfGrupo(grupo){
  var array;
  var parametros = {
    "tipo" : 'checkIf',
    "grupo" : grupo
}; 
  $.ajax({
    data:  parametros,
    url:   'ajax/dropdowns/bookkepping_category_table.php',
    type:  'post',
    async: false,
    beforeSend: function () {
        $("#resultado").html("Procesando, espere por favor...");
        
    },
    success:  function (response) {
        try {
            var data = jQuery.parseJSON(response);
            array = data;
        } catch (e) {
            $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
            //alertas();
            console.log(e);
        }  
    },
    error: function (request, status, error) {
        console.log(error);
    }
  });
  if(array.length){
    return true;
  }else{
    return false;
  }
}