
function limitText(limitField, limitCount, limitNum) {
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    } else {
        limitCount.value = limitField.value.length;
        
    }
$('#cuentaTextArea').text(limitCount.value);
}

function rellenarRellenable(rellenable,datosRellenable,campoVacio){
    $('#'+rellenable).prop('disabled', false);
    if(datosRellenable == ''){
        $('#'+rellenable).empty()
        $('#'+rellenable).selectpicker('refresh');
        $('#'+rellenable).selectpicker('deselectAll');
        $('#'+rellenable).prop('disabled', true);
        $('#'+rellenable).selectpicker('refresh');
        return;
    }    

    $('#'+rellenable).empty()

    if (campoVacio =='campoVacio'){ 
        $('#'+rellenable).append(
            $('<option></option>')
        );
    }
    if(datosRellenable == []){return;}
    /*datosRellenable.sort(function (a, b) {
        if(typeof a.texto === 'string' && typeof b.texto === 'string'){
            return a.texto.localeCompare(b.texto);
        }
    })*/
    $.each(datosRellenable, function(index, element) {
        $('#'+rellenable).append(
            $('<option></option>')
            .text(element.texto)
            .val(element.id)
            );
    });
    $('#'+rellenable).selectpicker('refresh')
    //$('#'+rellenable).selectpicker('selectAll')

}