//Devuelve la diferencia de tiempro entre la fecha introducida y hoy en años, minutos y segundos (no es exacta)
//Esta integrada la funcion "timeUntilNow", simplemente hay que dejar "fechaFin" vacia
function timeBetweenDates(fechaInicio,fechaFin){
    var starts = moment(fechaInicio);
    if (fechaFin ==""){
        var ends   = moment();
    }else{
        var ends   = moment(fechaFin);
    }
        
    var diff = moment.duration(ends.diff(starts));

    return (diff._data.years+"A "+diff._data.months+"M "+diff._data.days+"D");
}

//Devuelve la diferencia de tiempro entre la fecha introducida y hoy en años, minutos y segundos (no es exacta)
function timeUntilNow(fechaInicio){
    var starts = moment(fechaInicio);
    var ends   = moment();
    var diff = moment.duration(ends.diff(starts));

    return (diff._data.years+"A "+diff._data.months+"M "+diff._data.days+"D");
}

// Devuelve el mismo array introducido sin los valores duplicados
function eliminateArrayDuplicates(array){
    let unique_array = []
    for(let i = 0;i < array.length; i++){
        if(unique_array.indexOf(array[i]) == -1){
            unique_array.push(array[i])
        }
    }
    return unique_array
}

// Devuelve en euros el valor introducido
function euros(quantity){
    return $.number(quantity, 2, ',', '.' ) + " €"
}
// Devuelve en euros en superindice el valor introducido
function euros_sup(quantity){
    return $.number(quantity, 2, ',', '.' ) + " <sup>€</sup>"
}
//Devuelve en formato numero el valor introducido
function numberToShow(numero,decimales){
    return $.number(numero, decimales, ',', '.' );
}
function extractColumn(arr, column) {
    return arr.map(x => x[column])
}
function timeBetweenDatesInMonths(fechaInicio,fechaFin){
    var starts = moment(fechaInicio);
    if (fechaFin ==""){
        var ends   = moment();
    }else{
        var ends   = moment(fechaFin);
    }
        
    var diff = moment.duration(ends.diff(starts));

    return (diff._data.years*12+diff._data.months);
}