var periodicityCount;
(function($) {
  "use strict"; // Start of use strict
  lookForPeriodicities();
  lookForProgramedTrips();
  lookForNotesWithAlert();

})(jQuery); // End of use strict

function lookForPeriodicities(){
  periodicityCount = 0;
  $('#periodicitiesDropdownList').empty();
  var parametros = {
      "tipo" : 'obtainTable',
  };
  $.ajax({
      data:  parametros,
      url:   'ajax/bookkepping_periodicities.php',
      type:  'post',
      async: false,
      beforeSend: function () {
          $("#resultado").html("Procesando, espere por favor...");
          
      },
      success:  function (response) {
          try {
              var data = JSON.parse(response);
              //console.log(data)
              $.each(data, function(index, element) {
                lookForPeridoicityData(element);
              });
          }catch (e){
              console.log(e);
          }
          

      },
      error: function (request, status, error) {
          console.log(error);
      }
  });
  if(periodicityCount){
    $('#periodicitiesDropdownIcon span').text(periodicityCount);
    $('#periodicitiesDropdownIcon span').removeClass('hidden');
    $('#periodicitiesDropdown').removeClass('hidden');
  }else{
    $('#periodicitiesDropdownIcon span').addClass('hidden');
    $('#periodicitiesDropdown').addClass('hidden');
  }
  //console.log([datosColumnas,datosTabla])
}

function lookForPeridoicityData(dataPeriodicity){

  var alertStart = new Date(moment().subtract(2, 'months').format('L'));
  var alertEnd = new Date(moment().subtract(0, 'months').format('L'));
  var dataStart = new Date(dataPeriodicity.fecha_inicio);
  if(dataStart > alertStart){
    alertStart = dataStart;
  }else{
    alertStart = new Date(alertStart.setDate(dataStart.getDate()));
  }

  for (var alert = dataStart; alert <= alertEnd; alert = new Date(moment(alert).add(dataPeriodicity.periodicidad_meses, 'months').format('L'))) {
    var alertNext = new Date(alert);
    alertNext = new Date(moment(alertNext).add(dataPeriodicity.periodicidad_meses, 'months').format('L'));



    if (alert < alertStart /*|| alertNext > new Date(moment().format('YYYY-MM-DD') + ' 23:59:59')*/){
      continue;
    }

    var monthStart = new Date(moment(alert).startOf('month').format('YYYY-MM-DD'));
    var monthEnd = new Date(moment(alert).endOf('month').format('YYYY-MM-DD'));

    var parametros = {
        "tipo" : 'obtainDataPeriodicities',
        "id_concepto": dataPeriodicity.id_concepto,
        "fechaInicio": moment(monthStart).format('YYYY-MM-DD'),
        "fechaFinal": moment(monthEnd).format('YYYY-MM-DD'),
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_periodicities.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
              var data = JSON.parse(response);
              if (!data.length){
                $('#periodicitiesDropdownList').append('<a class="dropdown-item d-flex align-items-center" href="#"><div><div class="small text-gray-500">' + moment(alert).locale("es").format('MMMM [de] YYYY') + '</div><span>' + 'El importe con categoría "' + dataPeriodicity.categoria + '" y concepto: "' + dataPeriodicity.concepto + '" esta sin introducir' + '</span></div></a>');
                periodicityCount += 1;    
              }
            }catch (e){
                console.log(e);
            }
            

        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
  }
}

function lookForProgramedTrips(){
  tripsCount = 0;
  $('#tripsDropdownList').empty();
  var parametros = {
      "tipo" : 'obtainProgramedTrips',
  };
  $.ajax({
      data:  parametros,
      url:   'ajax/trips.php',
      type:  'post',
      async: false,
      beforeSend: function () {
          $("#resultado").html("Procesando, espere por favor...");
          
      },
      success:  function (response) {
          try {
              var data = JSON.parse(response);
              $.each(data, function(index, element) {
                $('#tripsDropdownList').append('<a class="dropdown-item d-flex align-items-center" href="#"><div><div class="small text-gray-500">' + moment(element.fecha).locale("es").format('LL') + '</div><span>' + 'Viaje programado a ' + ((element.lugar) ? (element.lugar + ' en ')  : '') + element.nombre + ((element.acompañantes) ? (' con ' + element.acompañantes)  : '') +'</span></div></a>');
                tripsCount += 1; 
              });
          }catch (e){
              console.log(e);
          }
          

      },
      error: function (request, status, error) {
          console.log(error);
      }
  });
  if(tripsCount){
    $('#tripsDropdownIcon span').text(tripsCount);
    $('#tripsDropdownIcon span').removeClass('hidden');
    $('#tripsDropdown').removeClass('hidden');
  }else{
    $('#tripsDropdownIcon span').addClass('hidden');
    $('#tripsDropdown').addClass('hidden');
  }
  //console.log([datosColumnas,datosTabla])
}
function lookForNotesWithAlert(){
  notesCount = 0;
  $('#notesDropdownList').empty();
  var parametros = {
      "tipo" : 'obtainNotesWithAlert',
  };
  $.ajax({
      data:  parametros,
      url:   'ajax/notes.php',
      type:  'post',
      async: false,
      beforeSend: function () {
          $("#resultado").html("Procesando, espere por favor...");
          
      },
      success:  function (response) {
          try {
              var data = JSON.parse(response);
              //console.log(data)
              $.each(data, function(index, element) {
                var fechaProximoAviso = new Date(element.fecha_aviso + ' 23:59:59');
                var today = new Date();

                if(element.periodicidad_activa && element.periodicidad_meses){
                  var i=1;
                  while (fechaProximoAviso < today) {
                    fechaProximoAviso = new Date(moment(element.fecha_aviso + ' 23:59:59').add(element.periodicidad_meses * i, 'months').format('L'));
                    i++
                  }
                }
                if(element.tiempo_aviso_dias){
                  var tiempo_aviso = element.tiempo_aviso_dias
                }else{
                  var tiempo_aviso = 1;
                }
                
                var alertStart = new Date(new Date(fechaProximoAviso).setDate(new Date(fechaProximoAviso).getDate() - tiempo_aviso));

                if(alertStart <= today && fechaProximoAviso >= today){
                  $('#notesDropdownList').append('<a class="dropdown-item d-flex align-items-center" href="#"><div><div class="small text-gray-500">' + moment(element.fecha_aviso).locale("es").format('LL') + '</div><span>' + element.nota +'</span></div></a>');
                  notesCount += 1; 
                } 
              });
          }catch (e){
              console.log(e);
          }
          

      },
      error: function (request, status, error) {
          console.log(error);
      }
  });
  if(notesCount){
    $('#notesDropdownIcon span').text(notesCount);
    $('#notesDropdownIcon span').removeClass('hidden');
    $('#notesDropdown').removeClass('hidden');
  }else{
    $('#notesDropdownIcon span').addClass('hidden');
    $('#notesDropdown').addClass('hidden');
  }
  //console.log([datosColumnas,datosTabla])
}

function logOutConfirmation(){
  Swal.fire({
    title: '¿Seguro que quieres cerrar sesion?',
    //text: "¡Nos vemos pronto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Cerrar sesion',
    cancelButtonText: 'Cancelar'
  }).then((result) => {
    if (result.value) {
      location.href = 'logeo/logout.php';
    }
  })
}