var savedArrays =[];
var estados = [{id:'Bueno',texto:'Bueno'},
               {id:'Con problemas',texto:'Con problemas'},
               {id:'Cambiar',texto:'Cambiar'},
               {id:'Roto',texto:'Roto'},
               {id:'Vendido',texto:'Vendido'},
               {id:'Perdido',texto:'Perdido'}]

$(document).ready(function() {
    $("#enterForm").on('submit',(function(e) {
        e.preventDefault();
        $.ajax({
            url: "ajax/inventory_table.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                $('#dataEnterModal').modal('hide');  
                createTable();
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'Base de datos actualizada',
                    showConfirmButton: false,
                    timer: 1000
                })
            },
            error: function (request, status, error) {
                console.log("Error: "+error);
            }          
        });
    }));
    $("#dataEnterModal").on('hidden.bs.modal',(function(e) {
        $("#enterForm")[0].reset();
    })); 

    $('#fecha_adquisicion').datetimepicker({format:'DD/MM/YYYY', locale: 'es'});
    $('#fecha_desecho').datetimepicker({format:'DD/MM/YYYY', locale: 'es'});
});

function addInAdditionModal(Titulo){
    $("#tipo").val('new');
    $('#dataEnterModalLabel').text(Titulo);
    rellenarRellenable('ubicacion',obtenerRellenable('ubicacion'),'campoVacio');
    rellenarRellenable('tipoElemento',obtenerRellenable('tipoElemento'),'campoVacio');
    rellenarRellenable('estado',estados,'campoVacio');
}

/*Funcion para crear las tablas*/
function createTable() {

    datosTabla = tableData();
    columnas = datosTabla[0];
    datos = datosTabla[1];
    if ( $.fn.DataTable.isDataTable( '#datatable' ) ) {
        $('#datatable').DataTable().destroy();
        $('#datatable').empty();
    };

    moment.updateLocale(moment.locale(), { invalidDate: "" });

    table = $('#datatable').DataTable({
        responsive: true,
        columnDefs: [
            { targets: 6, render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' ) },
            { targets: 8, render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' ) },
            { responsivePriority: 1, targets: -14 },
            { responsivePriority: 2, targets: -1 },
            { responsivePriority: 3, targets: -10 },
            { responsivePriority: 4, targets: -11 },
            { responsivePriority: 5, targets: -13 },
            { responsivePriority: 6, targets: -3 },
            { responsivePriority: 7, targets: -4 },
            { responsivePriority: 8, targets: -12 },
        ],
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        data: datos,
        columns: columnas,
        "pagingType": ($(window).width() > 768) ? 'simple_numbers' : 'simple',
        "order": [[ 8, "asc" ]],
        "createdRow": function ( row, data, index ) {
            console.log(data)
            if (data['Estado'] == "Bueno" ) {
                $('td', row).eq(13).addClass('good');
            }else if (data['Estado'] == "Con problemas" ) {
                $('td', row).eq(13).addClass('regular');
            }else if (data['Estado'] == "Cambiar" ) {
                $('td', row).eq(13).addClass('bad');
            }else if (data['Estado'] == "Perdido" || data['Estado'] == "Vendido" || data['Estado'] == "Roto"){
                $('td', row).eq(13).addClass('catastrophic');
            }
            if(data['Fecha_desecho']){
                $(row).addClass('discarded');
            }
        }
    });

    return (table);
 };

/************************************************************************************/
/*Funciones para obtencion de los datos de las tablas*/

function tableData(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainTable',
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/inventory_table.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = JSON.parse(response);
                data = data.reverse();
                datosColumnas = [{data: 'Elemento', title: 'Elemento'},{data: 'Ubicacion', title: 'Ubicación'},{data: 'Tipo', title: 'Tipo'},{data: 'Modelo', title: 'Marca/modelo'},{data: 'Identificativo', title: 'Identificativo'},{data: 'Lugar_adquisicion', title: 'Lugar adquisición'},{data: 'Fecha_adquisicion', title: 'Fecha adquisición'},{data: 'Valor_adquisicion', title: 'Valor adquisición'},{data: 'Fecha_desecho', title: 'Fecha desecho'},{data: 'Valor_residual', title: 'Valor residual'},{data: 'Tiempo_posesion', title: 'Tiempo posesión'},{data: 'Estado', title: 'Estado'},{data: 'Observaciones', title: 'Observaciones'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    var valorResidual="";
                    var fechaDesecho="";
                    var tiempoPosesion="";

                    if (element.fecha_desecho){
                        fechaDesecho = element.fecha_desecho;
                        valorResidual = euros(element.valor_residual);
                    };

                    tiempoPosesion = timeBetweenDates(element.fecha_adquisicion,fechaDesecho);
                    datosTabla[index] = {Elemento: element.elemento, Ubicacion: element.ubicacion, Tipo: element.tipoElemento, Modelo: element.modelo, Identificativo: element.identificativo, Lugar_adquisicion: element.lugar_adquisicion, Fecha_adquisicion: element.fecha_adquisicion, Valor_adquisicion: euros_sup(element.valor_adquisicion), Fecha_desecho: fechaDesecho, Valor_residual: valorResidual, Tiempo_posesion: tiempoPosesion, Estado: element.estado, Observaciones: element.observaciones, Edit: ' <a href="#" class="popoverReference edit-icon" title="Copiar elemento" data-content="Copiar datos del elemento" onclick="copiarDatos(this.id);" id='+element.id+'><i class="fa fa-copy"></i></a><a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'};
                });
            }catch (e){
                console.log(e);
            }
            

        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    return [datosColumnas,datosTabla];
}

function editarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    $("#tipo").val('update');
    $("#editTag").val(a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/inventory_table.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);

                rellenarRellenable('ubicacion',obtenerRellenable('ubicacion'),'campoVacio');
                rellenarRellenable('tipoElemento',obtenerRellenable('tipoElemento'),'campoVacio');
                rellenarRellenable('estado',estados,'campoVacio');
                $('#dataEnterModal #ubicacion').selectpicker('val', data[0].id_ubicacion);
                $('#dataEnterModal #tipoElemento').selectpicker('val', data[0].id_tipo_elemento);
                $('#dataEnterModal #elemento').val(data[0].elemento);
                $('#dataEnterModal #lugar_adquisicion').val(data[0].lugar_adquisicion);
                $('#dataEnterModal #modelo').val(data[0].modelo);
                $('#dataEnterModal #identificativo').val(data[0].identificativo);
                $('#dataEnterModal #fecha_ad').val(data[0].fecha_adquisicion.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                $('#dataEnterModal #valor_adquisicion').val(data[0].valor_adquisicion);
                if (data[0].fecha_desecho){
                    $('#dataEnterModal #fecha_des').val(data[0].fecha_desecho.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                };
                if (data[0].valor_residual != 0){
                    $('#dataEnterModal #valor_residual').val(data[0].valor_residual); 
                 };

                $('#dataEnterModal #estado').selectpicker('val', data[0].estado);
                $('#dataEnterModal #cantidad').val(data[0].cantidad);
                $('#dataEnterModal #observaciones').val(data[0].observaciones);

                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });

    $('#dataEnterModalLabel').text("Editar elemento");
   
        //$(this).find('form')[0].reset();
}

function copiarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/inventory_table.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);

                rellenarRellenable('ubicacion',obtenerRellenable('ubicacion'),'campoVacio');
                rellenarRellenable('tipoElemento',obtenerRellenable('tipoElemento'),'campoVacio');
                rellenarRellenable('estado',estados,'campoVacio');
                $('#dataEnterModal #ubicacion').selectpicker('val', data[0].id_ubicacion);
                $('#dataEnterModal #tipoElemento').selectpicker('val', data[0].id_tipo_elemento);
                $('#dataEnterModal #elemento').val(data[0].elemento);
                $('#dataEnterModal #lugar_adquisicion').val(data[0].lugar_adquisicion);
                $('#dataEnterModal #modelo').val(data[0].modelo);
                $('#dataEnterModal #identificativo').val(data[0].identificativo);
                $('#dataEnterModal #fecha_ad').val(data[0].fecha_adquisicion.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                $('#dataEnterModal #valor_adquisicion').val(data[0].valor_adquisicion);
                if (data[0].fecha_desecho){
                    $('#dataEnterModal #fecha_des').val(data[0].fecha_desecho.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                };
                if (data[0].valor_residual != 0){
                    $('#dataEnterModal #valor_residual').val(data[0].valor_residual); 
                 };

                $('#dataEnterModal #estado').selectpicker('val', data[0].estado);
                $('#dataEnterModal #cantidad').val(data[0].cantidad);
                $('#dataEnterModal #observaciones').val(data[0].observaciones);

                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });

    $("#tipo").val('new');
    $("#editTag").val(a);
    $('#dataEnterModalLabel').text("Copiar elemento");F
   
        //$(this).find('form')[0].reset();
}

function borrarItem(a){
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {   
            var parametros = {
                "tipo" : 'delete',
                "table_id" : a
            }; 

            $.ajax({
                data:  parametros,
                url:   'ajax/inventory_table.php',
                type:  'post',
                async: false,
                beforeSend: function () {
                    $("#resultado").html("Procesando, espere por favor...");
                    
                },
                success:  function (response) {
                    try {
                        
                        createTable();
                        console.log("Elemento con id = "+a+" borrado con exito"); 
                        Swal.fire({
                            position: 'top',
                            type: 'success',
                            title: 'Base de datos actualizada',
                            showConfirmButton: false,
                            timer: 1000
                        })
                        
                    } catch (e) {
                        $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                        /*alertas();*/
                        console.log(e);
                    }  
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        } 
    })
}
function obtenerRellenable(grupo){
    var array;
    switch (grupo) {
        case 'ubicacion':
            var parametros = {
                "tipo" : 'obtainPlaces',
            };  
            break;
        case 'tipoElemento':
            var parametros = {
                "tipo" : 'obtainElementTypes',
            };  
            break;
        default:
            break;
    }   
        
    $.ajax({
        data:  parametros,
        url:   'ajax/inventory_table.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            array = jQuery.parseJSON(response);
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    savedArrays['categorias'] = array;
    return array;
}