/* Scripts iniciales incluidos en el Bootstrap */
/*jQuery*/
document.write("<script type='text/javascript' src='vendor/jquery/jquery.min.js'></script>");
document.write("<script type='text/javascript' src='vendor/jquery-easing/jquery.easing.min.js'></script>");
//document.write("<script type='text/javascript' src='vendor/jquery/jquery-ui.min.js'></script"+">");

/*Bootstrap Core JavaScript*/
document.write("<script type='text/javascript' src='vendor/bootstrap/js/bootstrap.bundle.min.js'></script>");
//document.write("<script type='text/javascript' src='vendor/bootstrap3-editable/js/bootstrap-editable.min.js'></script"+">");
document.write("<script type='text/javascript' src='vendor/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js'></script"+">");

/*Metis Menu Plugin JavaScript*/
//document.write("<script type='text/javascript' src='vendor/metisMenu/metisMenu.min.js'></script>");

/* Custom Theme JavaScript */
document.write("<script type='text/javascript' src='js/generals/sb-admin-2.js'></script>");
document.write("<script type='text/javascript' src='js/generals/agp.js'></script>");

/*Datetimepicker JavaScript*/
document.write("<script type='text/javascript' src='vendor/moment-js/moment-with-locales.min.js'></script"+">");
document.write("<script type='text/javascript' src='vendor/bootstrap/js/bootstrap-datetimepicker.js'></script"+">");

/*Funciones generales*/
document.write("<script type='text/javascript' src='js/generals/genericFunctions.js'></script"+">");
document.write("<script type='text/javascript' src='js/generals/commonFunctions.js'></script"+">");

/*DataTables*/
document.write("<script type='text/javascript' src='js/masters/master_datatables.js'></script>");

/*Chart.js*/
//document.write("<script type='text/javascript' src='vendor/chart-js/Chart.bundle.min.js'></script"+">");
document.write("<script type='text/javascript' src='vendor/chart.js/Chart.min.js'></script"+">");

/*Number formats*/
document.write("<script type='text/javascript' src='vendor/jquery-number-master/jquery.number.min.js'></script"+">");

/*Toolbar*/
document.write("<script type='text/javascript' src='js/generals/toolbar.js'></script"+">");

/*Alert*/
document.write("<script type='text/javascript' src='vendor/sweetalert2/sweetalert2.all.min.js'></script"+">");







