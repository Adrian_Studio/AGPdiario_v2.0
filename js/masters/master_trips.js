/**** Main trips JS ****/

document.write("<script type='text/javascript' src='js/trips/trips_map.js'></script"+">");
document.write("<script type='text/javascript' src='js/trips/trips_table.js'></script"+">");

/*jVectorMap*/
document.write("<script type='text/javascript' src='js/trips/jquery-jvectormap-2.0.4.min.js'></script"+">");

/*Mapas del mundo*/
document.write("<script type='text/javascript' src='js/trips/maps/jVectorMap-World-Continents.js'></script"+">");
document.write("<script type='text/javascript' src='js/trips/maps/jVectorMap-World-Countries.js'></script"+">");

/*Mapas de continente*/
document.write("<script type='text/javascript' src='js/trips/maps/jVectorMap-Africa-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='js/trips/maps/jVectorMap-Asia-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='js/trips/maps/jVectorMap-Europe-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='js/trips/maps/jVectorMap-NorthAmerica-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='js/trips/maps/jVectorMap-Oceania-Countries.js'></script"+">");
document.write("<script type='text/javascript' src='js/trips/maps/jVectorMap-SouthAmerica-Countries.js'></script"+">");
/*Mapas de pais*/
document.write("<script type='text/javascript' src='js/trips/maps/jVectorMap-Spain-Provinces.js'></script"+">");