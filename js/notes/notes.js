var savedArrays =[];
var arrayClasificacion =[{id: '1 - Importante', texto: '1 - Importante'},
                        {id: '5 - Avisos', texto: '5 - Avisos'},
                        {id: '9 - Recordatorio', texto: '9 - Recordatorio'}];

$(document).ready(function() {

    $("#enterForm").on('submit',(function(e) {
        e.preventDefault();
        $.ajax({
            url: "ajax/notes.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                $('#dataEnterModal').modal('hide');  
                createTable();
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'Base de datos actualizada',
                    showConfirmButton: false,
                    timer: 1000
                })
                lookForNotesWithAlert();
            },
            error: function (request, status, error) {
                console.log("Error: "+error);
            }          
        });
    }));
    $("#dataEnterModal").on('hidden.bs.modal',(function(e) {
        $("#enterForm")[0].reset();
        $('#aviso_activo').val(0);
        $('#periodicidad_activa').val(0);
        $('#periodicidad_desactivada').prop('hidden', false);
        $('#periodicidad_activada').prop('hidden', true);
        $('#aviso_desactivado').prop('hidden', false);
        $('#aviso_activado').prop('hidden', true);
    })); 

    $('#fechaNote').datetimepicker({format:'DD/MM/YYYY', locale: 'es'});
});

function addInAdditionModal(Titulo){
    $("#tipo").val('new');
    $('#dataEnterModalLabel').text(Titulo);
    rellenarRellenable('clasificacion',arrayClasificacion,'campoVacio');
}

/*Funcion para crear las tablas*/
function createTable() {

    datosTabla = tableData();
    columnas = datosTabla[0];
    datos = datosTabla[1];
    
    if ( $.fn.DataTable.isDataTable( '#datatable' ) ) {
        $('#datatable').DataTable().destroy();
        $('#datatable').empty();
    };
    moment.updateLocale(moment.locale(), { invalidDate: "" });
    table = $('#datatable').DataTable({
        responsive: true,
        columnDefs: [
            { render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' ), targets: 0 },
            { type: 'formated-sorting', targets: [2,4] },
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 5 },
        ],
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        data: datos,
        columns: columnas,
        order: [[ 6, "asc" ],[ 0, "asc" ]],
        "pagingType": ($(window).width() > 768) ? 'simple_numbers' : 'simple',
    });

    return (table);
 };

/************************************************************************************/
/*Funciones para obtencion de los datos de las tablas*/

function tableData(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainTable',
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/notes.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = JSON.parse(response);
                //console.log(data);
                datosColumnas = [{data: 'Fecha', title: 'Fecha aviso'}, {data: 'Nota', title: 'Nota'}, {data: 'Tiempo_aviso', title: 'Tiempo aviso'}, {data: 'ConAviso', title: 'Aviso activo'}, {data: 'Periodicidad', title: 'Periodicidad'}, {data: 'ConPeriodicidad', title: 'Peridicidad activa'}, {data: 'Clasificacion', title: 'Clasificación'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    if (element.periodicidad_meses){
                        var periodicidad = element.periodicidad_meses + ' meses';
                    }else{
                        var periodicidad = '';
                    }
                    if (element.tiempo_aviso_dias){
                        var tiempo_aviso = element.tiempo_aviso_dias + ' días';
                    }else{
                        var tiempo_aviso = '';
                    }
                    datosTabla[index] = {Fecha: element.fecha_aviso, Nota: element.nota, Tiempo_aviso: tiempo_aviso, ConAviso: (element.aviso_activo) ? 'Sí' : 'No', Periodicidad: periodicidad, ConPeriodicidad: (element.periodicidad_activa) ? 'Sí' : 'No', Clasificacion: element.clasificacion, Edit: ' <a href="#" class="popoverReference edit-icon" title="Copiar elemento" data-content="Copiar datos del elemento" onclick="copiarDatos(this.id);" id='+element.id+'><i class="fa fa-copy"></i></a><a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'}
                });
            }catch (e){
                console.log(e);
            }
            

        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    //console.log([datosColumnas,datosTabla])
    return [datosColumnas,datosTabla];
}

function editarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    $("#tipo").val('update');
    $("#editTag").val(a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/notes.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);
                //console.log(data);
                rellenarRellenable('clasificacion',arrayClasificacion,'campoVacio');
                $('#dataEnterModal #fecha').val(data[0].fecha_aviso.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                $('#dataEnterModal #clasificacion').selectpicker('val', data[0].clasificacion);
                if(data[0].tiempo_aviso_dias){
                    $('#dataEnterModal #tiempo_aviso').val(data[0].tiempo_aviso_dias);
                };
                $('#dataEnterModal #aviso_activo').val(data[0].aviso_activo);
                if(data[0].aviso_activo){
                    $('#aviso_desactivado').prop('hidden', true);
                    $('#aviso_activado').prop('hidden', false);
                }
                if(data[0].periodicidad_meses){
                    $('#dataEnterModal #periodicidad_meses').val(data[0].periodicidad_meses);
                };
                $('#dataEnterModal #periodicidad_activa').selectpicker('val', data[0].periodicidad_activa);
                if(data[0].periodicidad_activa){
                    $('#periodicidad_desactivada').prop('hidden', true);
                    $('#periodicidad_activada').prop('hidden', false);
                }
                $('#dataEnterModal #nota').val(data[0].nota);


                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });

    $('#dataEnterModalLabel').text("Editar elemento");
   
        //$(this).find('form')[0].reset();
}

function copiarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/notes.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);
                //console.log(data);
                rellenarRellenable('clasificacion',arrayClasificacion,'campoVacio');
                $('#dataEnterModal #fecha').val(data[0].fecha_aviso.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                $('#dataEnterModal #clasificacion').selectpicker('val', data[0].clasificacion);
                if(data[0].tiempo_aviso_dias){
                    $('#dataEnterModal #tiempo_aviso').val(data[0].tiempo_aviso_dias);
                };
                $('#dataEnterModal #aviso_activo').val(data[0].aviso_activo);
                if(data[0].aviso_activo){
                    $('#aviso_desactivado').prop('hidden', true);
                    $('#aviso_activado').prop('hidden', false);
                }
                if(data[0].periodicidad_meses){
                    $('#dataEnterModal #periodicidad_meses').val(data[0].periodicidad_meses);
                };
                $('#dataEnterModal #periodicidad_activa').selectpicker('val', data[0].periodicidad_activa);
                if(data[0].periodicidad_activa){
                    $('#periodicidad_desactivada').prop('hidden', true);
                    $('#periodicidad_activada').prop('hidden', false);
                }
                $('#dataEnterModal #nota').val(data[0].nota);


                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });

    $("#tipo").val('new');
    $("#editTag").val(a);
    $('#dataEnterModalLabel').text("Copiar elemento");
   
        //$(this).find('form')[0].reset();
}

function borrarItem(a){
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {
            var parametros = {
                "tipo" : 'delete',
                "table_id" : a
            }; 
    
            $.ajax({
                data:  parametros,
                url:   'ajax/notes.php',
                type:  'post',
                async: false,
                beforeSend: function () {
                    $("#resultado").html("Procesando, espere por favor...");
                    
                },
                success:  function (response) {
                    try {
                        
                        createTable();
                        console.log("Elemento con id = "+a+" borrado con exito"); 
                        Swal.fire({
                            position: 'top',
                            type: 'success',
                            title: 'Base de datos actualizada',
                            showConfirmButton: false,
                            timer: 1000
                        })
                        lookForNotesWithAlert();

                    } catch (e) {
                        $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                        /*alertas();*/
                        console.log(e);
                    }  
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        }
    })
}
