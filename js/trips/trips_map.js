var etiquetas = new Array();
var datosGraf = new Array();
var continentes = new Array();
var paises = new Array();
var marcadores = new Array();
var lugares = new Array();
var gdpData = new Array();
var mapMarkersScale= new Array();
var mapScale;
var mapaGuardado;
var initialMapScale;
var mapaCargado = "world_mill";


$(document).ready(function() {
    
    recogeLugares();
    cargaMapa(mapaCargado, gdpData);

});

/************************************************FUNCIONES GENERALES************************************************/

//Obtiene los lugares del mapa para cargarlo
function recogeLugares(){
    
    var parametros = {
        "tipo" : 'obtainPlaces'
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/trips.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (data) {
            var i = 0;
            var lat = "";
            var lon = "";

            $.each(JSON.parse(data), function(index, element) {
                gdpData[element.codigo_pais] = element.categoria;
                paises[index]= element.codigo_pais;
                lugares[index]= element.lugar;
                lat = element.latitud;
                lon = element.longitud ;
                if (lat != "" && lon != "") {
                marcadores[i] = {latLng: [lat, lon], name: element.lugar, status: element.categoria}
                mapMarkersScale[i] = [1];
                i++;
                }
            });
       },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    //$(this).find('form')[0].reset();
   
}


//Obtiene la constante seleccionada para editarla
function cargaMapa(map, gdpData){
    //console.log(gdpData);
    mapaCargado = map;

    $('#world-map').vectorMap({
        map: map,
        markers: marcadores,
        backgroundColor: '#589bd7',
        regionStyle: {
            initial: {
                fill            : '#e7dcd8',
                'fill-opacity'  : 1,
                stroke          : 'none',
                'stroke-width'  : 0,
                'stroke-opacity': 1
            }
        },
        series: {
           /*markers: [{
                attribute: 'image',
                scale: {
                    'Viviendo': './img/house_30x30.png',
                    'Programado': './img/programed_30x30.png',
                    'Visitado': './img/visited_30x30.png',
                    'Trabajo': './img/visited_30x30.png'
                  },
                values: marcadores.reduce(function(p, c, i){ p[i] = c.status; return p }, {}),
            }],*/ //Los iconos no se ven bien
            markers: [{
                attribute: 'fill',
                scale: { "Viviendo" : "#226E01",
                        "Programado" : "#85E3D6",   
                        "Visitado" : "#329E56",   
                        "Trabajo" : "#FFC202" }, 
                values: marcadores.reduce(function(p, c, i){ p[i] = c.status; return p }, {}),
            }],
            regions: [{
                values: gdpData,
                scale: { "Viviendo" : "#226E01",
                        "Programado" : "#85E3D6",   
                        "Visitado" : "#329E56",   
                        "Trabajo" : "#FFC202" }, 
                normalizeFunction: 'polynomial'
            }],
        },
        markerStyle: {
            initial: {
                fill: '#F8E23B',
                "stroke-width": 1,
                stroke: 'black',
                r: 4
            },
            hover: {
                fill: 'white',
                "stroke-width": 2,
                cursor: 'pointer'
              },
        },
        onRegionClick: function (event, code, region) {

        },
        onRegionOver: function (e, code, region) {
            document.body.style.cursor = "pointer";
        },
        onRegionOut: function (e, code, region) {
            document.body.style.cursor = "default";
        },
        onViewportChange: function (e, scale) {
            //e.preventDefault();
        },
        onMarkerTipShow: function (e, tip, code) {
            //e.preventDefault();
        },
        onMarkerOver: function (e, code) {
            //e.preventDefault();
        },
        onMarkerOut: function (e, code) {
            e.preventDefault();
        },
        onMarkerClick: function (e, code) {
            e.preventDefault();
        },
        onMarkerSelected: function (e, code) {
            e.preventDefault();
        },  
        onLabelShow: function(e, label, code) {
            e.preventDefault();
        }
        
    });
    $('.mapSelected').removeClass('mapSelected');
    $('#'+map).addClass('mapSelected');
    initialMapScale = $('#world-map .jvectormap-container').attr('transform');//.substring(6, 24);
}

function changeoMap(mapa){
    var altura = $("#trips-car-body").height();
    $("#trips-car-body").css('height', altura + 'px')
    $(".jvectormap-tip").remove();
    $('#world-map').empty();
    cargaMapa(mapa, gdpData);
    $("#trips-car-body").css('height', 'unset')
}