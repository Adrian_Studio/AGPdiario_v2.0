var savedArrays =[];
var arrayCategorias =[{id: 'Visitado', texto: 'Visitado'},
                      {id: 'Programado', texto: 'Programado'},
                      {id: 'Viviendo', texto: 'Viviendo'},
                      {id: 'Trabajando', texto: 'Trabajando'}];

$(document).ready(function() {

    $("#enterForm").on('submit',(function(e) {
        e.preventDefault();
        $.ajax({
            url: "ajax/trips.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data){
                $('#dataEnterModal').modal('hide');  
                createTable();
                $('#world-map').empty();
                recogeLugares();
                cargaMapa(mapaCargado, gdpData);
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'Base de datos actualizada',
                    showConfirmButton: false,
                    timer: 1000
                })
                lookForProgramedTrips()
            },
            error: function (request, status, error) {
                console.log("Error: "+error);
            }          
        });
    }));
    $("#dataEnterModal").on('hidden.bs.modal',(function(e) {
        $("#enterForm")[0].reset();
    })); 

    $('#fechaTrip').datetimepicker({format:'DD/MM/YYYY', locale: 'es'});
});

function addInAdditionModal(Titulo){
    $("#tipo").val('new');
    $('#dataEnterModalLabel').text(Titulo);
    rellenarRellenable('pais',obtenerRellenable('paises'),'campoVacio');
    rellenarRellenable('categoria',arrayCategorias,'campoVacio');
}

/*Funcion para crear las tablas*/
function createTable() {

    datosTabla = tableData();
    columnas = datosTabla[0];
    datos = datosTabla[1];
    
    if ( $.fn.DataTable.isDataTable( '#datatable' ) ) {
        $('#datatable').DataTable().destroy();
        $('#datatable').empty();
    };

    table = $('#datatable').DataTable({
        responsive: true,
        columnDefs: [
            { render: $.fn.dataTable.render.moment( 'DD/MM/YYYY' ), targets: 0 },
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 6 },
            { responsivePriority: 3, targets: 2 },
        ],
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        data: datos,
        columns: columnas,
        order: [[ 0, "desc" ]],
        "pagingType": ($(window).width() > 768) ? 'simple_numbers' : 'simple',
    });

    return (table);
 };

/************************************************************************************/
/*Funciones para obtencion de los datos de las tablas*/

function tableData(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainTable',
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/trips.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = JSON.parse(response);
                //console.log(data);
                datosColumnas = [{data: 'Fecha', title: 'Fecha'}, {data: 'Pais', title: 'País'}, {data: 'Latitud', title: 'Latitud'}, {data: 'Longitud', title: 'Longitud'}, {data: 'Lugar', title: 'Lugar'}, {data: 'Categoria', title: 'Categoría'}, {data: 'Acompañantes', title: 'Acompañantes'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    datosTabla[index] = {Fecha: element.fecha, Pais: element.pais, Latitud: element.latitud, Longitud: element.longitud, Lugar: element.lugar, Categoria: element.categoria, Acompañantes: element.acompañantes, Edit: ' <a href="#" class="popoverReference edit-icon" title="Copiar elemento" data-content="Copiar datos del elemento" onclick="copiarDatos(this.id);" id='+element.id+'><i class="fa fa-copy"></i></a><a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a><a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>'}
                });
            }catch (e){
                console.log(e);
            }
            

        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    //console.log([datosColumnas,datosTabla])
    return [datosColumnas,datosTabla];
}

function editarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    $("#tipo").val('update');
    $("#editTag").val(a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/trips.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);
                console.log(data);
                rellenarRellenable('pais',obtenerRellenable('paises'),'campoVacio');
                rellenarRellenable('categoria',arrayCategorias,'campoVacio');
                $('#dataEnterModal #fecha').val(data[0].fecha.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                $('#dataEnterModal #pais').selectpicker('val', data[0].codigo_pais);
                $('#dataEnterModal #latitud').val(data[0].latitud);
                $('#dataEnterModal #longitud').val(data[0].longitud);
                $('#dataEnterModal #lugar').val(data[0].lugar);
                $('#dataEnterModal #categoria').selectpicker('val', data[0].categoria);
                $('#dataEnterModal #acompañantes').val(data[0].acompañantes);


                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });

    $('#dataEnterModalLabel').text("Editar elemento");
   
        //$(this).find('form')[0].reset();
}

function copiarDatos(a){
    var tipoContabilidad;
    console.log("El id del elemento es: "+a);
    var parametros = {
        "tipo" : 'obtainOne',
        "table_id" : a
    }; 

    $.ajax({
        data:  parametros,
        url:   'ajax/trips.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = jQuery.parseJSON(response);
                console.log(data);
                rellenarRellenable('pais',obtenerRellenable('paises'),'campoVacio');
                rellenarRellenable('categoria',arrayCategorias,'campoVacio');
                $('#dataEnterModal #fecha').val(data[0].fecha.replace( /(\d{4})-(\d{2})-(\d{2})/, "$3/$2/$1"));
                $('#dataEnterModal #pais').selectpicker('val', data[0].codigo_pais);
                $('#dataEnterModal #latitud').val(data[0].latitud);
                $('#dataEnterModal #longitud').val(data[0].longitud);
                $('#dataEnterModal #lugar').val(data[0].lugar);
                $('#dataEnterModal #categoria').selectpicker('val', data[0].categoria);
                $('#dataEnterModal #acompañantes').val(data[0].acompañantes);


                $('#dataEnterModal').modal('show');  
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                //alertas();
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    $("#tipo").val('new');
    $("#editTag").val(a);
    $('#dataEnterModalLabel').text("Copiar elemento");
   
        //$(this).find('form')[0].reset();
}

function borrarItem(a){
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) {  
            var parametros = {
                "tipo" : 'delete',
                "table_id" : a
            }; 

            $.ajax({
                data:  parametros,
                url:   'ajax/trips.php',
                type:  'post',
                async: false,
                beforeSend: function () {
                    $("#resultado").html("Procesando, espere por favor...");
                    
                },
                success:  function (response) {
                    try {
                        
                        createTable();
                        console.log("Elemento con id = "+a+" borrado con exito"); 
                        Swal.fire({
                            position: 'top',
                            type: 'success',
                            title: 'Base de datos actualizada',
                            showConfirmButton: false,
                            timer: 1000
                        })
                        lookForProgramedTrips()
                        
                    } catch (e) {
                        $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                        /*alertas();*/
                        console.log(e);
                    }  
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        } 
    })
}

function obtenerRellenable(tipo){
    switch(tipo){
        case 'paises': 
            var parametros = {
                "tipo" : 'obtainCountries',
            };     
        break;
    }
    $.ajax({
        data:  parametros,
        url:   'ajax/trips.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            array = jQuery.parseJSON(response);
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    
    savedArrays[tipo] = array;
    
    return array;
}