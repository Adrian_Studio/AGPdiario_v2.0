var savedArrays =[];

$(document).ready(function() {
    
    $("#enterForm").on('submit',(function(e) {
        e.preventDefault();
        if($('#password').val() == $('#password2').val()){
            e.preventDefault();
            $.ajax({
                url: "ajax/users.php",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(data){
                    $('#dataEnterModal').modal('hide');  
                    createTable();
                    Swal.fire({
                        position: 'top',
                        type: 'success',
                        title: 'Base de datos actualizada',
                        showConfirmButton: false,
                        timer: 1000
                    })
                },
                error: function (request, status, error) {
                    console.log("Error: "+error);
                }          
            });
        }else{
            alert('Las contraseñas no coinciden')
        }
    }));
    $("#dataEnterModal").on('hidden.bs.modal',(function(e) {
        $("#enterForm")[0].reset();
        $('.file-tab img').remove();
        $('.file-tab btn btn-default btn-primary span').text('Cargar imagen');
    })); 

});

function addInAdditionModal(Titulo){
    $("#tipo").val('new');
    $('#dataEnterModalLabel').text(Titulo);
    $('#password').prop('required',true);
    $('#password2').prop('required',true);
}

/*Funcion para crear las tablas*/
function createTable() {

    datosTabla = tableData();
    columnas = datosTabla[0];
    datos = datosTabla[1];
    
    if ( $.fn.DataTable.isDataTable( '#datatable' ) ) {
        $('#datatable').DataTable().destroy();
        $('#datatable').empty();
    };

    table = $('#datatable').DataTable({
        responsive: true,
        columnDefs: [
            { render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD/MM/YYYY HH:mm:ss'  ), targets: 0 },
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 4 },
            { responsivePriority: 3, targets: 1 },
        ],
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        data: datos,
        columns: columnas,
        "pagingType": ($(window).width() > 768) ? 'simple_numbers' : 'simple',
    });

    return (table);
 };

/************************************************************************************/
/*Funciones para obtencion de los datos de las tablas*/

function tableData(){
    var datosColumnas, datosTabla = new Array();
    var parametros = {
        "tipo" : 'obtainTable',
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/users.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            try {
                var data = JSON.parse(response);
                console.log(data)
                var opcionBorrar = false;
                datosColumnas = [{data: 'Fecha', title: 'Fecha de creacioón'}, {data: 'Usuario', title: 'Usuario'}, {data: 'Nombre', title: 'Nombre'}, {data: 'Apellido', title: 'Apellido'}, {data: 'Privilegios', title: 'Privilegios'}, {data: 'Edit', title: 'Editar'}]
                $.each(data, function(index, element) {
                    if (element.opcionBorrar){
                        var botonBorrar = '<a href="#" class="popoverReference edit-icon" title="Borrar elemento" data-content="Borrar datos de elemento" onclick="borrarItem(this.id);" id='+element.id+' ><i class="fa fa-times"></i></a>';
                    }else{
                        var botonBorrar = '';
                    }
                    datosTabla[index] = {Fecha: element.fecha, Usuario: element.usuario, Nombre: element.nombre, Apellido: element.apellido, Privilegios: element.privilegios, Edit: ' <a href="#" class="popoverReference edit-icon" title="Modificar elemento" data-content="Modificar datos del elemento" onclick="editarDatos(this.id);" id='+element.id+' ><i class="fa fa-edit"></i></a>' + botonBorrar}
                });
            
            }catch (e){
                console.log(e);
            }
            

        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    //console.log([datosColumnas,datosTabla])
    return [datosColumnas,datosTabla];
}

function editarDatos(a){

    $('#password').prop('required',false);
    $('#password2').prop('required',false);

    var parametros = {
        "tipo" : 'obtainOne',
        "datos" : a
    };
    $.ajax({
        data:  parametros,
        url:   'ajax/users.php',
        type:  'post',
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) { 
            try {
                var data = jQuery.parseJSON(response);

                $('#tipo').val(data.tipo);
                $('#modalTitle').text(data.titulo);
                $('#editTag').val(a);

                $('#nombre').val(data.nombre);
                $('#nombreReal').val(data.nombreReal);
                $('#apellido').val(data.apellido);
                $('#privilegios').val(data.privilegios);

                $('.file-tab').prepend("<img src='"+data.Foto.substring(4,data.Foto.length-1)+"' alt='Image preview' class='thumbnail' style='max-width: 250px; max-height: 250px'>");

            $('#dataEnterModal').modal('show');
            } catch (e) {
                $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                /*alertas();*/
                console.log(e);
            }  
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });

    $('#dataEnterModalLabel').text("Editar elemento");
   
        //$(this).find('form')[0].reset();
}

function borrarItem(a){
    Swal.fire({
        title: '¿Seguro de que quieres borrar el elemento?',
        type: 'warning',
        confirmButtonColor: '#d33',
        showCancelButton: true,
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Sí, borrar',
        cancelButtonText: 'No, cancelar'
    }).then((result) => {
        if (result.value) { 
            var parametros = {
                "tipo" : 'delete',
                "table_id" : a
            }; 

            $.ajax({
                data:  parametros,
                url:   'ajax/users.php',
                type:  'post',
                async: false,
                beforeSend: function () {
                    $("#resultado").html("Procesando, espere por favor...");
                    
                },
                success:  function (response) {
                    try {
                        
                        createTable();
                        console.log("Elemento con id = "+a+" borrado con exito"); 
                        Swal.fire({
                            position: 'top',
                            type: 'success',
                            title: 'Base de datos actualizada',
                            showConfirmButton: false,
                            timer: 1000
                        })
                        
                    } catch (e) {
                        $('body').prepend('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Atención!</strong> No tienes permiso para acceder a esta opción</div>');
                        /*alertas();*/
                        console.log(e);
                    }  
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        } 
    })
}

function obtenerRellenable(tipo,filtro){
    switch(tipo){
        case 'categorias': 
            var parametros = {
                "tipo" : 'obtainCategories',
            };     
        break;
        case 'conceptos': 
            var parametros = {
                "tipo" : 'obtainConcept',
                "id_categoria": filtro
            }; 
        break;
    }
    $.ajax({
        data:  parametros,
        url:   'ajax/bookkepping_general.php',
        type:  'post',
        async: false,
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            
        },
        success:  function (response) {
            array = jQuery.parseJSON(response);
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
    
    savedArrays[tipo] = array;
    
    return array;
}