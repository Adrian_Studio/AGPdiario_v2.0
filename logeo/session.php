<?php 
    include('../connection/connection.php');
    $db = new Conexiones();
    $db = Conexiones::createConnection();

    

    if (!empty($_POST['username']) && !empty($_POST['password'])){
        session_start();
        $username = $_POST['username'];
        $password = md5($_POST['password']);
        
        $stmt = $db->prepare("SELECT * FROM usuarios WHERE Usuario =?  AND Pass COLLATE utf8_bin =? ");
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        $count = mysqli_num_rows($result);

        if($count == 1){
            $_SESSION['username'] = $username;

            $datosUsuario = array();
            while($usuario = $result->fetch_assoc()) {
                $datosUsuario[] = $usuario;
            }

            $_SESSION['rol'] = $datosUsuario[0]['Privilegios'];
            $_SESSION['id'] = $datosUsuario[0]['id'];
            $_SESSION['nombre'] = $datosUsuario[0]['Nombre'];
            $_SESSION['apellido'] = $datosUsuario[0]['Apellido1'];
            $_SESSION['foto'] = "data:image/jpeg;base64,".base64_encode($datosUsuario[0]['Foto']);

            header("Location: ../notes.php");
        }else{
            header("Location: ../login.php?logeo=datos");
        }
    }else{
        header("Location: ../login.php?logeo=datos");
    }

?>