<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
<div class="modal fade" id="dataEnterModal" tabindex="-1" role="dialog" aria-labelledby="dataEnterModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content card-shadow">
        <div class="modal-header card-header">
          <h5 class="modal-title m-0 font-weight-bold text-primary" id="dataEnterModalLabel">Variable</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="enterForm" method="POST">
                <div class="panel-body">
                    <input type="hidden" id="tipo" name="tipo" value="new">
                    <input type="hidden" id="editTag" name="editTag" value="">
                    <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $_SESSION['id']; ?>">  
                    <!-- bootstrap-imageupload. -->
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="categoria">
                                Categoría
                            </label>
                            <select id="categoria" name="categoria" class="form-control selectpicker" data-live-search="true" data-size="5">
                                <option value="" ></option>
                            </select>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="concepto">
                                Concepto
                            </label>
                            <select id="concepto" name="concepto" class="form-control selectpicker" data-live-search="true" data-size="5" required>
                                <option value="" ></option>
                            </select>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="fechaPeriodicity">
                                Fecha
                            </label>
                            <div id="fechaPeriodicity" class="form-group input-group date">
                                <input id="fecha" name="fecha" type="text" class="form-control" autocomplete="off" value="" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="periodicidad">
                                Periodicidad (Meses)
                            </label>
                            <div class="form-group input-group date">
                                <input type="number" step="1" id="periodicidad" name="periodicidad" class="form-control" value="" autocomplete="off" required>  
                            </div>
                        </div>
                    </div>                           
                </div>

                <div class="modal-footer" style="margin-top: 10px;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-primary" type="submit"  id="guardar">Guardar cambios</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>