<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
<div class="modal fade" id="dataEnterModal" tabindex="-1" role="dialog" aria-labelledby="dataEnterModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content card-shadow">
        <div class="modal-header card-header">
          <h5 class="modal-title m-0 font-weight-bold text-primary" id="dataEnterModalLabel">Variable</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="enterForm" method="POST">
                <div class="panel-body">
                    <input type="hidden" id="tipo" name="tipo" value="new">
                    <input type="hidden" id="editTag" name="editTag" value="">
                    <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $_SESSION['id']; ?>">  

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="categoria">
                                Categoría
                            </label>
                            <input id="categoria" name="categoria" type="text" class="form-control" autocomplete="off" value="" required>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="check-paraVivienda" onclick="$('#check-paraVehiculo')[0].checked = false;$('#paraVehiculo').val(false);$('#paraVivienda').val((this.checked)?1:0);">
                                <label class="form-check-label" for="paraVivienda">Categoría de vivienda</label>
                                <input type="hidden" id="paraVivienda" name="paraVivienda" value="0">
                            </div>      
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="check-paraVehiculo" onclick="$('#check-paraVivienda')[0].checked = false;$('#paraVivienda').val(false);$('#paraVehiculo').val((this.checked)?1:0);">
                                <label class="form-check-label" for="paraVehiculo">Categoría de vehículo</label>
                                <input type="hidden" id="paraVehiculo" name="paraVehiculo" value="0">
                            </div>  
                        </div>
                    </div>   
                    <div class="row edit-warning" style="margin-top: 10px; display:none;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h6>!! No se puede editar el grupo en que se encuentra (General/Vehículo/Vivienda)</h6>    
                        </div>        
                    </div>                  
                </div>

                <div class="modal-footer" style="margin-top: 10px;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-primary" type="submit"  id="guardar">Guardar cambios</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>