<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
<div class="modal fade" id="dataFilterModal" tabindex="-1" role="dialog" aria-labelledby="dataEnterModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header card-header">
          <span class="modal-title m-0 font-weight-bold text-primary specialButton" id="filterSwicher" onclick="switchFilter()"><i class="fa fa-random"></i></span>
          <h5 class="modal-title m-0 font-weight-bold text-primary" id="dataFilterModalLabel">Filtrar elementos por mes y año </h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="filterForm" method="POST">
                <div class="panel-body">
                    <input type="hidden" id="filt_tipo" name="tipo" value="new">
                    <input type="hidden" id="filt_editTag" name="editTag" value="">
                    <input type="hidden" id="filt_id_usuario" name="id_usuario" value="<?php echo $_SESSION['id']; ?>">  
                    <!-- bootstrap-imageupload. -->
                    <div class="row grupoDatos" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="categoria">
                                Categoría
                            </label>
                            <select id="filt_categoria" name="categoria" class="form-control selectpicker" data-live-search="true" data-selected-text-format="count" data-size="5" data-actions-box="true" multiple>
                                <option value="" ></option>
                            </select>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="concepto">
                                Concepto
                            </label>
                            <select id="filt_concepto" name="concepto" class="form-control selectpicker" data-live-search="true" data-selected-text-format="count" data-size="5" data-actions-box="true" multiple>
                                <option value="" ></option>
                            </select>
                        </div>
                    </div>
                    <div class="row periodoDatos switchFilter" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="año">
                                Año
                            </label>
                            <select id="filt_año" name="año" class="form-control selectpicker" data-live-search="true" data-selected-text-format="count" data-size="5" data-actions-box="true">
                            </select>    
                        </div> 
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="mes">
                                Mes
                            </label>
                            <select id="filt_mes" name="mes" class="form-control selectpicker" data-live-search="true" data-selected-text-format="count" data-size="5" data-actions-box="true">
                            </select>
                        </div>
                    </div>  
                    <div class="row switchFilter periodoDatos hidden" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="filt_fechaInicial">Fecha inicial</label>
                            <div id="filt_fechaInicial" class="form-group input-group date">
                                <input id="filt_fechaIni" name="fechaIni" type="text" class="form-control" autocomplete="off">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="filt_fechaFinal">Fecha final</label>
                            <div id="filt_fechaFinal" class="form-group input-group date">
                                <input id="filt_fechaFin" name="fechaFin" type="text" class="form-control" autocomplete="off">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row vivienda" style="margin-top: 10px; display: none;" >
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="vivienda">
                                Vivienda
                            </label>
                            <select id="filt_vivienda" name="vivienda" class="form-control selectpicker" data-live-search="true" data-selected-text-format="count" data-size="5" data-actions-box="true" multiple>
                                <option value="" ></option>
                            </select>   
                        </div>
                    </div>

                    <div class="row vehiculo" style="margin-top: 10px; display: none;" >
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="vehiculo">
                                Vehículo
                            </label>
                            <select id="filt_vehiculo" name="vehiculo" class="form-control selectpicker" data-live-search="true" data-selected-text-format="count" data-size="5" data-actions-box="true" multiple>
                                <option value="" ></option>
                            </select>   
                        </div>
                    </div>                      
                </div>

                <div class="modal-footer" style="margin-top: 10px;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="cancelFilter()">Cancelar</button>
                    <button class="btn btn-primary" type="submit"  id="filtrar">Filtrar</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>