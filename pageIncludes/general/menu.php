
<?php 

//include('./logeo/session.php');
session_start();
//Reenviamos al logeo en caso de que no exista sesion
if(empty($_SESSION['username'])){
    header('Location: login.php?logeo=error');  
}

//Reenviamos al inicio en caso de que no sea Administrador
/*$lugar = $_SERVER['REQUEST_URI'];

if( (stristr($lugar, 'usuarios.php') == TRUE || stristr($lugar, 'logs.php') == TRUE) && $_SESSION['rol'] != 'Administrador'){
    header('location: ./index.php'); //No se que hace
}*/

?>

<!-- Sidebar -->
<ul class="navbar-nav bg-sidebar-agp sidebar sidebar-dark accordion menu" id="accordionSidebar">
<div class = "menu-header">
  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="notes.php">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">AGPdiario <sup>2.0</sup></div>
  </a>
</div>



<!-- Nav Item -->
<!--li class="nav-item">
  <a class="nav-link" id="overview.php" onclick="redirectPage(this.id);">
    <i class="fas fa-fw fa-chalkboard"></i>
    <span>Principal</span></a>
</li-->

<!-- Divider -->
<hr class="sidebar-divider"-->

<!-- Heading -->
<div class="sidebar-heading">
  Contabilidad
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseGeneralBookkepping" aria-expanded="true" aria-controls="collapseGeneralBookkepping">
    <i class="fas fa-fw fa-book"></i>
    <span>General</span>
  </a>
  <div id="collapseGeneralBookkepping" class="collapse" aria-labelledby="headingGeneralBookkepping" data-parent="#accordionSidebar">
    <div class="bg-sidebar-group-agp py-2 collapse-inner">
      <a class="collapse-item" id="generalBookkeppingGraphs.php" onclick="redirectPage(this.id);">Graficos</a>
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Tablas:</h6>
      <a class="collapse-item" id="generalBookkeppingIncomesTable.php" onclick="redirectPage(this.id);">Ingresos</a>
      <a class="collapse-item" id="generalBookkeppingExpensesTable.php" onclick="redirectPage(this.id);">Gastos</a>
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Resumenes:</h6>
      <a class="collapse-item" id="generalBookkeppingTotalSummary.php" onclick="redirectPage(this.id);">Total</a>
      <a class="collapse-item" id="generalBookkeppingAnnualSummary.php" onclick="redirectPage(this.id);">Anual</a>
    </div>
  </div>
</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item collapseHouseBookkepping hidden">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseHouseBookkepping" aria-expanded="true" aria-controls="collapseHouseBookkepping">
    <i class="fas fa-fw fa-home"></i>
    <span>Vivienda</span>
  </a>
  <div id="collapseHouseBookkepping" class="collapse" aria-labelledby="headingHouseBookkepping" data-parent="#accordionSidebar">
    <div class="bg-sidebar-group-agp py-2 collapse-inner">
      <a class="collapse-item" id="houseBookkeppingGraphs.php" onclick="redirectPage(this.id);">Graficos</a>
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Tablas:</h6>
      <a class="collapse-item" id="houseBookkeppingIncomesTable.php" onclick="redirectPage(this.id);">Ingresos</a>
      <a class="collapse-item" id="houseBookkeppingExpensesTable.php" onclick="redirectPage(this.id);">Gastos</a>
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Resumenes:</h6>
      <a class="collapse-item" id="houseBookkeppingTotalSummary.php" onclick="redirectPage(this.id);">Total</a>
      <a class="collapse-item" id="houseBookkeppingAnnualSummary.php" onclick="redirectPage(this.id);">Anual</a>
    </div>
  </div>
</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item collapseVehicleBookkepping hidden">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseVehicleBookkepping" aria-expanded="true" aria-controls="collapseVehicleBookkepping">
    <i class="fas fa-fw fa-car"></i>
    <span>Vehículo</span>
  </a>
  <div id="collapseVehicleBookkepping" class="collapse" aria-labelledby="headingVehicleBookkepping" data-parent="#accordionSidebar">
    <div class="bg-sidebar-group-agp py-2 collapse-inner">
      <a class="collapse-item" id="vehicleBookkeppingGraphs.php" onclick="redirectPage(this.id);">Graficos</a>
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Tablas:</h6>
      <a class="collapse-item" id="vehicleBookkeppingGasTable.php" onclick="redirectPage(this.id);">Combustible</a>
      <a class="collapse-item" id="vehicleBookkeppingMaintenanceTable.php" onclick="redirectPage(this.id);">Mantenimiento</a>
      <a class="collapse-item" id="vehicleBookkeppingFinancingTable.php" onclick="redirectPage(this.id);">Financiación</a>
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Resumenes:</h6>
      <a class="collapse-item" id="vehicleBookkeppingTotalSummary.php" onclick="redirectPage(this.id);">Total</a>
      <a class="collapse-item" id="vehicleBookkeppingAnnualSummary.php" onclick="redirectPage(this.id);">Anual</a>
    </div>
  </div>
</li>
<li class="nav-item">
  <a class="nav-link" id="bookkeppingPeriodicities.php" onclick="redirectPage(this.id);">
    <i class="fas fa-fw fa-clock"></i>
    <span>Perioicidades</span></a>
</li>
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBookkeppingDropdown" aria-expanded="true" aria-controls="collapseBookkeppingDropdown">
    <i class="fas fa-fw fa-caret-square-down"></i>
    <span>Desplegables</span>
  </a>
  <div id="collapseBookkeppingDropdown" class="collapse" aria-labelledby="headingBookkeppingDropdown" data-parent="#accordionSidebar">
    <div class="bg-sidebar-group-agp py-2 collapse-inner">
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Desplegables:</h6>
      <a class="collapse-item" id="bookkeppingDropdownCategory.php" onclick="redirectPage(this.id);">Categorías</a>
      <a class="collapse-item" id="bookkeppingDropdownConcept.php" onclick="redirectPage(this.id);">Conceptos</a>
      <a class="collapse-item" id="bookkeppingDropdownHouses.php" onclick="redirectPage(this.id);">Viviendas</a>
      <a class="collapse-item" id="bookkeppingDropdownVehicles.php" onclick="redirectPage(this.id);">Vehículos</a>
    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">
<!-- Heading -->
<div class="sidebar-heading">
  Inventario
</div>

<!-- Nav Item -->
<li class="nav-item">
  <a class="nav-link" id="inventoryTable.php" onclick="redirectPage(this.id);">
    <i class="fas fa-fw fa-boxes"></i>
    <span>Vivienda</span></a>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseInventoryDropdown" aria-expanded="true" aria-controls="collapseInventoryDropdown">
    <i class="fas fa-fw fa-caret-square-down"></i>
    <span>Desplegables</span>
  </a>
  <div id="collapseInventoryDropdown" class="collapse" aria-labelledby="headingInventoryDropdown" data-parent="#accordionSidebar">
    <div class="bg-sidebar-group-agp py-2 collapse-inner">
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Desplegables:</h6>
      <a class="collapse-item" id="inventoryDropdownPlace.php" onclick="redirectPage(this.id);">Ubicaciones</a>
      <a class="collapse-item" id="inventoryDropdownElementType.php" onclick="redirectPage(this.id);">Tipos</a>
    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Heading -->
<div class="sidebar-heading">
  Viajes
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTrips" aria-expanded="true" aria-controls="collapsePages">
    <i class="fas fa-fw fa-fighter-jet"></i>
    <span>Viajes</span>
  </a>
  <div id="collapseTrips" class="collapse" aria-labelledby="headingTrips" data-parent="#accordionSidebar">
    <div class="bg-sidebar-group-agp py-2 collapse-inner">
      <a class="collapse-item" id="tripsMap.php" onclick="redirectPage(this.id);">Mapa</a>
      <a class="collapse-item" id="tripsTable.php" onclick="redirectPage(this.id);">Tabla</a>
    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<!--div class="sidebar-heading">
  Notas
</div-->

<!--li class="nav-item">
  <a class="nav-link" id="notes.php" onclick="redirectPage(this.id);">
    <i class="fas fa-fw fa-clipboard-list"></i>
    <span>Notas</span></a>
</li-->

<!-- Divider -->
<!--hr class="sidebar-divider d-none d-md-block"-->

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->
