<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow toolbar">

<!-- Sidebar Toggle (Topbar) -->
<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
  <i class="fa fa-bars"></i>
</button>

<!-- Topbar Navbar -->
<ul class="navbar-nav ml-auto">
  
  <!-- Nav Item - Notes -->
  <li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" href="#" id="notesDropdownIcon" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-clipboard fa-fw"></i>
      <!-- Counter - Notes -->
      <span class="badge badge-info badge-counter hidden">?</span>
    </a>
    <!-- Dropdown - Notes -->
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in hidden" id="notesDropdown" aria-labelledby="notesDropdown" >
      <h6 class="dropdown-header">
        Notas con aviso
      </h6>
      <div id="notesDropdownList">
      </div>
    </div>
  </li>

  <!-- Nav Item - Periodicities -->
  <li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" href="#" id="periodicitiesDropdownIcon" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-clock fa-fw"></i>
      <!-- Counter - Periodicities -->
      <span class="badge badge-warning badge-counter hidden">?</span>
    </a>
    <!-- Dropdown - Periodicities -->
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in hidden" id="periodicitiesDropdown" aria-labelledby="periodicitiesDropdown" >
      <h6 class="dropdown-header">
        Avisos de peridicidades
      </h6>
      <div id="periodicitiesDropdownList">
      </div>
    </div>
  </li>

  <!-- Nav Item - Trips -->
  <li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" href="#" id="tripsDropdownIcon" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-plane fa-fw"></i>
      <!-- Counter - Trips -->
      <span class="badge badge-success badge-counter hidden">?</span>
    </a>
    <!-- Dropdown - Trips -->
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in hidden" id="tripsDropdown" aria-labelledby="tripsDropdown" >
      <h6 class="dropdown-header">
        Lugares pendientes
      </h6>
      <div id="tripsDropdownList">
      </div>
    </div>
  </li>

  <div class="topbar-divider d-none d-sm-block"></div>

  <li class="nav-item dropdown no-arrow show">
    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['nombre']." ".$_SESSION['apellido'] ?></span>
      <img class="img-profile rounded-circle" style="background-image: url('<?php echo $_SESSION['foto'] ?>'); background-size: cover!important;">
    </a>
    <!-- Dropdown - User Information -->
    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
      <a class="dropdown-item" id="users.php" onclick="redirectPage(this.id);">
        <i class="fas fa-users fa-sm fa-fw mr-2 text-gray-400"></i>
        Gestión usuarios
      </a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal" onclick="logOutConfirmation()">
        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
        Logout
      </a>
    </div>
  </li>
</ul>

</nav>
<!-- End of Topbar -->