<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
<div class="modal fade" id="dataEnterModal" tabindex="-1" role="dialog" aria-labelledby="dataEnterModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content card-shadow">
        <div class="modal-header card-header">
          <h5 class="modal-title m-0 font-weight-bold text-primary" id="dataEnterModalLabel">Variable</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="enterForm" method="POST">
                <div class="panel-body">
                    <input type="hidden" id="tipo" name="tipo" value="new">
                    <input type="hidden" id="editTag" name="editTag" value="">
                    <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $_SESSION['id']; ?>">  

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="ubicacion">
                                Ubicación
                            </label>
                            <select id="ubicacion" name="ubicacion" class="form-control selectpicker" data-live-search="true" data-size="5" required>
                                <option value="" ></option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="tipoElemento">
                                Tipo de elemento
                            </label>
                            <select id="tipoElemento" name="tipoElemento" class="form-control selectpicker" data-live-search="true" data-size="5" required>
                                <option value="" ></option>
                            </select>
                        </div>
                    </div>       
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="elemento">
                                Elemento
                            </label>
                            <input id="elemento" name="elemento" type="text" class="form-control" autocomplete="off" value="" required>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="lugar_adquisicion">
                                Lugar de adquisicion
                            </label>
                            <input id="lugar_adquisicion" name="lugar_adquisicion" type="text" class="form-control" autocomplete="off" value="">
                        </div>
                    </div>   
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="modelo">
                                Modelo
                            </label>
                            <input id="modelo" name="modelo" type="text" class="form-control" autocomplete="off" value="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="identificativo">
                                Identificativo
                            </label>
                            <input id="identificativo" name="identificativo" type="text" class="form-control" autocomplete="off" value="">
                        </div>
                    </div>  
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="fecha_adquisicion">
                                Fecha de adquisición
                            </label>
                            <div id="fecha_adquisicion" class="form-group input-group date">
                                <input id="fecha_ad" name="fecha_adquisicion" type="text" class="form-control" autocomplete="off" value="" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="valor_adquisicion">
                                Valor de adquisición
                            </label>
                            <div class="form-group input-group date">
                                <input type="number" step="0.01" id="valor_adquisicion" name="valor_adquisicion" class="form-control" value="" autocomplete="off" required>  
                                <span class="input-group-addon"><i class="fa fa-euro-sign"></i></span>
                            </div>
                        </div>
                    </div>  
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="fecha_desecho">
                                Fecha de desecho
                            </label>
                            <div id="fecha_desecho" class="form-group input-group date">
                                <input id="fecha_des" name="fecha_desecho" type="text" class="form-control" autocomplete="off" value="">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="valor_residual">
                                Valor residual
                            </label>
                            <div class="form-group input-group date">
                                <input type="number" step="0.01" id="valor_residual" name="valor_residual" class="form-control" value="" autocomplete="off">  
                                <span class="input-group-addon"><i class="fa fa-euro-sign"></i></span>
                            </div>
                       </div>
                    </div>    
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="estado">
                                Estado
                            </label>
                            <select id="estado" name="estado" class="form-control selectpicker" data-live-search="true" data-size="5" required>
                                <option value="" ></option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="cantidad">
                                Cantidad
                            </label>
                            <input id="cantidad" name="cantidad" type="number" step="1" class="form-control" autocomplete="off" value="" required>
                        </div>
                    </div>          
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="observaciones">
                                Observaciones
                            </label>
                            <input id="observaciones" name="observaciones" type="text" class="form-control" autocomplete="off" value="">
                        </div>
                    </div>   
                </div>
                <div class="modal-footer" style="margin-top: 10px;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-primary" type="submit"  id="guardar">Guardar cambios</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>