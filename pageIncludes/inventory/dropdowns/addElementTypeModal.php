<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
<div class="modal fade" id="dataEnterModal" tabindex="-1" role="dialog" aria-labelledby="dataEnterModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content card-shadow">
        <div class="modal-header card-header">
          <h5 class="modal-title m-0 font-weight-bold text-primary" id="dataEnterModalLabel">Variable</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="enterForm" method="POST">
                <div class="panel-body">
                    <input type="hidden" id="tipo" name="tipo" value="new">
                    <input type="hidden" id="editTag" name="editTag" value="">
                    <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $_SESSION['id']; ?>">  

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="tipoElemento">
                                Tipo de elemento
                            </label>
                            <input id="tipoElemento" name="tipoElemento" type="text" class="form-control" autocomplete="off" value="" required>
                        </div>
                    </div>                     
                </div>
                <div class="modal-footer" style="margin-top: 10px;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-primary" type="submit"  id="guardar">Guardar cambios</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>