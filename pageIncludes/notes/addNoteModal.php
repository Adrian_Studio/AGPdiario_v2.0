<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
<div class="modal fade" id="dataEnterModal" tabindex="-1" role="dialog" aria-labelledby="dataEnterModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content card-shadow">
        <div class="modal-header card-header">
          <h5 class="modal-title m-0 font-weight-bold text-primary" id="dataEnterModalLabel">Variable</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="enterForm" method="POST">
                <div class="panel-body">
                    <input type="hidden" id="tipo" name="tipo" value="new">
                    <input type="hidden" id="editTag" name="editTag" value="">
                    <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $_SESSION['id']; ?>">  
                    <!-- bootstrap-imageupload. -->

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="fechaNote">
                                Fecha
                            </label>
                            <div id="fechaNote" class="form-group input-group date">
                                <input id="fecha" name="fecha" type="text" class="form-control" autocomplete="off" value="" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="clasificacion">
                                Clasificación
                            </label>
                            <select id="clasificacion" name="clasificacion" class="form-control selectpicker" data-live-search="true" data-size="5" required>
                                <option ></option>
                            </select>
                        </div>
                    </div>       
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="tiempo_aviso">
                                Tiempo aviso (días)
                            </label>
                            <div class="form-group input-group">
                                <input type="number" step="1" min="1" id="tiempo_aviso" name="tiempo_aviso" class="form-control" value="" autocomplete="off">  
                                <span class="input-group-addon" id="aviso_desactivado" onclick="$('#aviso_desactivado').prop('hidden', true);$('#aviso_activado').prop('hidden', false);$('#aviso_activo').val(1);"><i class="fa fa-bell-slash"></i></span>
                                <span class="input-group-addon" id="aviso_activado" onclick="$('#aviso_desactivado').prop('hidden', false);$('#aviso_activado').prop('hidden', true);$('#aviso_activo').val(0);" hidden><i class="fa fa-bell"></i></span>
                                <input type="hidden" id="aviso_activo" name="aviso_activo" value="0">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <label for="periodicidad_meses">
                                Periodicidad (meses)
                            </label>
                            <div class="form-group input-group">
                                <input type="number" step="1" min="1" id="periodicidad_meses" name="periodicidad_meses" class="form-control" value="" autocomplete="off">  
                                <span class="input-group-addon" id="periodicidad_desactivada" onclick="$('#periodicidad_desactivada').prop('hidden', true);$('#periodicidad_activada').prop('hidden', false);$('#periodicidad_activa').val(1);"><i class="fa fa-slash"></i></span>
                                <span class="input-group-addon" id="periodicidad_activada" onclick="$('#periodicidad_desactivada').prop('hidden', false);$('#periodicidad_activada').prop('hidden', true);$('#periodicidad_activa').val(0);" hidden><i class="fa fa-clock"></i></span>
                                <input type="hidden" id="periodicidad_activa" name="periodicidad_activa" value="0">
                            </div>
                        </div>
                    </div>       
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="nota">
                                Nota
                            </label>
                            <div class="form-group input-group date">
                                <input type="text" id="nota" name="nota" class="form-control" value="" autocomplete="off" required>  
                            </div>
                        </div>
                    </div>                  
                </div>

                <div class="modal-footer" style="margin-top: 10px;">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button class="btn btn-primary" type="submit"  id="guardar">Guardar cambios</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>