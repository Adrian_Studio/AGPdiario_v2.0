<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
<div class="modal fade" id="dataEnterModal" tabindex="-1" role="dialog" aria-labelledby="dataEnterModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content card-shadow">
        <div class="modal-header card-header">
          <h5 class="modal-title m-0 font-weight-bold text-primary" id="dataEnterModalLabel">Variable</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="enterForm" method="POST">
                <div class="panel-body">
                    <input type="hidden" id="tipo" name="tipo" value="new">
                    <input type="hidden" id="editTag" name="editTag">    
                    <!-- bootstrap-imageupload. -->

                    <label>Foto de perfil</label>
                    <div class="imageupload panel panel-default">
                        <div class="file-tab panel-body">
                            <label class="btn btn-default btn-primary">
                                <span>Cargar imagen</span>
                                <!-- The file is stored here. -->
                                <input type="file" name="image-file" id="image-file">
                            </label>
                        </div>
                    </div>

                    <label for="nombre">
                        Nombre de usuario
                    </label>
                    <input type="text" id="nombre" name="nombre" class="form-control" required disabled>

                    <label for="password">
                        Contraseña
                    </label>
                    <input type="password" id="password" name="password" class="form-control" required>

                    <label for="password2">
                        Confirmación de contraseña
                    </label>
                    <input type="password" id="password2" name="password2" class="form-control" required>

                    <label for="nombreReal">
                        Nombre
                    </label>
                    <input type="text" id="nombreReal" name="nombreReal" class="form-control" required>

                    <label for="apellido">
                        Apellido
                    </label>
                    <input type="text" id="apellido" name="apellido" class="form-control" required>
                    
                    <?php if($_SESSION['rol'] == 'Administrador') echo ' <label for="privilegios">
                        Privilegios
                    </label>
                    <select id="privilegios" name="privilegios" class="form-control">
                        <option value="Usuario" selected>Usuario</option>
                        <option value="Administrador">Administrador</option>
                    </select>'; ?>
                    
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary" id="guardar">Guardar cambios</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>