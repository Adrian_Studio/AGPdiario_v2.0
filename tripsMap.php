<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>AGPdiario - Viajes</title>
  <link rel="shortcut icon" type="image/png" href="favicon.ico">

  <!-- Custom CSS -->
  <link href="css/masters/master.css" rel="stylesheet">
  <link href="css/masters/master_trips.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php include("pageIncludes/general/menu.php"); ?>
    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include("pageIncludes/general/toolbar.php"); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">
            Cuaderno de bitácora
            <a href="#" class="popoverReference data-addition" title="Añadir viaje" data-toggle="modal" data-target="#dataEnterModal" data-content="Añadir viaje" onclick="addInAdditionModal(this.title,this.id);" id="Viajes"><i class="fa fa-plus-circle"></i></a>          
          </h1>
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Países que <span style="color: #226E01;"> he estado viviendo </span> , <span style="color: #85E3D6;"> tengo viajes programados </span>,<span style="color: #329E56;"> he visitado </span> y/o <span style="color: #FFC202;"> he trabajado </span></h6>
            </div>
            <div class="card-body" id="trips-car-body">
              <div class="row" style="margin-top: 0px; text-align: center;"> 
                <div id="world-map" class="col-md-12 col-sm-12 col-xs-12" style="height: 70vh; position: relative; overflow: hidden;"></div>
              </div>
            </div>
            <div class="card-footer py-3 mapFooter">
              <div  style="text-align:center;">
                <button href="#" class="btn btn-primary btn-icon-split mapSelected" id="world_mill" onclick="changeoMap(this.id)">
                  <span class="text">Mundial</span>
                </button>
                <button href="#" class="btn btn-primary btn-icon-split" id="es_mill" onclick="changeoMap(this.id)" >
                  <span class="text">España</span>
                </button>
                <button href="#" class="btn btn-primary btn-icon-split" id="europe_mill" onclick="changeoMap(this.id)">
                  <span class="text">Europa</span>
                </button>
                <button href="#" class="btn btn-primary btn-icon-split" id="asia_mill" onclick="changeoMap(this.id)">
                  <span class="text">Asia</span>
                </button>
                <button href="#" class="btn btn-primary btn-icon-split" id="africa_mill" onclick="changeoMap(this.id)">
                  <span class="text">África</span>
                </button>
                <button href="#" class="btn btn-primary btn-icon-split" id="north_america_mill" onclick="changeoMap(this.id)">
                  <span class="text">NorteAmérica</span>
                </button>
                <button href="#" class="btn btn-primary btn-icon-split" id="south_america_mill" onclick="changeoMap(this.id)">
                  <span class="text">Sudamérica</span>
                </button>
                <button href="#" class="btn btn-primary btn-icon-split" id="oceania_mill" onclick="changeoMap(this.id)" >
                  <span class="text">Oceanéa</span>
                </button>
              </div>

            </div>
          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <?php include("pageIncludes/general/footer.php"); ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  
  <!-- Add category Modal-->
  <?php include("pageIncludes/bookkeeping/addTripModal.php"); ?>

  <!-- Javascript Master -->
  <script src="js/masters/master.js"></script>
  <script src="js/masters/master_trips.js"> </script>
</body>

</html>
